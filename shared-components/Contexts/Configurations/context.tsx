import {createContext, ReactNode, useReducer, useEffect} from "react";
import {Configs} from "./model";
import gql from "graphql-tag";
import { useQuery } from "@apollo/client";




const ConfigStateContext = createContext<Configs>({});
const ConfigDispatchContext = createContext(null);

const defaultState : Configs = {};

const reducer = (state : Configs, action) : Configs => {
    switch (action.type) {
        case 'merge':
            return {...state, ...action.payload};
        case 'reset':
            return {...defaultState};
        default:
            throw new Error("no funciona");
    }
};

const ConfigurationsProvider = ({ children, initialState = null, active } : {children: ReactNode, initialState : Configs, active: boolean}) => {

    const [state, dispatch] = useReducer(reducer,initialState == null?defaultState:initialState);

    if(!active) {
        return (<>{children}</>);
    }


    return (

        <ConfigDispatchContext.Provider value={dispatch}>
            <ConfigStateContext.Provider value={state}>
                {children}
            </ConfigStateContext.Provider>
        </ConfigDispatchContext.Provider>
    );

};

export {ConfigStateContext, ConfigDispatchContext, ConfigurationsProvider}


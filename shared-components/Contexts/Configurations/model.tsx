export interface Configs {
	country_id?: any;
	country_name?: any;
	country_capital?: any;
	country_capital_id?: any;
	country_code?: any;
	logo?: any;
	information_email?: any;
	sales_email?: any;
	currency?: any;
	timezone?: any;
	IVA?: any;
	analytics_id?: any;
	google_tag_manager_id?: any;
	onesignal_configuration?: any;
	facebook_configuration?: any;
	pinterest_id?: any;
	instagram_client_id?: any;
	header_configuration?: any;
	socialMediaLinks?: any;
	header_links?: HeaderLink[];
	rtb_id?: string;
	main_domain?: string;
	site_name?: string;
}

interface BaseLink {
	id: string;
	title: String;
	link: String;
}

interface HeaderLink extends BaseLink {
	links: SubHeaderLink[];
	banner_params: BannerParams;
}

interface SubHeaderLink extends BaseLink {
	links: ThirdHeaderLink[];
}

interface ThirdHeaderLink extends BaseLink {
	category: string;
}

type BannerParams = {
	operation_type_id: number;
};

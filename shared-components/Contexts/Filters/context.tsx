import {createContext, ReactNode, useEffect, useReducer} from "react";
import {Filters} from "./model";


export const FiltersStateContext = createContext(null);
export const FiltersDispatchContext = createContext(null);

const defaultState : Filters = {
    page: {
        value: 1,
        text: "Pagina 1"
    },
    order: {
        value: 2,
        text: "Popularidad",
    },
    currencyID: {
        value: 1,
        text: "U$S"
    }
};

const reducer = (state : Filters, action) : Filters => {
    switch (action.type) {
        case 'merge':

            if (!("page" in action.payload)) {
                action.payload.page = { value: 1, text: "Pagina 1" };
            }

            return {...state, ...action.payload};
        case 'reset':
            return {...defaultState};
        default:
            throw new Error("no funciona");
    }
};

export const FiltersContextProvider = ({ children, initialState = null, active } : {children: ReactNode, initialState : Filters, active: boolean}) => {
    if(!active) {
        return (<>{children}</>);
    }
    const [state, dispatch] = useReducer(reducer,{...defaultState,...initialState});

    useEffect(() => {
        if (!initialState) return;
        dispatch({type:'merge',payload:initialState})
    },[initialState]);


    return (

        <FiltersDispatchContext.Provider value={dispatch}>
            <FiltersStateContext.Provider value={state}>
                {children}
            </FiltersStateContext.Provider>
        </FiltersDispatchContext.Provider>
    );

};

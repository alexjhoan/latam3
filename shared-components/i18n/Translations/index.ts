import es from "./es.json";
import pt from "./pt.json";

export const langs = {
	es: {
		translation: es,
	},
	pt: {
		translation: pt,
	},
};

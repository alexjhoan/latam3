import React from "react";
import { View, Text } from "react-native";
import { useCountrySelection } from "./Country.hook";
import { Button } from "../Inputs/Button/app";

export const CountrySelect = () => {
  const { countryList, selected, select, save } = useCountrySelection();

  if (countryList.loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>...Loading</Text>
      </View>
    );
  }
  if (countryList.error) {
    console.log(countryList.error);
    return <Text>error</Text>;
  }

  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Country Select</Text>

      <View>
        {countryList.data.countries.map((c: any) => (
          <Button
            key={c.id}
            type={selected.id == c.id ? "primary" : "wired"}
            content={"Set as: " + c.name}
            handleClick={() => select(c)}
          />
        ))}
        <Button
          content={"Guardar"}
          handleClick={() => save(selected.data)}
          type="primary"
        />
      </View>
    </View>
  );
};

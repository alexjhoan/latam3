import { useState } from "react";
import { ApolloError, useMutation, useLazyQuery } from "@apollo/client";
import {
	CURRENT_USER_QUERY,
	LOGIN_MUTATION,
	LOGOUT_MUTATION,
	REGISTER_MUTATION,
	USER_EXISTS_QUERY,
	SOCIAL_LOGIN_MUTATION,
	FORGOT_PASSWORD_MUTATION,
	GOOGLE_ONE_TAP_MUTATION,
} from "./User.querys";
import { useQuerySkipAuth } from "../../GlobalHooks/useQuerySkipAuth";

type UserInput = { email: string; password: string };

interface UserHookInterface {
	user: {
		data: any;
		loading: boolean;
		error: ApolloError | undefined;
		refetch: Function;
	};
	isLoggedIn: boolean;
	userInput: {
		value: UserInput;
		set: ({ email, password }: UserInput) => void;
	};
	login: { send: () => void; response: any };
	logout: { send: () => Promise<any>; response: any };
	register: { send: () => void; doLater: () => void; response: any };
	userExists: { send: () => void; response: any };
	forgotPassword: { send: () => void; response: any };
	socialLogin: { send: (x: any) => void; response: any };
	oneTapSignIn: { send: (x: any) => void; response: any };
	validationErrors: { errors: string[]; set: (x: string[]) => void };
}

const useUser = (): UserHookInterface => {
	const { data, loading, error, refetch } = useQuerySkipAuth(CURRENT_USER_QUERY);
	const [userInput, setUserInput] = useState({ email: "", password: "" });
	const [validationErrors, setValidationErrors] = useState<string[]>([]);

	const errorHandler = (errors: ApolloError) => {
		let valErrors: string[] = [];
		errors.graphQLErrors.forEach(err => {
			if (err.extensions.category == "validation") {
				for (const [key, value] of Object.entries(err.extensions.validation)) {
					valErrors.push(value[0]);
				}
			} else if (err.extensions.category == "authentication") {
				valErrors.push(err.message);
			} else if (err.extensions.category == "WrongCountry") {
				valErrors.push(err.message);
			}
		});
		setValidationErrors(valErrors);
	};

	/* Log In */
	const [loginMutation, loginResponse] = useMutation(LOGIN_MUTATION, {
		onError: (errors: ApolloError) => errorHandler(errors),
	});
	const sendLogin = () => {
		setValidationErrors([]);
		loginMutation({
			variables: {
				email: userInput.email,
				pass: userInput.password,
			},
		});
	};
	/* End Log In */

	/* Register */
	const [registerMutation, registerResponse] = useMutation(REGISTER_MUTATION, {
		onError: (errors: ApolloError) => errorHandler(errors),
	});
	const sendRegister = () => {
		setValidationErrors([]);
		registerMutation({
			variables: {
				email: userInput.email,
				pass: userInput.password,
			},
		});
	};
	const sendFastRegister = () => {
		setValidationErrors([]);
		registerMutation({
			variables: { email: userInput.email },
		});
	};
	/* End Register */

	/* User Exists */
	const [userExists, userExistsResponse] = useLazyQuery(USER_EXISTS_QUERY, {
		onError: (errors: ApolloError) => errorHandler(errors),
		fetchPolicy: "no-cache",
	});
	const sendUserExist = () => {
		setValidationErrors([]);
		userExists({ variables: { email: userInput.email } });
	};
	/* End User Exists */

	/* Social Login */
	const [loginSocial, socialResponse] = useMutation(SOCIAL_LOGIN_MUTATION, {
		onError: (errors: ApolloError) => errorHandler(errors),
	});
	const sendSocialLogin = variables => {
		setValidationErrors([]);
		loginSocial({ variables: { ...variables } });
	};
	/* End Social Login */

	/* One Tap Sign In */
	const [MUTATOR_ONE_TAP_SIGN_IN, responseOneTapSignIn] = useMutation(GOOGLE_ONE_TAP_MUTATION);
	const sendOneTapSignIn = variables => MUTATOR_ONE_TAP_SIGN_IN({ variables: { ...variables } });
	/* End One Tap Sign In */

	/* Forget Password */
	const [forgotPassword, forgotPasswordResponse] = useMutation(FORGOT_PASSWORD_MUTATION, {
		onError: (errors: ApolloError) => errorHandler(errors),
	});
	const sendForgotPassword = () => {
		setValidationErrors([]);
		forgotPassword({ variables: { email: userInput.email } });
	};
	/* End Forget Password */

	/* Log Out */
	const [logoutMutation, logoutResponse] = useMutation(LOGOUT_MUTATION);
	/* End Log Out */

	return {
		user: { data: data, loading: loading, error: error, refetch },
		isLoggedIn: !error && data && data?.me != null && data.me.id ? true : false,
		userInput: {
			value: userInput,
			set: setUserInput,
		},
		login: {
			send: sendLogin,
			response: loginResponse,
		},
		logout: { send: logoutMutation, response: logoutResponse },
		register: {
			send: sendRegister,
			doLater: sendFastRegister,
			response: registerResponse,
		},
		userExists: {
			send: sendUserExist,
			response: userExistsResponse,
		},
		socialLogin: {
			send: sendSocialLogin,
			response: socialResponse,
		},
		oneTapSignIn: {
			send: sendOneTapSignIn,
			response: responseOneTapSignIn,
		},
		forgotPassword: {
			send: sendForgotPassword,
			response: forgotPasswordResponse,
		},
		validationErrors: {
			errors: validationErrors,
			set: setValidationErrors,
		},
	};
};

export { useUser };

import { gql, useMutation } from "@apollo/client";
import { useUser } from "../User.hook";

const CHANGE_STATUS_MUTATION = gql`
	mutation changeNotificationStatus($id: ID!, $seen: Boolean!) {
		notificationSetSeen(id: $id, seen: $seen) {
			id
		}
	}
`;

const ALL_NOTIFICATION_SEEN_MUTATION = gql`
	mutation changeNotificationsStatus {
		notificationSeeAll
	}
`;

export const useNotifications = () => {
	const { user } = useUser();

	const [changeNotification, { loading }] = useMutation(CHANGE_STATUS_MUTATION, {
		onCompleted: response => user.refetch(),
		onError: error => console.error(error),
	});

	const changeNotificationStatus = (id, status) => {
		changeNotification({ variables: { seen: status, id: id } });
	};

	const [setAllNotificationSeen, { loading: loadingAll }] = useMutation(
		ALL_NOTIFICATION_SEEN_MUTATION,
		{
			onCompleted: response => user.refetch(),
			onError: error => console.error(error),
		}
	);

	return {
		notifications: user?.data?.me.notifications.data,
		unread_notifications: user?.data?.me.unread_notifications,
		changeNotificationStatus,
		loadingChange: loading,
		setAllNotificationSeen,
		loadingAll: loadingAll,
		loadingUser: user.loading,
	};
};

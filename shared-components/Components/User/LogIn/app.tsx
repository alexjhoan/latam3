import React from "react";
import { Text, TextInput } from "react-native";
import { useUser } from "../User.hook";
import { LocalStorage } from "../../../../src/LocalStorage";
import { Button } from "../../Inputs/Button/app";

const Login = () => {
	const {
		isLoggedIn,
		login: { send, response },
		userInput: input,
	} = useUser();

	let output = <></>;
	if (response.loading) output = <Text>...</Text>;
	else if (response.error)
		output = <Text>{response.error.graphQLErrors[0].message}</Text>;
	else if (response.data) {
		LocalStorage.set("authToken", response.data.login.access_token, false);
	}

	return (
		<>
			{isLoggedIn ? (
				<Text>Loged in </Text>
			) : (
				<>
					<TextInput
						style={{ height: 40 }}
						placeholder="Email"
						onChangeText={email => input.set({ ...input.value, email: email })}
						defaultValue={input.value.email}
					/>
					<TextInput
						style={{ height: 40 }}
						placeholder="Password"
						onChangeText={pass => input.set({ ...input.value, password: pass })}
						defaultValue={input.value.password}
					/>

					<Button content="Send" type="primary" handleClick={send} />

					{output}
				</>
			)}
		</>
	);
};

export { Login };

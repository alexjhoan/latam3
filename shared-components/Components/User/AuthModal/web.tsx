import "./styles.less";

import React, { useEffect, useState } from "react";

import { CloseCircleFilled } from "@ant-design/icons";
import { Login } from "../LogIn/web";
import { Modal } from "antd";
import { useIsAuthModalShowing } from "../../../GlobalHooks/useIsAuthModalShowing";
import { FullScreen } from "../../FullScreen/web";

type ModalType = "LogIn" | "Register" | false;
const EVENT_NAME = "EVENT_SHOW_LOGIN_MODAL";

const AuthModal = () => {
	const [modalType, setModalType] = useState<ModalType>(false);
	const [email, setEmail] = useState("");
	const { setIsAuthModalShowing } = useIsAuthModalShowing();

	const [event, setEvent] = useState({
		accept: data => {},
		reject: () => {},
	});

	const listener = ({ detail: { accept, reject, defaultModalType, email } }) => {
		console.warn(`${EVENT_NAME} FIRED`);
		setIsAuthModalShowing(true);
		if (!modalType) {
			setModalType(defaultModalType);
			setEvent({ accept: accept, reject: reject });
			setEmail(email);
		}
	};

	useEffect(() => {
		console.warn(`LISTENING TO ${EVENT_NAME} `);
		// @ts-ignore
		window.addEventListener(EVENT_NAME, listener);
		window[EVENT_NAME] = true;

		return () => {
			console.warn(`REMOVING LISTENER OF ${EVENT_NAME}`);
			// @ts-ignore
			window.removeEventListener(EVENT_NAME, listener);
			window[EVENT_NAME] = false;
		};
	}, []);

	const onCancel = () => {
		event.reject();
		setIsAuthModalShowing(false);
		setModalType(false);
	};

	const onOk = data => {
		event.accept(data);
		setIsAuthModalShowing(false);
		setModalType(false);
	};

	if (!modalType) {
		return null;
	}

	return (
		<Modal
			wrapClassName="auth-modal-wrapper"
			className="auth-modal"
			width="100%"
			visible={!!modalType}
			onCancel={onCancel}
			footer={null}
			centered
			closeIcon={<CloseCircleFilled />}>
			<FullScreen className="modal-auth-container">
				<Login onOk={onOk} email={email} />
			</FullScreen>
		</Modal>
	);
};

const openAuthModal = ({
	withPromise = false,
	defaultModalType = "LogIn",
	email = "",
}: {
	withPromise?: boolean;
	defaultModalType?: ModalType;
	email?: string;
}) => {
	if (withPromise) {
		return new Promise((accept, reject) => {
			if (
				typeof window !== "undefined" &&
				typeof window[EVENT_NAME] !== "undefined" &&
				window[EVENT_NAME]
			) {
				window.dispatchEvent(
					new CustomEvent(EVENT_NAME, {
						detail: {
							defaultModalType: defaultModalType,
							accept: accept,
							reject: reject,
							email,
						},
					})
				);
			} else {
				reject("WINDOW ISNT LISTENING TO " + EVENT_NAME);
			}
		});
	} else {
		window.dispatchEvent(
			new CustomEvent(EVENT_NAME, {
				detail: {
					defaultModalType: defaultModalType,
					accept: () => {},
					reject: () => {},
					email,
				},
			})
		);
	}
};

export { AuthModal, openAuthModal };

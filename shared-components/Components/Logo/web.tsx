import "./styles.less";

import { ImagenOptimizada } from "../Image/web";
import { PageLink } from "../Link/web";
import React from "react";
import { useTheme } from "../../Styles/ThemeHook";

export function Brand({ type = "logo", link = false }: { type?: "logo" | "icon"; link?: Boolean }) {
	const { theme } = useTheme();

	const logo = (
		<div className="logo">
			<ImagenOptimizada src={theme.images.logo} alt="logo" className="logo_image" />
		</div>
	);

	const icon = (
		<div className="icon">
			<ImagenOptimizada src={theme.images.icono} alt="icono" className="icon_image" />
		</div>
	);

	const brand = type == "logo" ? logo : icon;

	return (
		<>
			{link ? (
				<PageLink pathname="/home" as={""}>
					{brand}
				</PageLink>
			) : (
				brand
			)}
		</>
	);
}

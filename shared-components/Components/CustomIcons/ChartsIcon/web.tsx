import Icon from "@ant-design/icons";

const ChartsIconSvg = () => (
	<svg
		width="1em"
		height="1em"
		viewBox="0 0 22 22"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			d="M8.43385 15.956L5.3387 12.859V15.956H8.43385ZM21.2948 21.276H5.3387V21.28H0.0150103V21.276H0V15.956H0.0150103V0L21.2948 21.276Z"
			fill="#869099"
		/>
	</svg>
);

export const ChartsIcon = props => <Icon component={ChartsIconSvg} {...props} />;

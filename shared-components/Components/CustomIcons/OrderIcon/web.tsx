import Icon from "@ant-design/icons";

const OrderIconSvg = () => (
	<svg
		width="1.2em"
		height="1em"
		viewBox="0 0 15 13"
		fill="none"
		xmlns="http://www.w3.org/2000/svg">
		<path
			d="M4.72792 10.2778L6.75328 8.72524L7.78325 10.0176L3.89225 13L0 10.0176L1.02934 8.72524L3.05532 10.2778V0H4.72792V10.2778ZM7.65497 2.42933V0.789224H14.7333V2.42933H7.65497ZM9.32757 6.76513V5.12502H14.7333V6.76513H9.32757V6.76513ZM11.0002 11.5128V9.8727H14.7333V11.5128H11.0002Z"
			fill="currentColor"
		/>
	</svg>
);

export const OrderIcon = props => <Icon component={OrderIconSvg} {...props} />;

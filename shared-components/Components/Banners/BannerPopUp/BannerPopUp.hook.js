import React, {useContext, useEffect, useState} from "react";
import { useLazyQuery, gql } from "@apollo/client";


const BANNER_POPUP_QUERY = gql`
  query BannerPopUp {
    featuredPopup {
      id
      title
      url
      image
      mobile_image
      video
      video_thumbnail
      html
    }
  }
`;


const useBannerPopUp = () => {

  const [getBannerPopUp, {data, error }] = useLazyQuery(
    BANNER_POPUP_QUERY
  );
    useEffect(() => {
      
          getBannerPopUp();
    
  }, []);
  
  return {
    data: data?.featuredPopup,
    error: error,
  };
};

export { useBannerPopUp };

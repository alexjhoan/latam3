import { useState, useEffect } from "react";

export type SelectTypes = "multiple" | "single" | "checkbox" | "radio";

type OptionObject = {
	[customKey: string]: any;
};
export interface SelectProps {
	type?: SelectTypes;
	options?: Array<OptionObject>;
	customKey?: string;
	valueKey?: string;
	value?: any | any[];
	onChange?: (x: any) => void;
	placeholder?: string;
	status?: any;
}

export const useSelects = (props: SelectProps) => {
	const {
		type = "single",
		customKey = "id",
		valueKey = "name",
		value,
		onChange = () => {},
		options = [],
		status,
		placeholder = "Seleccione una opción",
	} = props;

	const [nvalue, setNValue] = useState(normalizeValue(value, type, customKey));
	useEffect(() => {
		const normalize = normalizeValue(value, type, customKey);
		if (normalize != nvalue) {
			setNValue(normalize);
		}
	}, [value]);

	const isSelected = (val: number | string) => {
		let res = false;
		if (type == "multiple" || type == "checkbox") {
			if (Array.isArray(nvalue)) res = nvalue.findIndex((k) => k == val) >= 0;
		} else {
			res = nvalue != null && val == nvalue;
		}
		return res;
	};

	const handleSelectOption = (newKey: number | string) => {
		if (type == "multiple" || type == "checkbox") {
			let newKeys: Array<number | string> = [];
			if (!Array.isArray(value)) newKeys.push(newKey);
			else {
				newKeys = [...nvalue];
				const index = newKeys.findIndex((k) => k == newKey);
				if (index >= 0) newKeys.splice(index, 1);
				else newKeys.push(newKey);
			}
			const res = options.filter((opt) => newKeys.includes(opt[customKey]));
			setNValue(newKey);
			onChange(res);
		} else {
			if (newKey != nvalue) {
				const res = options.find((o) => o[customKey] == newKey);
				setNValue(newKey);
				onChange(res);
			}
		}
	};

	return {
		nvalue,
		isSelected,
		handleSelectOption,
		type,
		customKey,
		valueKey,
		onChange,
		options,
		status,
		placeholder,
	};
};

export const normalizeValue = (
	values: any | any[],
	type: SelectTypes,
	customKey: string
) => {
	if (type == "multiple" || type == "checkbox") {
		if (values && Array.isArray(values)) {
			return values.map((v) => {
				if (typeof v == "object") return v[customKey];
				else return v;
			});
		} else return [];
	} else {
		if (values) {
			if (typeof values == "object") return values[customKey];
			else return values;
		} else return "";
	}
};

import React, { FC } from "react";
import { Button } from "../../Button/app";
import { useSelects, SelectProps } from "../Selects.hook";
import { View, StyleSheet } from "react-native";

export interface ButtonSelectProps extends SelectProps {
	// podria tener una opcion que sea button type
}

const styles = StyleSheet.create({
	container: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-around",
	},
	option: { flexGrow: 1 },
});

export const ButtonSelect: FC<ButtonSelectProps> = (props) => {
	const {
		isSelected,
		handleSelectOption,
		customKey,
		valueKey,
		options,
		status = "active",
	} = useSelects(props);

	return (
		<View style={styles.container}>
			{options.map((opt, i) => {
				return (
					<View
						key={"button_select_option_" + opt[customKey] + "_" + i}
						style={styles.option}>
						<Button
							content={opt[valueKey]}
							type={isSelected(opt[customKey]) ? "primary" : "text"}
							handleClick={() => handleSelectOption(opt[customKey])}
							status={status}
						/>
					</View>
				);
			})}
		</View>
	);
};

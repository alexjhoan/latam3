import React, { FC } from "react";
import { Button } from "../../Button/web";
import { ButtonTypes } from "../../Button/Button.hook";
import theme from "../../../../Styles/_toBeDeleted_theme";
import { useSelects, SelectProps } from "../Selects.hook";

export interface ButtonSelectProps extends SelectProps {
	// podria tener una opcion que sea button type
}

export const ButtonSelect: FC<ButtonSelectProps> = props => {
	const { isSelected, handleSelectOption, customKey, valueKey, options, status } = useSelects(
		props
	);

	return (
		<React.Fragment>
			<div className={"button-select-container" + (status ? " " + status : "")}>
				<div className={"button-select"}>
					{options.map((opt, i) => {
						return (
							<div
								className="button-option"
								key={"button_select_option_" + opt[customKey] + "_" + i}>
								<Button
									content={opt[valueKey]}
									handleClick={() => handleSelectOption(opt[customKey])}
									type={isSelected(opt[customKey]) ? "primary" : "wired"}
								/>
							</div>
						);
					})}
				</div>
			</div>
			<style jsx>{`
				.button-select-container {
					height: 100%;
					width: 100%;
					position: relative;
				}
				.button-select {
					display: flex;
					justify-content: flex-start;
					flex-wrap: wrap;
					width: "auto";
				}
				.button-option {
          /*
          flex-basis: ${100 / options.length + "%"};
					border-left: 1px solid ${theme.colors.border};
					border-top: 1px solid ${theme.colors.border};
					border-bottom: 1px solid ${theme.colors.border};
          */
					box-sizing: border-box;
					overflow: hidden;
					justify-content: stretch;
          display: flex;
          margin: 0 2px;
        }
        /*
				.button-option:last-child {
          border-radius: 0 ${theme.borderRadius}px ${theme.borderRadius}px 0;
					border-right: 1px solid ${theme.colors.border};
				}
				.button-option:first-child {
          border-radius: ${theme.borderRadius}px 0 0 ${theme.borderRadius}px;
				}
        */
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</React.Fragment>
	);
};

import { useState, useEffect, ReactNode } from "react";
import { useSelects, SelectProps, SelectTypes } from "../Selects.hook";

export interface CheckboxSelectProps extends SelectProps {
  maxCountOptions?: number;
  showMoreText?: string | ReactNode;
  inline?: boolean;
}

export const useCheckboxSelects = (props: CheckboxSelectProps) => {
  const {
    maxCountOptions,
    showMoreText = "Ver Más",
    inline = false,
    type = "radio",
    ...selectprops
  } = props;

  // normalizo type
  let typeCheckbox: SelectTypes = type;
  if (typeCheckbox == "single") typeCheckbox = "radio";
  if (typeCheckbox == "multiple") typeCheckbox = "checkbox";

  const { ...useselectsprops } = useSelects({
    ...selectprops,
    type: typeCheckbox,
  });

  const [maxOptions, setOptionCount] = useState(
    typeof maxCountOptions !== "undefined"
      ? maxCountOptions
      : selectprops.options.length
  );

  const toggleOptions = () => {
    if (maxOptions < selectprops.options.length)
      setOptionCount(selectprops.options.length);
    else setOptionCount(maxCountOptions);
  };

  return {
    ...useselectsprops,
    inline,
    maxOptions,
    showMoreText,
    enableToggleOptions: typeof maxCountOptions !== "undefined",
    toggleOptions,
    type: typeCheckbox,
  };
};

import React, { FC } from "react";
import { CheckBox } from "../../Checkbox/web";
import { Button } from "../../Button/web";
import { useCheckboxSelects, CheckboxSelectProps } from "./CheckboxSelect.hook";

export const CheckboxSelect: FC<CheckboxSelectProps> = (props) => {
  const {
    isSelected,
    handleSelectOption,
    customKey,
    valueKey,
    options,
    status,
    type,
    inline,
    showMoreText,
    maxOptions,
    enableToggleOptions,
    toggleOptions,
  } = useCheckboxSelects(props);

  return (
    <React.Fragment>
      <div
        className={
          "select-container " +
          type +
          (status ? " " + status : "") +
          (inline ? " inline" : "")
        }
      >
        {options.map((opt, i) => {
          if (i >= maxOptions) return null;
          return (
            <div
              className="checkbox_select_option"
              key={"checkbox_select_option_" + i + "_" + opt[customKey]}
            >
              <CheckBox
                checked={isSelected(opt[customKey])}
                text={opt[valueKey]}
                type={type}
                handleClick={() => handleSelectOption(opt[customKey])}
              />
            </div>
          );
        })}
        {enableToggleOptions && (
          <Button
            content={showMoreText}
            type="text"
            handleClick={toggleOptions}
          />
        )}
      </div>
      <style jsx>{`
        .inline {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
          align-items: center;
        }
        .inline .checkbox_option {
          flex-basis: calc(
            ${100 / options.length}% - ${8 * (options.length - 1)}px
          );
        }
        .readonly {
          cursor: default;
          pointer-events: none;
          touch-action: none;
          user-select: none;
        }
        .disabled {
          cursor: default;
          pointer-events: none;
          touch-action: none;
          user-select: none;
          opacity: 0.5;
        }
      `}</style>
    </React.Fragment>
  );
};

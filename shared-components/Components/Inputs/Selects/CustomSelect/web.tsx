import { FC, useRef } from "react";
import { useCustomSelect } from "./CustomSelect.hook";
import { CheckboxSelect } from "../CheckboxSelect/web";
import { CustomSelectProps } from "./CustomSelect.hook";
import { WindowEventListener } from "../../../../GlobalHooks/web/WindowEventListener.hook";
import theme from "../../../../Styles/_toBeDeleted_theme";

export const CustomSelect: FC<CustomSelectProps> = props => {
	/* hook */
	const { open, title, placeholder, status, setOpen, ...checkboxselectprops } = useCustomSelect(
		props
	);

	/* click outside */
	const selectRef = useRef(null);
	const handleClickOutside = event => {
		if (open && !selectRef.current.contains(event.target)) setOpen(false);
	};
	WindowEventListener("click", handleClickOutside, false);
	/* end click outside */

	return (
		<>
			<div className={"custom_select" + (status ? " " + status : "")} ref={selectRef}>
				<div className={"custom_select_header"} onClick={() => setOpen(!open)}>
					<span className={"title" + (title != "" ? " placeholder" : "")}>
						{title != "" ? title : placeholder}
					</span>
				</div>
				<div className={"custom_select_dropdown" + (open ? " open" : "")}>
					<CheckboxSelect {...checkboxselectprops} />
				</div>
			</div>
			<style jsx>{`
				.custom_select {
					height: 100%;
					position: relative;
				}
				.custom_select_header {
					position: relative;
					width: 100%;
					height: 100%;
					display: flex;
					align-items: center;
					padding: 10px 30px 10px 7px;
					box-sizing: border-box;
					width: 100%;
					cursor: pointer;
					user-select: none;
					border: 1px solid ${theme.colors.border};
					background: ${theme.colors.background};
				}
				.custom_select_header:before {
					content: "";
					position: absolute;
					top: calc(50% - 3px);
					right: 8px;
					width: 0px;
					height: 0px;
					border: 5px solid transparent;
					border-top-color: ${theme.colors.textLighter};
				}
				.custom_select_header span {
					width: 100%;
					white-space: nowrap;
					text-overflow: ellipsis;
					overflow: hidden;
					font-size: ${theme.fontSizes.text}px;
					color: ${theme.colors.text};
				}
				.custom_select_dropdown {
					position: absolute !important;
					width: 100%;
					box-sizing: border-box;
					height: 0px;
					opacity: 0;
					background: ${theme.colors.background};
					border: 1px solid ${theme.colors.border};
					position: relative;
					z-index: 10;
					max-height: 200px;
					overflow-y: auto;
					overflow-x: hidden;
					box-shadow: 0px 4px 5px -4px rgba(0, 0, 0, 0.2);
					border-top: 1px solid #dddddd;
					transition: all ease 300ms;
					padding: 0 8px;
				}
				.custom_select_dropdown.open {
					height: auto;
					opacity: 1;
				}
				.placeholder {
					opacity: 0.7;
				}
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</>
	);
};

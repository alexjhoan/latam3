import { ReactNode } from "react";
import { useTheme } from "../../../Styles/ThemeHook";

export type ButtonTypes = "text" | "primary" | "secondary" | "wired" | "noStyle";
export interface ButtonProps {
	content: ReactNode | string;
	handleClick: () => void;
	type?: ButtonTypes;
	status?: "disabled" | "active"; //@Todo implement active for each type
	size?: "sm" | "md" | "lg";
}

export interface ButtonStyles {
	radius: number;
	width: string;
	height: string;
	background: string;
	borderColor: string;
	borderHover: string;
	color: string;
	colorHover: string;
	backgroundHover: string;
	borderWith: number;
	paddingVertical: number;
	paddingHorizontal: number;
	opacity: number;
}

const btnSpacing = (size: ButtonProps["size"]) => {
	const { theme } = useTheme();
	return size == "lg"
		? theme.spacing.lgSpacing
		: size == "sm"
		? theme.spacing.smSpacing
		: theme.spacing.mdSpacing;
};

export const useButton = (props: ButtonProps) => {
	const {theme} = useTheme();
	let styles: ButtonStyles = {
		radius: theme.spacing.,
		width: "auto",
		height: "auto",
		background: "transparent",
		borderColor: "transparent",
		borderHover: "transparent",
		color: theme.colors.primaryColor,
		colorHover: theme.colors.primaryDarkColor,
		backgroundHover: theme.colors.backgroundColor,
		borderWith: 1,
		paddingVertical: btnSpacing(props.size) * 0.66,
		paddingHorizontal: btnSpacing(props.size),
		opacity: props.status == "disabled" ? 0.5 : 1,
	};

	switch (props.type) {
		case "wired":
			styles.background = "transparent";
			styles.color = theme.colors.primaryColor;
			styles.backgroundHover = "transparent";
			styles.borderColor = theme.colors.primaryColor;
			styles.borderHover = theme.colors.primaryColor + "af";
			styles.colorHover = theme.colors.primaryColor + "af";
			break;
		case "secondary":
			styles.background = theme.colors.secondaryColor;
			styles.color = "white";
			styles.borderColor = styles.background;
			styles.backgroundHover = theme.colors.secondaryHoverColor;
			styles.colorHover = "white";
			break;
		case "primary":
			styles.background = theme.colors.primaryColor;
			styles.color = "white";
			styles.borderColor = styles.background;
			styles.backgroundHover = theme.colors.primaryHoverColor;
			styles.colorHover = "white";
			break;
		case "text":
			styles.background = "transparent";
			styles.backgroundHover = theme.colors.backgroundColor;
			styles.color = theme.colors.primaryColor;
			styles.colorHover = theme.colors.primaryHoverColor;
			break;
		case "noStyle":
			styles.background = "transparent";
			styles.backgroundHover = "transparent";
			styles.color = theme.colors.textColor;
			styles.colorHover = theme.colors.textSecondaryColor;
			styles.borderWith = 0;
			styles.paddingVertical = 0;
			styles.paddingHorizontal = 0;
			break;
	}

	return styles;
};

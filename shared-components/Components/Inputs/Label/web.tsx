import React, { ReactNode, FC } from "react";
import theme from "../../../Styles/_toBeDeleted_theme";

export interface LabelProps {
	labelContent?: string;
	children?: ReactNode;
	className?: string;
	showLabel?: boolean;
	htmlID?: string;
}

export const Label: FC<LabelProps> = ({
	labelContent,
	children,
	showLabel = true,
	className,
	htmlID,
}) => {
	return (
		<React.Fragment>
			{showLabel && labelContent && (
				<label
					title={labelContent}
					className={"label" + (className ? " " + className : "")}
					id={htmlID ? htmlID + "_label" : ""}
					htmlFor={htmlID}>
					{labelContent}
				</label>
			)}
			{children}
			<style jsx>{`
				.label {
					font-weight: bold;
					color: ${theme.colors.text};
					font-size: ${theme.fontSizes.text}px;
					display: inline-block;
					margin-bottom: 4px;
				}
			`}</style>
		</React.Fragment>
	);
};

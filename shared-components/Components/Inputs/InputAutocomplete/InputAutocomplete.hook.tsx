import { useState, useEffect } from "react";
import { useDebounce } from "../../../GlobalHooks/useDebounce";
import { InputProps } from "../Input/Input.hook";
import { SmartDropdownProps } from "./SmartDropdown/SmartDropdown.hook";

export interface InputAutocompleteProps extends InputProps, SmartDropdownProps {
	type?: "text";
	loading?: boolean;
	clear?: boolean;
	firstOpen?: boolean;
	openOnEmpty?: boolean;
	inputChangeFrequency?: number;
	callbackOnSelect?: (selected: object | null, inputVal: string) => void;
	callbackOnChange?: (value: string) => void;
}

export const useInputAutocomplete = (props: InputAutocompleteProps) => {
	const {
		type = "text",
		firstOpen = false,
		openOnEmpty = false,
		focused = false,
		inputChangeFrequency = 200,
		defaultValue = "",
		clear = true,
		callbackOnSelect = () => {},
		callbackOnChange = () => {},
		onFocus,
		onBlur,
		placeholder = "Escribir",
		...otherprops
	} = props;

	// open dropdown
	const [open, setOpen] = useState(firstOpen);

	// input value
	const [input, setInput] = useState(defaultValue);
	useEffect(() => setInput(defaultValue), [defaultValue]);
	const onInputChange = (value: string) => setInput(value);

	// input debounce
	const [debuncedInput] = useDebounce(input, inputChangeFrequency);
	useEffect(() => callbackOnChange(debuncedInput), [debuncedInput]);

	// input focus
	const [inputFocus, setFocus] = useState(focused || firstOpen);

	// onSelectedOption
	const onSelect = (option: object) => {
		callbackOnSelect(option, input);
		setOpen(false);
	};

	// onClear
	const onClear = () => callbackOnSelect(null, "");

	const handleFocus = (e: any) => {
		onInputChange("");
		if (typeof onFocus == "function") onFocus(e);
		if (!open) setOpen(true);
	};
	const handleBlur = (e: any) => {
		if (typeof onBlur == "function") onBlur(e);
	};

	return {
		open,
		setOpen,

		input,
		onInputChange,

		inputFocus,
		setFocus,

		clear,
		onClear,

		type,
		openOnEmpty,

		onSelect,

		handleFocus,
		handleBlur,

		placeholder,
		...otherprops,
	};
};

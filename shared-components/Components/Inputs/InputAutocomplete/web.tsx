import { FC, useEffect, useRef, useState } from "react";
import { Input } from "../Input/web";
import { useInputAutocomplete } from "./InputAutocomplete.hook";
import { SmartDropdown } from "./SmartDropdown/web";
import theme from "../../../Styles/_toBeDeleted_theme";
import { WindowEventListener } from "../../../GlobalHooks/web/WindowEventListener.hook";
import { Loader } from "../../Loader/web";
import { Button } from "../Button/web";

import { InputAutocompleteProps } from "./InputAutocomplete.hook";

export const InputAutocomplete: FC<InputAutocompleteProps> = props => {
	const {
		status,
		openOnEmpty,
		loading,
		open,
		setOpen,
		clear,
		onClear,
		handleBlur,
		handleFocus,

		required,
		name,
		type,
		input,
		placeholder,
		inputFocus,
		disabled,
		onInputChange,

		...smartdropdownprops
	} = useInputAutocomplete(props);

	/* click outside */
	const autoRef = useRef(null);
	const handleClickOutside = event => {
		if (open && autoRef && !autoRef.current.contains(event.target)) {
			setOpen(false);
			handleBlur(event);
		}
	};
	WindowEventListener("click", handleClickOutside, false);
	/* end click outside */

	return (
		<>
			<div className={"autocomplete" + (status ? " " + status : "")} ref={autoRef}>
				<div className="autocomplete_header" onClick={() => setOpen(true)}>
					<Input
						defaultValue={input}
						placeholder={placeholder}
						focused={inputFocus}
						disabled={status == "disabled"}
						type={type}
						onChange={onInputChange}
						required={required}
						name={name}
						onFocus={handleFocus}
					/>
					{clear && input != "" && (
						<div className="autocomplete-clear">
							<Button
								content={<i className="icon-cancel"></i>}
								type="text"
								handleClick={onClear}
							/>
						</div>
					)}
				</div>
				{open && (input.length > 0 || openOnEmpty) ? (
					<div className={"autocomplete_dropdown"}>
						{loading ? (
							<div className="autocomplete-loader">
								<Loader />
							</div>
						) : (
							<SmartDropdown match={input} {...smartdropdownprops} />
						)}
					</div>
				) : null}
			</div>
			<style jsx>{`
				.autocomplete {
					height: 100%;
					position: relative;
					font-size: 16px;
				}
				.autocomplete_header {
					position: relative;
					width: 100%;
					height: 100%;
					display: flex;
					align-items: center;
					padding: 0px;
					box-sizing: border-box;
					width: 100%;
					cursor: pointer;
					user-select: none;
					background: ${theme.colors.background};
				}
				.autocomplete-clear {
					position: absolute;
					right: 8px;
				}
				.autocomplete_dropdown {
					width: 100%;
					box-sizing: border-box;
					position: absolute !important;
					background: ${theme.colors.background};
					border: 1px solid ${theme.colors.border};
					position: relative;
					z-index: 10;
					box-shadow: 0px 4px 5px -4px rgba(0, 0, 0, 0.2);
					transition: height ease 300ms;
					padding: 0;
					height: auto;
					opacity: 1;
				}
				.autocomplete-loader {
					padding: 10px 0px;
				}
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</>
	);
};

export interface SmartDropdownOptionProps {
  data?: object;
  navCursorRef?: any;
  isNavCursor?: boolean;
  onSelect: (x: object) => void;
  valueKey?: string;
  customKey?: string;
  categoryKey?: string;
  groupKey?: string;
  boldMatch?: boolean;
  match?: string;
}

export const useSmartDropdownOption = ({
  valueKey = 'name',
  groupKey = 'group',
  boldMatch = false,
  match = '',
  ...otherprops
}: SmartDropdownOptionProps) => {
  return {
    valueKey,
    groupKey,
    boldMatch,
    match,
    ...otherprops,
  };
};

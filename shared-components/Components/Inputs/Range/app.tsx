import React, { FC } from "react";
import { Button } from "../Button/app";
import { Input } from "../Input/app";
import { Icon } from "../../Icon/app";
import theme from "../../../Styles/theme";
import { View, StyleSheet } from "react-native";
import { IconNames } from "../../Icon/Icon.hook";
import { useRange, RangeProps } from "./Range.hook";

export const Range: FC<RangeProps> = (props) => {
	const {
		confirmButton: {
			showButton,
			contentButton = <Icon name={IconNames.ok} color={theme.colors.background} size={25} />,
			...buttonSettings
		},
		minInput,
		maxInput,
	} = useRange(props);

	return (
		<>
			<View style={style.range}>
				<View style={style.input}>
					<Input {...minInput} />
				</View>
				<View style={style.input}>
					<Input {...maxInput} />
				</View>
				{showButton && <Button content={contentButton} {...buttonSettings} />}
			</View>
		</>
	);
};

const style = StyleSheet.create({
	range: {
		flexDirection: "row",
		alignItems: "center",
	},
	input: {
		flex: 1,
		marginRight: 10,
	},
});

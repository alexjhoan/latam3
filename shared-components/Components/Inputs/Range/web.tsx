import { FC } from "react";
import { Button } from "../web";
import { Input } from "../Input/web";
import { useRange, RangeProps } from "./Range.hook";
import { Icon } from "../../Icon/web";
import { IconNames } from "../../Icon/Icon.hook";
import { useTheme } from "../../../Styles/ThemeHook";

export const Range: FC<RangeProps> = props => {
	const { theme } = useTheme();
	const {
		confirmButton: {
			showButton,
			contentButton = <Icon name={IconNames.ok} color={theme.colors.backgroundColor} />,
			...buttonSettings
		},
		maxInput,
		minInput,
		status,
	} = useRange(props);

	return (
		<>
			<div className={"range-input" + (status ? " " + status : "")}>
				<div className="range-min">
					<Input {...minInput} />
				</div>
				<div className="range-max">
					<Input {...maxInput} />
				</div>
				{showButton && (
					<div className="range-button">
						<Button content={contentButton} {...buttonSettings} />
					</div>
				)}
			</div>
			<style jsx>{`
				.range-input {
					display: flex;
					align-items: flex-end;
					justify-content: space-between;
				}
				.range-input .range-min,
				.range-input .range-max {
					width: 100%;
					height: 100%;
				}
				.range-input .range-min {
					margin-right: ${theme.spacing.smSpacing}px;
				}
				.range-input .range-max {
					margin-right: ${showButton ? theme.spacing.smSpacing : 0}px;
				}
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</>
	);
};

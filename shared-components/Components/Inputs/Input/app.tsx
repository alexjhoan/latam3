import React, { FC, useRef, useEffect } from "react";
import { InputProps, useInput } from "./Input.hook";
import { TextInput, StyleSheet } from "react-native";
import theme from "../../../Styles/theme";

export const Input: FC<InputProps> = (props: InputProps) => {
	const { value, handleChange } = useInput({
		type: props.type,
		onChange: props.onChange,
		defaultValue: props.defaultValue,
	});

	const setKeyboardType = () => {
		switch (props.type) {
			case "number":
			case "currency":
				return "numeric";
			case "email":
				return "email-address";
			case "tel":
				return "phone-pad";
			default:
				return "default";
		}
	};

	const inputRef = useRef(null);
	useEffect(() => {
		if (typeof props.focused != "undefined") {
			props.focused ? inputRef.current.focus() : inputRef.current.blur();
		}
	}, [props.focused]);

	return (
		<>
			<TextInput
				placeholderTextColor="#ccc"
				ref={inputRef}
				style={style.input}
				defaultValue={value}
				placeholder={props.placeholder}
				onBlur={e => {
					if (typeof props.onBlur == "function")
						props.onBlur(e.nativeEvent.text);
				}}
				onFocus={props.onFocus}
				value={value}
				secureTextEntry={props.type == "password"}
				onChangeText={text => {
					handleChange(text);
				}}
				keyboardType={setKeyboardType()}
				editable={props.disabled}
			/>
		</>
	);
};

const style = StyleSheet.create({
	input: {
		borderColor: theme.colors.primary,
		borderRadius: theme.borderRadius,
		borderStyle: "solid",
		borderWidth: 1,
		paddingHorizontal: 20,
		paddingVertical: 8,
		fontSize: theme.fontSizes.appText,
		backgroundColor: "#fff",
	},
});

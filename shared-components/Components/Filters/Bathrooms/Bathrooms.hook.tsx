import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showBathrooms } from "../../../Utils/Functions";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_BATHROOMS_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		  id
		  name
		  options
	  `
);

export const FRAGMENT_BATHROOMS = new FragmentDefiner(
	"Filters",
	`
		  bathrooms {
		  ${FRAGMENT_BATHROOMS_OPTIONS.query()}
		  }
	  `
);

export const useBathrooms = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.bathrooms,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_BATHROOMS_OPTIONS, "bathrooms");

	const handleChange = newValue => {
		let res = [];
		if (newValue != null) {
			res = newValue.map(v => {
				return {
					value: v["value"],
					text: v["amount"] == 1 ? `${v["amount"]} Baño` : `${v["amount"]} Baños`,
				};
			});
		}
		filterChanged({ bathrooms: res });
	};

	const show = showBathrooms(currentFilters.property_type_id, currentFilters.operation_type_id);
	const type: SelectTypes = "multiple";

	let options = [];
	if (data) {
		options = data.options.map(element => {
			let cant = element.amount;
			return {
				value: element.amount,
				amount: cant + (cant >= data.options.length ? "+" : ""),
			};
		});
	}

	return {
		loading: loading,
		error: error,
		show,
		data: {
			onChange: handleChange,
			options: options,
			value: selectedValue,
			customKey: "value",
			valueKey: "amount",
			type: type,
		},
		labeled,
		label: data?.name,
	};
};

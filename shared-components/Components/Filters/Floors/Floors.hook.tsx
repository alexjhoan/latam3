import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { showFloors } from "../../../Utils/Functions";
import { useState, useEffect } from "react";
import { useFilters } from "../Filters.hook";

export const FRAGMENT_FLOORS_OPTIONS = new FragmentDefiner(
	"Filter",
	`
      id
      name
      options
	`
);
export const FRAGMENT_FLOORS = new FragmentDefiner(
	"Filters",
	`
      floors {
        ${FRAGMENT_FLOORS_OPTIONS.query()}
      }
	`
);
export interface FloorProps {
	labeled?: boolean;
	collapsable?: boolean;
	filterChanged: ({}: any) => void;
}
export const useFloors = ({ filterChanged }: FloorProps) => {
	const { filters, filtersTags } = useFilters();
	const [floors, setFloors] = useState(filters?.floors || []);
	const { data, loading } = useReadFragment(FRAGMENT_FLOORS_OPTIONS, "floors");
	const [firstTime, setFirstTime] = useState(true);

	useEffect(() => {
		if (!firstTime) {
			updateFilters();
		} else {
			setFirstTime(false);
		}
	}, [floors]);

	const updateFilters = () => {
		let filtered = data.options
			.filter(o => floors.includes(o["value"]))
			.map(v => {
				return { value: v["value"], text: v["text"] };
			});
		filterChanged({ floors: filtered });
	};

	const show = showFloors(filters?.property_type_id);

	const type: SelectTypes = "multiple";
	return {
		floors: {
			value: floors,
			set: setFloors,
			loading,
			options: data?.options,
		},
		show,
	};
};

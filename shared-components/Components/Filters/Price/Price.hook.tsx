import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useContext, useEffect, useState } from "react";

import { formatMoney } from "../../../Utils/Functions";
import { useFilters } from "../Filters.hook";
import { useTranslation } from "react-i18next";
import { ConfigStateContext } from "../../../Contexts/Configurations/context";

const FRAGMENT_PRICES_COMMONEXPENSES_OPTIONS = new FragmentDefiner(
	"Filter",
	` id
	name
	options `
);

export const FRAGMENT_PRICES_COMMONEXPENSES = new FragmentDefiner(
	"Filters",
	` price {
		${FRAGMENT_PRICES_COMMONEXPENSES_OPTIONS.query()}
	}
    commonExpenses{
		${FRAGMENT_PRICES_COMMONEXPENSES_OPTIONS.query()}
    }`
);

export interface PriceFilterProps {
	filterChanged: ({}: any) => void;
	labeled?: boolean;
	collapsable?: boolean;
}

export const usePrice = ({ filterChanged }: PriceFilterProps) => {
	const {
		filters,
		filtersTags: { currencyID },
	} = useFilters();
	const { currency: confCurrency } = useContext(ConfigStateContext);
	const { t } = useTranslation();
	const { loading, data } = useReadFragment(FRAGMENT_PRICES_COMMONEXPENSES_OPTIONS, "price");
	const [minPrice, setMinPrice] = useState(filters.minPrice);
	const [maxPrice, setMaxPrice] = useState(filters.maxPrice);
	const [currency, setCurrency] = useState<{ text; value } | null>(currencyID);
	const [commonEx, setCommonEx] = useState(filters.commonExpenses);

	useEffect(() => setMinPrice(filters.minPrice), [filters.minPrice]);
	useEffect(() => setMaxPrice(filters.maxPrice), [filters.maxPrice]);
	useEffect(() => {
		if (filters.minPrice || filters.maxPrice) setCurrency(currencyID);
	}, [currencyID]);
	useEffect(() => setCommonEx(filters.commonExpenses), [filters.commonExpenses]);
	useEffect(() => {
		if (!minPrice && !maxPrice) {
			if (filters.operation_type_id == 2 || filters.operation_type_id == 4)
				setCurrency({ text: confCurrency.name, value: Number(confCurrency.id) });
			else if (filters.operation_type_id == 1) setCurrency({ text: "U$S", value: 1 });
		}
	}, [filters.operation_type_id, minPrice, maxPrice]);

	const setPriceFilter = () => {
		filterChanged({
			minPrice:
				minPrice > 0
					? {
							text: `${t("Desde")} ${currency.text} ${formatMoney(minPrice)}`,
							value: minPrice,
					  }
					: null,
			maxPrice:
				maxPrice > 0
					? {
							text: `${t("Hasta")} ${currency.text} ${formatMoney(maxPrice)}`,
							value: maxPrice,
					  }
					: null,
			currencyID: currency,
			commonExpenses: commonEx
				? { value: commonEx, text: t("Incluyendo Gastos Comunes") }
				: null,
		});
	};

	return {
		minPrice: { value: minPrice, set: setMinPrice },
		maxPrice: { value: maxPrice, set: setMaxPrice },
		currency: { ...currency, set: setCurrency, options: data?.options, loading },
		commonEx: { value: commonEx, set: setCommonEx },
		showCommonExpenses: filters.operation_type_id == 2,
		saveFilter: setPriceFilter,
	};
};

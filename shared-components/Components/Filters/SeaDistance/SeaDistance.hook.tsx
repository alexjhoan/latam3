import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { SelectTypes } from "../../Inputs/Selects/Selects.hook";
import { showSeaDistance } from "../../../Utils/Functions";
import { useCountrySelection } from "../../CountrySelect/Country.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_SEADISTANCE_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		id
		name
		options
	`
);

export const FRAGMENT_SEADISTANCE = new FragmentDefiner(
	"Filters",
	`
		seaDistance {
			${FRAGMENT_SEADISTANCE_OPTIONS.query()}
		}
	`
);

export const useSeaDistance = props => {
	const { filters, filtersTags, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.seaDistanceID,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_SEADISTANCE_OPTIONS, "seaDistance");

	const handleChange = newValue => {
		const resSeaDistance = newValue ? { value: newValue.id, text: newValue.nombre } : null;
		filterChanged({ seaDistanceID: resSeaDistance });
	};
	const show = showSeaDistance(currentFilters?.operation_type_id, 1);

	const type: SelectTypes = "radio";
	return {
		loading: loading,
		error: error,
		show: show,
		data: {
			onChange: handleChange,
			options: data?.options,
			value: selectedValue,
			valueKey: "nombre",
			type: type,
			maxCountOptions: 4,
		},
		labeled,
		label: data?.name,
	};
};

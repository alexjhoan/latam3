import { useQuery, useMutation } from "@apollo/client";
import { getOnlyValues } from "./Filters.hook";
import {
  QUERY_SAVED_FILTERS,
  QUERY_LOCATIONS_SAVE,
  TYPENAME_SAVED_FILTERS,
} from "../../_toBeDeleted_ApolloResolvers/SavedFilters";
import { MUTATION_LOCAL_STORE } from "../../_toBeDeleted_ApolloResolvers";

const useSavedFilters = () => {
  const {
    data: dataSavedFilters,
    loading: loadingSavedFilters,
    error: errorSavedFilters,
  } = useQuery(QUERY_SAVED_FILTERS);

  const {
    data: dataSavedLocations,
    loading: loadingSavedLocations,
    error: errorSavedLocations,
  } = useQuery(QUERY_LOCATIONS_SAVE);

  const [MUTATOR_LOCAL_STORE] = useMutation(MUTATION_LOCAL_STORE);

  const saveFilters = (filters) => {
    return MUTATOR_LOCAL_STORE({
      variables: {
        new_state: getOnlyValues(filters),
        query: QUERY_SAVED_FILTERS,
      },
    });
  };

  // save location searched
  const saveLocation = (location) => {
    let newLocations = dataSavedLocations
      ? dataSavedLocations[TYPENAME_SAVED_FILTERS]["locations"]
      : [];

    newLocations = newLocations.filter(
      (l) => l.id != location["id"] || l["__typename"] != location["__typename"]
    );

    newLocations = [{ ...location }, ...newLocations];
    if (newLocations.length > 5) newLocations.pop();

    return MUTATOR_LOCAL_STORE({
      variables: {
        new_state: { locations: newLocations },
        query: QUERY_LOCATIONS_SAVE,
      },
    });
  };

  return {
    loading: loadingSavedFilters || loadingSavedLocations,
    data: {
      dataSavedFilters: dataSavedFilters?.[TYPENAME_SAVED_FILTERS],
      dataSavedLocations:
        dataSavedLocations?.[TYPENAME_SAVED_FILTERS]["locations"],
    },
    error: errorSavedFilters || errorSavedLocations,
    saveFilters,
    saveLocation,
  };
};

export { useSavedFilters };

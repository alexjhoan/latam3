import { CheckboxSelect } from "../../Inputs/app";
import { usePropertyType, PropertyTypeProps } from "./PropertyType.hook";
import { Label } from "../../Inputs/Label/app";
import { Text, View } from "react-native";
import React from "react";

export const PropertyType = (props: PropertyTypeProps) => {
	const {
		loading,
		data,
		error,
		show,
		labeled,
		label,
		inputType,
	} = usePropertyType(props);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>Error</Text>;

	return (
		<View>
			<Label showLabel={labeled} labelContent={label}>
				{inputType == "checkboxselect" ? (
					<CheckboxSelect {...data} maxCountOptions={5} />
				) : (
					<Text>No esta la componente CustomSelect</Text>
					// <CustomSelect {...data} placeholder={label} />
				)}
			</Label>
		</View>
	);
};

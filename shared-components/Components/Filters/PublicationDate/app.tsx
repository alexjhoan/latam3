import { FilterComponentType } from "../Filters.hook";
import { usePublicationDate } from "./PublicationDate.hook";
import { Label } from "../../Inputs/Label/app";
import { CheckboxSelect } from "../../Inputs/app";

import React from "react";
import { View, Text } from "react-native";

const PublicationDate = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = usePublicationDate(
		props
	);

	if (!show) return null;
	if (loading) return null;
	if (error) return <Text>error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<CheckboxSelect {...data} />
			</Label>
		</View>
	);
};

export { PublicationDate };

import { Checkbox, Col, Collapse, Row, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { usePropStates } from "./PropStates.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const PropStates = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		labeled,
		data: { options, onChange, value = [] },
	} = usePropStates(props);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const handleChecked = checked => {
		const res = options.filter(o => checked.includes(o.id));
		onChange(res);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={value} onChange={handleChecked}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{options?.map(o => {
					return (
						<Col span={24} key={`key_${o.id}_propertyState`}>
							<Checkbox value={o.id}>{t(o.name)}</Checkbox>
						</Col>
					);
				})}
			</Row>
		</Checkbox.Group>
	);

	const label = "Estado";

	return (
		<div className="filter property-state-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={value?.length >= 1 ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value?.length >= 1 && extraIcon()}
						disabled={loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title level={4} disabled={loading}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};

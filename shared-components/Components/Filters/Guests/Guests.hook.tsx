import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showGuests } from "../../../Utils/Functions";
import { inputType } from "../../Inputs/Input/Input.hook";
import { useFilters } from "../Filters.hook";

const FRAGMENT_GUESTS_OPTIONS = new FragmentDefiner(
	"Filter",
	` id
	  name `
);

export const FRAGMENT_GUESTS = new FragmentDefiner(
	"Filters",
	`guests {
		  ${FRAGMENT_GUESTS_OPTIONS.query()}
	  }`
);

export const useGuests = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.guests,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(FRAGMENT_GUESTS_OPTIONS, "guests");

	const onChange = newGuestsAmount => {
		const resGuests = newGuestsAmount
			? { value: newGuestsAmount, text: newGuestsAmount }
			: null;
		filterChanged({ guests: resGuests });
	};

	const show = showGuests(currentFilters.operation_type_id);
	const type: inputType = "number";
	return {
		show,
		loading: loading,
		error: error,
		data: {
			defaultValue: selectedValue,
			name: data?.name,
			search: onChange,
			type: type,
		},
		labeled,
		label: data?.name,
	};
};

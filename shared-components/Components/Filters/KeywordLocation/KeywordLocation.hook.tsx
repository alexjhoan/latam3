import { useState, useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import { useFilters } from "../Filters.hook";
import { isTemporal } from "../../../Utils/Functions";
import { FRAGMENT_PROPERTY_CARD } from "../../Property/PropertyCard/PropertyCard.hook";

const KEYWORD_LOCATION_QUERY = gql`
	query Location($strSearch: String!) {
		searchLocation(searchTerm: $strSearch) {
			... on Estate {
				id
				name
			}
			... on Neighborhood {
				id
				name
				estate {
					id
					name
				}
			}
			__typename
		}
	}
`;
const REF_QUERY = gql`
	query searchByRef($code: String!) {
		searchByRef(refCode: $code) {
			${FRAGMENT_PROPERTY_CARD.query()}
		}
	}
`;

export interface KeywordLocationProps {
	className?: string;
	filterChanged: ({}: any) => void;
}

export const useKeywordLocation = ({ filterChanged }: KeywordLocationProps) => {
	// hook setup
	const { filters } = useFilters();
	const [keyword, setKeyWord] = useState<string>("");
	const [location, setLocation] = useState(null);

	useEffect(() => updateFilters(), [location]);

	// hook functions
	const onSelect = (val, opt) => {
		setLocation(opt);
		setKeyWord(val);
	};

	const onSearch = (val: string) => {
		setKeyWord(val);
		if (val.length > 0) searchQuery({ variables: { strSearch: val } });
		if (val.length >= 5) refQuery({ variables: { code: val } });
	};

	const [searchQuery, { data, loading }] = useLazyQuery(KEYWORD_LOCATION_QUERY);

	const [refQuery, { data: dataRef, loading: loadingRef }] = useLazyQuery(REF_QUERY);

	const updateFilters = () => {
		if (location) {
			if (location.__typename == "keyword") {
				filterChanged({
					neighborhood_id: null,
					estate_id: null,
					searchstring: { text: keyword, value: keyword },
				});
			} else if (location.estate)
				filterChanged({
					neighborhood_id: [{ text: location.name, value: location.id }],
					estate_id: { text: location.estate.name, value: location.estate.id },
					searchstring: null,
				});
			else
				filterChanged({
					neighborhood_id: null,
					estate_id: { text: location.name, value: location.id },
					searchstring: null,
				});
		}
	};

	// hook output API
	return {
		keyword,
		location,
		show: true,
		onSearch,
		onSelect,
		searchResults: data,
		refResults: dataRef,
		searchLoading: loading || loadingRef,
	};
};

import "./styles.less";

import { Skeleton, Typography } from "antd";

import { useLocationTag } from "./LocationTag.hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Text } = Typography;

export function LocationTag({ id, showAddress = false, mode = "auto" }) {
	const { address, neighborhood, estate, loading, error } = useLocationTag({ id, mode });
	const { theme } = useTheme();

	if (loading) return <LocationTagSkeleton />;
	if (error) return null;

	return (
		<>
			<Text className="property-location-tag" ellipsis={true}>
				{showAddress && address && <Text>{address}, </Text>}
				<Text>
					{neighborhood}, {estate}
				</Text>
			</Text>
			<style jsx global>{`
				// NO BORRAR, ESTAMOS PROBANDO 
				// .property-card .property-location-tag {
				// 	font-size: ${theme.fontSizes.xsFontSize};
				// 	line-height: ${theme.fontSizes.baseLineHeight};
				// 	text-transform: uppercase;
				// }
				.property-card .property-location-tag {
					font-size: ${theme.fontSizes.baseFontSize};
					line-height: ${theme.fontSizes.baseLineHeight};
				}
			`}</style>
		</>
	);
}

export const LocationTagSkeleton = () => {
	return <Skeleton title paragraph={false} className="location-tag-skeleton" />;
};

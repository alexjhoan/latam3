import { Popover } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "../../../../Styles/ThemeHook";

export const CustomCell = ({ date, month, occupancies }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const marked = occupancies.some(elem => elem == date.format("YYYY-MM-DD"));
	if (month.month() != date.month()) return null;

	return (
		<>
			<Popover content={marked ? t("No disponible") : t("Disponible")}>
				<div className={"calendar-cell" + (marked ? " calendar-cell-marked" : "")}>
					{date.format("DD")}
				</div>
			</Popover>
			<style jsx>{`
				:global(.availability-calendar .ant-picker-cell) {
					padding: ${theme.spacing.xsSpacing}px 0;
				}
				.calendar-cell {
					width: calc(100% - ${2 * theme.spacing.xsSpacing}px);
					padding: ${theme.spacing.smSpacing}px;
					background-color: ${theme.colors.backgroundColorAternative};
					border: 1px solid ${theme.colors.borderColor};
					color: ${theme.colors.textColor};
				}
				.calendar-cell.calendar-cell-marked {
					background-color: ${theme.colors.errorColor};
					color: ${theme.colors.backgroundColor};
					border-color: ${theme.colors.errorColor};
				}
			`}</style>
		</>
	);
};

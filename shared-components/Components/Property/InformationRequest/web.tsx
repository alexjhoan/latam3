import "./styles.less";

import { Col, Divider, Form, Row, Tabs, Typography } from "antd";
import React, { ReactElement, useRef, useState } from "react";

import { PropComponentMode } from "../PropertyInterfaces";
import { ScheduleVisitForm } from "./ScheduleVisitForm/web";
import { TextForm } from "./TextForm/web";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Paragraph } = Typography;

interface InformationRequestProps {
	id: number | string;
	children?: ReactElement;
	onComplete?: () => void;
	showDisclaimer?: boolean;
	identifier?: number;
	mode?: PropComponentMode;
}

export const InformationRequest = ({
	id,
	children,
	onComplete = null,
	showDisclaimer = true,
	identifier = 1,
	mode = "auto",
}: InformationRequestProps) => {
	const { TabPane } = Tabs;
	const { t } = useTranslation();
	const { theme } = useTheme();

	const form_span = children ? { span: 24, lg: 14 } : { span: 24 };

	const children_holder = children ? (
		<Col span={24} lg={10}>
			{children}
		</Col>
	) : null;

	const disclaimer = false ? (
		<>
			<Col
				span={24}
				lg={{ push: children ? 10 : 0, span: children ? 14 : 24 }}
				className={"information-request-disclaimer"}>
				<Paragraph type={"secondary"}>
					{t(
						"Al enviar una consulta, usted acepta que InfoCasas y los profesionales" +
							" de bienes raíces pueden contactarlo por teléfono / mensaje de texto sobre" +
							" su consulta, lo que puede implicar el uso de medios automatizados." +
							" También acepta nuestros" +
							" de InfoCasas no respalda a ningún profesional de bienes raíces"
					)}
				</Paragraph>
			</Col>
		</>
	) : null;

	const [leadForm] = Form.useForm();
	const [schedulevisitForm] = Form.useForm();

	const TABKEY_LEADFORM = "TABKEY_LEADFORM";
	const TABKEY_SCHEDULEVISITFORM = "TABKEY_SCHEDULEVISITFORM";
	const [activeTab, setActiveTab] = useState(TABKEY_LEADFORM);

	return (
		<>
			<Tabs
				className={"property-information-request " + activeTab}
				style={{ overflow: "visible" }}
				activeKey={activeTab}
				type="card"
				size={"small"}
				onTabClick={key => {
					if (key === activeTab) return;
					else if (key === TABKEY_SCHEDULEVISITFORM)
						schedulevisitForm.setFieldsValue(leadForm.getFieldsValue());
					else if (key === TABKEY_LEADFORM)
						leadForm.setFieldsValue(schedulevisitForm.getFieldsValue());
					setActiveTab(key);
				}}>
				<TabPane tab={t("Consultar")} key={TABKEY_LEADFORM}>
					<Row gutter={[theme.spacing.xxlSpacing, 0]}>
						{children_holder}
						<Col {...form_span}>
							<TextForm
								identifier={identifier}
								id={id}
								form={leadForm}
								onComplete={onComplete}
								mode={mode}
							/>
						</Col>

						{disclaimer}
					</Row>
				</TabPane>
				<TabPane tab={t("Agendar Visita")} key={TABKEY_SCHEDULEVISITFORM}>
					<Row gutter={[theme.spacing.xxlSpacing, 0]}>
						{children_holder}
						<Col {...form_span}>
							<ScheduleVisitForm
								id={id}
								form={schedulevisitForm}
								onComplete={onComplete}
								identifier={identifier}
								mode={mode}
							/>
						</Col>
						{disclaimer}
					</Row>
				</TabPane>
			</Tabs>

			<style jsx global>{`
				.col-information-request-small-left
					.property-information-request
					.ant-tabs-content-holder {
					box-shadow: rgba(59, 65, 68, 0.18) 0px 17px 21px -1px;
				}

				.col-information-request-small-left
					.property-information-request
					.ant-tabs-nav-list {
					width: 100%;
				}

				.col-information-request-small-left .property-information-request .ant-tabs-tab {
					-webkit-box-flex: 1;
					flex: 1;
					display: block;
					text-align: center;
				}

				.property-information-request
					.ant-tabs-nav
					.ant-tabs-tab:not(.ant-tabs-tab-active) {
					border-color: ${theme.colors.backgroundColor};
					background: ${theme.colors.backgroundColor};
				}

				.property-information-request .ant-tabs-nav .ant-tabs-tab {
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px !important;
				}
				.property-information-request .ant-tabs-content-holder {
					padding: ${theme.spacing.mdSpacing}px;
					border: 1px solid ${theme.colors.borderColor};
					border-radius: ${theme.spacing.smSpacing}px;
					border-top-left-radius: ${theme.spacing.smSpacing}px;
				}
				.property-information-request.TABKEY_LEADFORM .ant-tabs-content-holder {
					border-top-left-radius: ${0}px;
				}
				.pd-main-content
					.property-information-request.TABKEY_SCHEDULEVISITFORM
					.ant-tabs-content-holder {
					border-top-right-radius: ${0}px;
				}
				.pd-ir-container-big .property-information-request .ant-tabs-content-holder {
					padding: ${theme.spacing.lgSpacing}px;
					border-color: ${theme.colors.backgroundColorAternative};
					background-color: ${theme.colors.backgroundColorAternative};
				}
				.property-information-request .ant-tabs-nav .ant-tabs-tab-active::before {
					border-color: ${theme.colors.backgroundColor};
				}
				.pd-ir-container-big
					.property-information-request
					.ant-tabs-nav
					.ant-tabs-tab-active,
				.pd-ir-container-big
					.property-information-request
					.ant-tabs-nav
					.ant-tabs-tab-active::before {
					background: ${theme.colors.backgroundColorAternative};
					border-color: ${theme.colors.backgroundColorAternative};
				}

				.pd-ir-container-big .property-information-request .ant-tabs-nav .ant-tabs-tab {
					padding: ${theme.spacing.mdSpacing}px ${theme.spacing.lgSpacing}px !important;
				}
				.property-information-request .information-request-disclaimer {
					margin-bottom: -${theme.spacing.mdSpacing / 2}px;
				}
				.property-information-request .information-request-disclaimer div.ant-typography {
					color: ${theme.colors.textSecondaryColor};
					font-size: ${theme.spacing.mdSpacing}px;
				}

				.note-popover {
					font-size: ${theme.fontSizes.smFontSize};
				}
			`}</style>
		</>
	);
};

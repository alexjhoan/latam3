import { Button, Checkbox, Col, Form, Input, Row } from "antd";
import React, { useEffect } from "react";

import { LeadButtons } from "../LeadButtons/web";
import { Rule } from "antd/lib/form";
import { useContact } from "../hook";
import { useTranslation } from "react-i18next";
import { useUser } from "../../../User/User.hook";
import { useGoogleAnalytics } from "../../../../GlobalHooks/web/GoogleAnalytics.hook";
import { useRouter } from "next/router";

const { TextArea } = Input;

export const TextForm = ({ id, onComplete = null, form, identifier, mode }) => {
	const { user } = useUser();
	const Router = useRouter();
	const { sendContact, resetForm, loading, formCompleted, disabledEmail } = useContact({
		id,
		onComplete,
		type: "lead",
		mode,
	});
	const GA = useGoogleAnalytics();
	const { t } = useTranslation();
	const reset = () => {
		resetForm();
		form.resetFields();
	};

	useEffect(() => {
		Router.events.on("routeChangeComplete", () => reset());
	}, []);

	useEffect(() => {
		if (user.data) {
			form.setFieldsValue({
				nombre: user.data.me.name,
				telefono: user.data.me.phone,
				email: user.data.me.email,
			});
		}
	}, [user]);

	useEffect(() => {
		if (formCompleted) {
			GA.Event({
				category: "formulario",
				action: "formulario ic2",
				label: "text form",
				value: 1,
			});
		}
	}, [formCompleted]);

	const validateMessages = {
		required: "El ${name} es requerido!",
		types: {
			email: "El ${name} no es valido!",
			number: "El ${name} no es un numero valido!",
		},
	};

	const validateRules: { [x: string]: Rule[] } = {
		nombre: [{ required: true }],
		telefono: [
			{
				required: true,
				pattern: new RegExp("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-s./0-9]*$"),
				message: t("Teléfono no válido"),
			},
		],
		email: [{ required: true, type: "email" }],
		mensaje: [{ required: true }],
	};

	const submitForm = () => {
		form.validateFields()
			.then(values => sendContact(values))
			.catch(info => console.log("Validate Failed:", info));
	};

	return (
		<>
			<Form
				form={form}
				name={"leadForm_" + identifier}
				initialValues={{
					nombre: user.data?.me.name,
					telefono: user.data?.me.phone,
					email: user.data?.me.email,
					mensaje: "",
				}}
				scrollToFirstError
				validateMessages={validateMessages}>
				<Row gutter={12}>
					<Col span={12}>
						<Form.Item name="nombre" hasFeedback rules={validateRules.nombre}>
							<Input className="secondary" placeholder={t("Nombre")} />
						</Form.Item>
					</Col>
					<Col span={12}>
						<Form.Item name="telefono" rules={validateRules.telefono}>
							<Input className="secondary" placeholder={t("Teléfono")} />
						</Form.Item>
					</Col>
				</Row>
				<Form.Item name="email" hasFeedback rules={validateRules.email}>
					<Input
						className="secondary"
						disabled={disabledEmail}
						placeholder={t("Email")}
					/>
				</Form.Item>
				<Form.Item rules={validateRules.mensaje} name="mensaje">
					<TextArea
						className="secondary"
						rows={7}
						placeholder={t("Escribe tu consulta")}
					/>
				</Form.Item>
				<Form.Item name="financing_information" valuePropName="checked">
					<Checkbox>{t("Quiero info. sobre financiamiento")}</Checkbox>
				</Form.Item>
				<Form.Item>
					<LeadButtons id={id} mode={mode}>
						<Button
							type="primary"
							loading={loading}
							disabled={formCompleted}
							className="btn-send-lead"
							onClick={submitForm}>
							{formCompleted
								? t("Enviado")
								: loading
								? t("Enviando")
								: t("Enviar Consulta")}
						</Button>
					</LeadButtons>
				</Form.Item>
			</Form>
		</>
	);
};

import { Button, Carousel, Space, Typography } from "antd";
import React, { useRef, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useTheme } from "../../../../../Styles/ThemeHook";
import { CarouselArrow } from "../../../LazyImageGallery/CarouselArrow/web";
const { Text } = Typography;
import "./styles.less";

export const DateSelector = ({ onSelect, selected }) => {
	const { theme } = useTheme();
	return (
		<>
			<Carousel
				className={"date-selector"}
				dots={false}
				arrows={true}
				infinite={false}
				autoplay={false}
				slidesToShow={4}
				slidesToScroll={4}
				nextArrow={<CarouselArrow side={"right"} type={"filled"} position={"outside"} />}
				prevArrow={<CarouselArrow side={"left"} type={"filled"} position={"outside"} />}>
				{[0, 1, 2, 3, 3, 4, 5, 6].map((o, i) => {
					const d = new Date();
					d.setHours(0, 0, 0, 0);
					d.setDate(d.getDate() + o);
					return (
						<DateButton
							key={"date_button_" + i}
							onClick={() => onSelect(d)}
							date={d}
							selected={selected ? selected.getTime() == d.getTime() : o == 0}
						/>
					);
				})}
			</Carousel>
			<style jsx global>{`
				.ant-carousel .date-selector .slick-slide {
					padding: 0 ${theme.spacing.xsSpacing}px;
				}
				.ant-carousel .date-selector .slick-list {
					margin: 0 -${theme.spacing.xsSpacing}px;
				}
			`}</style>
		</>
	);
};

export const DateButton = ({ date, onClick, selected, key }) => {
	const { t } = useTranslation();
	const ref = useRef(null);
	const { theme } = useTheme();

	useEffect(() => {
		if (selected) ref.current.click();
	}, []);

	return (
		<Button
			onClick={onClick}
			className={"secondary"}
			style={{
				height: "auto",
				width: "100%",
				...(selected
					? { borderWidth: "2px", borderColor: theme.colors.secondaryColor }
					: { margin: "1px 0" }),
			}}
			ref={ref}>
			<Space direction={"vertical"} size={0} style={{ textAlign: "center" }}>
				<Text>{t("DAY_" + date.getDay() + "_SHORT")}</Text>
				<Text strong={true} style={{ fontSize: "26px" }}>
					{date.getDate()}
				</Text>
				<Text>{t("MONTH_" + date.getMonth() + "_SHORT")}</Text>
			</Space>
		</Button>
	);
};

import { gql, useQuery } from "@apollo/client";
import { FRAGMENT_PRICETAG } from "../PriceTag/PriceTag.hook";
import { FRAGMENT_M2TAG } from "../TypologyTag/M2Tag/M2Tag.hook";
import dayjs from "dayjs";
import "dayjs/locale/es";
import relativeTime from "dayjs/plugin/relativeTime";
import updateLocale from "dayjs/plugin/updateLocale";
import { parseMoney } from "../../../Utils/Functions";

const QUERY_RELATED = gql`
  query similar($property_id: Int!, $results_per_page: Int!, $page: Int) {
    similarByPropertyId(property_id: $property_id, first: $results_per_page, page: $page) {
      data {
        __typename
        id
        title
        link
        created_at
        price_amount_usd
        bedrooms
        bathrooms
        ${FRAGMENT_M2TAG.query()}
      }
    }
  }
`;

export const useBigDataProperties = ({ property_id }) => {
	dayjs.locale("es");
	dayjs.extend(relativeTime);
	dayjs.extend(updateLocale);
	dayjs.updateLocale("es", {
		relativeTime: {
			past: "%s",
			d: "hoy",
			ay: "ayer",
			dd: "hace %d dias",
			M: "hace 1 mes",
			MM: "hace %d meses",
			y: "hace un año",
			yy: "hace %d años",
		},
	});

	const results_per_page = 15;
	const page = 1;

	const { loading, data, error } = useQuery(QUERY_RELATED, {
		variables: {
			property_id,
			results_per_page,
			page,
		},
		skip: property_id == undefined,
	});

	const result = data
		? data.similarByPropertyId.data.map(e => {
				let result = { ...e };
				result.currentProperty = e.id == property_id;
				result.price_m2 = Math.round(e.price_amount_usd / e.m2);
				result.property = {
					title: e.title,
					url: e.link,
					id: e.id,
				};
				result.fromNow = { id: e.id, value: dayjs(e.created_at).fromNow(true) };
				result.key = e.id;
				result.bedrooms = { id: e.id, value: e.bedrooms };
				result.bathrooms = { id: e.id, value: e.bathrooms };
				result.m2 = { id: e.id, value: e.m2 };
				result.price_amount_usd = {
					id: e.id,
					value: `U$S ${parseMoney(e.price_amount_usd)}`,
				};
				result.price_m2 = { id: e.id, value: result.price_m2 };
				return result;
		  })
		: null;

	return {
		loading,
		data: result,
		error,
	};
};

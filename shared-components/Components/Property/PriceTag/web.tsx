import "./styles.less";

import { ArrowDownOutlined, ArrowUpOutlined } from "@ant-design/icons";
import { Col, Popover, Row, Skeleton, Space, Typography } from "antd";

import { PropComponentMode } from "../PropertyInterfaces";
import React from "react";
import { TimeAgo } from "../../Days/TimeAgo/web";
import { usePriceTag } from "./PriceTag.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text, Paragraph } = Typography;

interface PriceTagProps {
	id: string;
	showExpenses?: boolean;
	threeDigitsFormat?: boolean;
	showOperationType?: boolean;
	showPriceVariation?: boolean;
	showFromText?: boolean;
	mode?: PropComponentMode;
	shortExpensesText?: boolean;
	skeleton?: string;
}

export function PriceTag({
	id,
	showExpenses = true,
	threeDigitsFormat = false,
	showOperationType = false,
	showPriceVariation = true,
	showFromText = true,
	shortExpensesText = true,
	mode = "auto",
	skeleton = "front",
}: PriceTagProps) {
	const { data, loading, error } = usePriceTag({
		id,
		threeDigitsFormat,
		showFromText,
		shortExpensesText,
		mode,
	});
	const { theme } = useTheme();
	const { t } = useTranslation();

	if (loading)
		return skeleton !== "front" ? (
			<PriceTagSkeleton direction="horizontal" />
		) : (
			<PriceTagSkeleton direction="vertical" />
		);
	if (error) return null;

	const PriceVariationIndicator = () => {
		const content = () => {
			return (
				<Paragraph>
					<Text>
						<TimeAgo date={data.price_variation.date} bold capitalize />
						{" " + t("el precio del inmueble") + " "}
						<Text strong>
							{data.price_variation.difference > 0 ? t("subió") : t("bajó")}
						</Text>
						{" " + t("un") + " "}
						<Text strong>{data.price_variation.percentage}%</Text>.
						<br />
						{data.price_variation.amount && (
							<Text>
								{t("Precio Anterior")}: {data.price_variation.text}
							</Text>
						)}
					</Text>
				</Paragraph>
			);
		};

		return (
			<Popover placement="bottom" content={content()} title={t("Cambio de Precio")}>
				{data.price_variation.difference <= 0 ? (
					<ArrowDownOutlined
						style={{ color: theme.colors.successColor, marginRight: 10 }}
					/>
				) : (
					<ArrowUpOutlined style={{ color: theme.colors.errorColor, marginRight: 10 }} />
				)}
			</Popover>
		);
	};

	if (!data?.price || data?.price.amount == 0 || data.price.isTemp && data.price.amount < 5) {
		return (
			<Text className="price" strong>
				{" "}
				{t("Consultar")}{" "}
			</Text>
		);
	}

	return (
		<>
			<Row gutter={8} align={"middle"} className="property-price-tag">
				<Col style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
					{showPriceVariation && data.price_variation && <PriceVariationIndicator />}
					<Text className="price" strong>
						{data.price.text}
					</Text>
				</Col>
				{showOperationType && (
					<Col span={24}>
						<Text type={"secondary"} className="operation_type">
							{t("Precio de")} {data.operationType.text}
						</Text>
					</Col>
				)}
				{showExpenses && !data.commonExpenses.hide && (
					<Col>
						<Text type={"secondary"} className="commonExpenses">
							{data.commonExpenses.text}
						</Text>
					</Col>
				)}
			</Row>
			<style jsx global>{`
				.property-card .property-price-tag .price {
					font-size: ${theme.fontSizes.smTitle};
					line-height: ${theme.fontSizes.smLineHeight};
				}
				.property-card .property-price-tag .commonExpenses {
					font-size: ${theme.fontSizes.smFontSize};
				}
				.pd-main-content .property-price-tag .price {
					font-size: ${theme.fontSizes.lgTitle};
					line-height: ${theme.fontSizes.lgLineHeight};
				}
				.pd_affix_top .property-price-tag .price {
					font-size: ${theme.fontSizes.xsTitle};
					line-height: ${theme.fontSizes.xsLineHeight};
				}
			`}</style>
		</>
	);
}

export const PriceTagSkeleton = ({ direction }) => {
	return (
		<Space align={"end"} className="price-tag-skeleton" direction={direction}>
			<Skeleton.Button active />
			<Skeleton.Button active />
		</Space>
	);
};

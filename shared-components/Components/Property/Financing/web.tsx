import { BBVAFinancing } from "./BBVAFinancing/web";
import { MapfreFinancing } from "./MapfreFinancing/web";
import { isRent, isSell, UY } from "../../../Utils/Functions";
import { useTheme } from "../../../Styles/ThemeHook";

export const Financing = ({operationType, id, countryId, propertyType}) => {
  const { theme } = useTheme();

  return (
    <>
      { isSell(operationType) && countryId == UY ? <BBVAFinancing id={id} /> : null }
      { isRent(operationType) && countryId == UY ? <MapfreFinancing id={id} propertyType={propertyType} /> : null}
      <style jsx global>{`
        .property-financing-box {
          background-color: ${theme.colors.backgroundColorAternative};
          padding: ${theme.spacing.mdSpacing}px;
					border-radius: ${theme.spacing.smSpacing}px;
        }
      `}</style>
    </>
  )
}
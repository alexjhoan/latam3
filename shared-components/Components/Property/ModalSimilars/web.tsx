import { useTranslation } from "react-i18next";
import { Col, Modal, Row, Typography } from "antd";
import { PropertySimilarsCards } from "../../../ViewFragments/PropertyDetails/PropertySimilars/PropertySimilarsCards/web";
import { useTheme } from "../../../Styles/ThemeHook";
import "./styles.less";
import { FullScreen } from "../../FullScreen/web";

export const ModalSimilars = ({ visible, onCancel, property_id, children, mode }) => {
	const { t } = useTranslation();
	const { theme } = useTheme();

	return (
		<Modal
			visible={visible}
			footer={null}
			onCancel={onCancel}
			width={420}
			className={"similar-modal"}>
			<FullScreen className="similar-modal-container">
				<Row
					justify={"center"}
					align={"middle"}
					gutter={[0, theme.spacing.mdSpacing]}
					className="similar-modal-content">
					<Col span={24}>{children}</Col>
					<Col span={24} style={{ textAlign: "center" }}>
						<Typography.Text strong>
							{t("También mirá estas propiedades") + ":"}
						</Typography.Text>
					</Col>
					<Col span={24}>
						<PropertySimilarsCards
							onPropertyClick={onCancel}
							title={null}
							id={property_id}
							mode={mode}
							grid={{ xs: 1, sm: 1, md: 1, lg: 1, xl: 1, xxl: 1 }}
						/>
					</Col>
				</Row>
			</FullScreen>
		</Modal>
	);
};

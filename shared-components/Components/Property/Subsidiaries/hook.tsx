import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { FRAGMENT_SUBSIDIARY_MASKED } from "./Subsidiary/hook";
import { Subsidiary } from "./Subsidiary/model";
import { FRAGMENT_OWNER_LOGO } from "../OwnerLogo/hook";

export const FRAGMENT_SUBSIDIARIES = new FragmentDefiner(
	"Property",
	`
    owner {
        id
        ...on RealEstateAgent {
            subsidiaries {
                ${FRAGMENT_SUBSIDIARY_MASKED.query()}
            }
		}
		...on Developer {
            subsidiaries {
                ${FRAGMENT_SUBSIDIARY_MASKED.query()}
            }
        }
    }
`
).uses(FRAGMENT_OWNER_LOGO);

interface usePropertyInformationSubsidiariesResponse {
	data: [Subsidiary];
	loading: boolean;
}

export const useSubsidiaries = ({ id }): usePropertyInformationSubsidiariesResponse => {
	const { data, loading } = useReadFragment(FRAGMENT_SUBSIDIARIES, id);

	return {
		data: data?.owner.subsidiaries,
		loading,
	};
};

import { useState, useEffect } from "react";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { PropComponentMode } from "../PropertyInterfaces";
import { cleanHtmlFromString } from "../../../Utils/Functions";

export const FRAGMENT_DESCRIPTION = new FragmentDefiner(
	"Property",
	`
    description
    project {
        description
    }
`
);

export interface DescriptionProps {
	id: string;
	limitLength?: number;
	hideInformation?: boolean;
	expandable?: boolean;
	mode?: PropComponentMode;
	keepLineBreaks?: boolean;
	showTitle?: boolean
}

export const useDescription = (id: string, mode: PropComponentMode, keepLineBreaks:boolean) => {
	const { loading, data, error } = useReadFragment(FRAGMENT_DESCRIPTION, id);
	const [description, setDescription] = useState<string>("");
	const descriptionProject = data?.project[0]?.description;
	const descriptionProperty = data?.description;

	useEffect(() => {
		if ((descriptionProject && mode == "auto") || mode == "project")
			setDescription(descriptionProject);
		else setDescription(descriptionProperty);
	}, [data]);

	return {
		loading,
		description: cleanHtmlFromString(description, keepLineBreaks),
		error,
	};
};

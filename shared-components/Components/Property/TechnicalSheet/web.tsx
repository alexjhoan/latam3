import "./styles.less";

import { Button, Col, Row, Skeleton, Space, Typography } from "antd";
import React, { FC, useEffect, useRef, useState } from "react";

import { Breakpoint } from "antd/lib/_util/responsiveObserve";
import { CheckCircleTwoTone } from "@ant-design/icons";
import { ModalSimilars } from "../ModalSimilars/web";
import { PropComponentMode } from "../PropertyInterfaces";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useTechnicalSheet } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

interface TechnicalSheetProps {
	id: string;
	rows?: number;
	columns?: Partial<Record<Breakpoint, number>>;
	mode?: PropComponentMode;
}

export const TechnicalSheet: FC<TechnicalSheetProps> = ({
	id,
	rows = 4,
	columns = { xxl: 3, xl: 3, lg: 3, md: 2, sm: 2, xs: 1 },
	mode,
}) => {
	const { technicalSheet, sendLead, loading } = useTechnicalSheet({ id });
	const { t } = useTranslation();
	const { theme } = useTheme();

	const [activeRows, setActiveRows] = useState(rows);
	const [modalVisible, setModalVisible] = useState(false);
	const [asked, setAsked] = useState([]);
	const screens = useBreakpoint();

	const tableRef = useRef(null);

	const breakpoint = b => {
		if (typeof b.xxl === "undefined") {
			return "xxl";
		}
		if (b.xxl) return "xxl";
		if (b.xl) return "xl";
		if (b.lg) return "lg";
		if (b.md) return "md";
		if (b.sm) return "sm";
		return "xs";
	};

	const askFastAction = field => {
		sendLead(field)
			.then(res => {
				if (res) {
					setAsked([...asked, field]);
					setModalVisible(true);
				}
			})
			.catch(error => {
				console.log(error);
			});
	};

	const SpanAskFast = ({ field }) => {
		if (asked.some(o => o == field)) {
			return <Typography.Text disabled>{t("¡Preguntado!")}</Typography.Text>;
		} else {
			return (
				<Typography.Link strong onClick={() => askFastAction(field)}>
					{t("¡Preguntale!")}
				</Typography.Link>
			);
		}
	};

	if (loading) return <Skeleton active />;

	const itemsToRender = technicalSheet.filter((o, i) => {
		if (["xs", "sm"].includes(breakpoint(screens))) {
			return i < activeRows * columns[breakpoint(screens)];
		}
		return activeRows;
	});

	const resetRows = () => {
		if (["xs", "sm", "md"].includes(breakpoint(screens))) {
			const offsetHeader = 120;
			const bodyRect = document.body.getBoundingClientRect().top;
			const tableRect = tableRef.current.getBoundingClientRect().top;
			const tablePosition = tableRect - bodyRect;
			const offsetPosition = tablePosition - offsetHeader;

			window.scrollTo({
				top: offsetPosition,
			});
		}

		setActiveRows(rows);
	};

	return (
		<>
			<ModalSimilars
				mode={mode}
				visible={modalVisible}
				onCancel={() => setModalVisible(false)}
				property_id={id}>
				<Row justify={"center"} style={{ textAlign: "center" }}>
					<Col span={24}>
						<CheckCircleTwoTone twoToneColor="#52c41a" style={{ fontSize: "25px" }} />
					</Col>
					<Col span={24}>
						<Typography.Text strong>
							{t("¡Consulta express enviada con éxito!")}
						</Typography.Text>
					</Col>
				</Row>
			</ModalSimilars>

			<div ref={tableRef} className="technical-sheet">
				{itemsToRender.map(o => {
					return (
						<Row key={o.field} gutter={theme.spacing.mdSpacing} justify="space-between">
							<Col>
								<Space size={theme.spacing.xsSpacing}>
									<Typography.Text type={"secondary"} className="small">
										᛫
									</Typography.Text>
									<Typography.Text>{t(o.text)}</Typography.Text>
								</Space>
							</Col>
							<Col flex={1} className="dots" />
							<Col>
								{o.value != null ? (
									<Typography.Text strong>{t(o.value)}</Typography.Text>
								) : (
									<SpanAskFast field={o.field} />
								)}
							</Col>
						</Row>
					);
				})}
			</div>
			{itemsToRender.length < technicalSheet.length && (
				<Button onClick={() => setActiveRows(activeRows * 3)} className="secondary">
					{t("Ver más")}
				</Button>
			)}
			{itemsToRender.length === technicalSheet.length && rows < activeRows && (
				<Button onClick={resetRows} className="secondary">
					{t("Ver menos")}
				</Button>
			)}

			<style jsx global>{`
				.technical-sheet {
					display: grid;
					grid-column-gap: ${theme.spacing.xlSpacing}px;
					grid-template-columns: ${[...Array(columns[breakpoint(screens)])]
						.map(v => "auto")
						.join(" ")};
				}
				.technical-sheet .ant-row {
					margin-bottom: ${theme.spacing.lgSpacing}px;
				}
				.technical-sheet .ant-row .dots {
					border-color: ${theme.colors.borderColor};
				}
			`}</style>
		</>
	);
};

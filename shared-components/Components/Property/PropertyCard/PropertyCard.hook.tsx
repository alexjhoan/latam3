import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { FRAGMENT_PRICETAG } from "../PriceTag/PriceTag.hook";
import { FRAGMENT_LOCATIONTAG } from "../LocationTag/LocationTag.hook";
import { FRAGMENT_TYPOLOGYTAG } from "../TypologyTag/TypologyTag.hook";
import { FRAGMENT_LISTINGTYPETAG } from "../ListingTypeTag/ListingType.hook";
import { FRAGMENT_IMAGE } from "../LazyImageGallery/LazyImageGallery.hook";
import { FRAGMENT_OWNER_LOGO } from "../OwnerLogo/hook";
import { FRAGMENT_PUBLICATIONTIME } from "../PublicationTime/PublicationTime.hook";
import { FRAGMENT_TITLE } from "../Title/Title.hook";
import { FRAGMENT_DESCRIPTION } from "../Description/Description.hook";
import { FRAGMENT_SOCIAL_SHARE } from "../SocialShare/hook";
import { FRAGMENT_HAS_WHATSAPP } from "../InformationRequest/LeadButtons/WhatsappButton/hook";
import { FRAGMENT_MASKED_PHONE } from "../InformationRequest/LeadButtons/PhoneButton/hook";
import { useState, useEffect } from "react";

export const FRAGMENT_PROPERTY_CARD = new FragmentDefiner(
	"Property",
	`
    id
    title
	link
	isExternal
    property_type {
        id
        name
	}
	project {
		id
		link
		isEspecial
	}
`
).uses(
	FRAGMENT_PRICETAG,
	FRAGMENT_LOCATIONTAG,
	FRAGMENT_TYPOLOGYTAG,
	FRAGMENT_LISTINGTYPETAG,
	FRAGMENT_IMAGE,
	FRAGMENT_OWNER_LOGO,
	FRAGMENT_PUBLICATIONTIME,
	FRAGMENT_TITLE,
	FRAGMENT_DESCRIPTION,
	FRAGMENT_SOCIAL_SHARE,
	FRAGMENT_HAS_WHATSAPP,
	FRAGMENT_MASKED_PHONE
);

export function usePropertyCard({ id }) {
	const { loading, data, error } = useReadFragment(FRAGMENT_PROPERTY_CARD, id);
	const [cardData, setCardData] = useState(data);

	useEffect(() => {
		const dataCopy = { ...data, pathname: "/propSingle" };
		if (data?.project.length) {
			dataCopy.id = dataCopy.project[0].id
			dataCopy.link = dataCopy.project[0].link;
			dataCopy.pathname = "/projectSingle";
			dataCopy.avoidRouter = dataCopy.project[0].isEspecial ?? false;
		}
		setCardData(dataCopy)
	}, [data]);

	return { loading, data: cardData, error };
}

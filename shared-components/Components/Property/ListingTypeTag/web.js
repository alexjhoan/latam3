import "./styles.less";

import { Skeleton, Space, Tag } from "antd";

import { UseListingTypeTag } from "./ListingType.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export function ListingTypeTag({ id }) {
	const { tags, loading, error } = UseListingTypeTag({ id });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (loading) {
		return (
			<Space>
				<Skeleton.Button size={"small"} style={{ width: "70px" }} />
				<Skeleton.Button size={"small"} style={{ width: "70px" }} />
			</Space>
		);
	}

	if (!tags.length || error) return null;

	return (
		<>
			{tags.map((tag, i) => {
				let tagColor = "default";
				// if (tag.type == "error") tagColor = "magenta";
				return (
					<Tag
						color={tagColor}
						size={"middle"}
						key={"property_" + id + "_tag_" + i}
						className="property-tag">
						{t(tag.text)}
					</Tag>
				);
			})}
			<style jsx global>{`
				.property-tag {
					border-color: ${theme.colors.backgroundColor};
					font-size: ${theme.fontSizes.xsFontSize};
					margin-bottom: ${theme.spacing.xsSpacing}px;
					margin-right: ${theme.spacing.smSpacing}px;
					height: ${theme.spacing.lgSpacing + 4}px;
					line-height: ${theme.spacing.lgSpacing + 4}px;
					padding: 0 ${theme.spacing.xsSpacing}px;
				}
			`}</style>
		</>
	);
}

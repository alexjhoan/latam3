import {
	FragmentDefiner,
	useReadFragment,
} from "../../../GlobalHooks/useReadFragment";
import dayjs from "dayjs";
import { isSuperHighlighted, isHighlighted } from "../../../Utils/Functions";
import { useTranslation } from "react-i18next";

export const FRAGMENT_LISTINGTYPETAG = new FragmentDefiner(
	"Property",
	`
    project {
        id
	}
	isExternal
    highlight
    created_at
    updated_at
`
);

export function UseListingTypeTag({ id }) {
	const { t } = useTranslation()
	const { loading, data, error } = useReadFragment(FRAGMENT_LISTINGTYPETAG, id);

	let tags = [];

	if (loading || error) return { loading, tags, error };

	if (data.project.length) {
		tags.push({ text: "Proyecto", type: "primary" });
	}

	if (data.highlight) {
		if (isSuperHighlighted(data.highlight)) {
			tags.push({ text: "Super Destacado", type: "error" });
		} else if (isHighlighted(data.highlight)) {
			tags.push({ text: "Destacado", type: "primary" });
		}
	}

	if (dayjs(data.created_at).isAfter(dayjs().subtract(5, "day"))) {
		//todo definir si estos tags se van a mostrar o no.
		tags.push({ text: "Nueva", type: "primary" });
	} else if (false && dayjs(data.updated_at).isAfter(dayjs().subtract(5, "day"))) {
		// por ahora no queremos mostrar este tag
		tags.push({ text: "Actualizada", type: "primary" });
	}

	if(data.isExternal) {
		tags.push({ text: t("Ficha externa"), type: "primary" })
	}

	return { loading, tags, error };
}

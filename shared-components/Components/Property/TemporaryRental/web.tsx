import "./styles.less";

import { Button, Col, Row, Skeleton, Space, Typography } from "antd";
import { useRef, useState } from "react";

import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useTemporaryRental } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const TemporaryRental = ({
	id,
	columns = { xxl: 3, xl: 3, lg: 3, md: 2, sm: 2, xs: 1 },
	rows = 4,
}) => {
	const { t } = useTranslation();
	const { data, sendLead, loading } = useTemporaryRental({ id });
	const screens = useBreakpoint();
	const { theme } = useTheme();
	const [activeRows, setActiveRows] = useState(rows);
	const [asked, setAsked] = useState([]);
	const tableRef = useRef(null);

	const breakpoint = b => {
		if (typeof b.xxl === "undefined") {
			return "xxl";
		}
		if (b.xxl) return "xxl";
		if (b.xl) return "xl";
		if (b.lg) return "lg";
		if (b.md) return "md";
		if (b.sm) return "sm";
		return "xs";
	};

	const itemsToRender = data.filter((o, i) => i < activeRows * columns[breakpoint(screens)]);

	const askFastAction = field => {
		sendLead(field)
			.then(res => {
				if (res) {
					setAsked([...asked, field]);
				}
			})
			.catch(error => {});
	};

	const SpanAskFast = ({ field }) => {
		if (asked.some(o => o == field)) {
			return <Typography.Text disabled>{t("¡Preguntado!")}</Typography.Text>;
		} else {
			return (
				<Typography.Link strong onClick={() => askFastAction(field)}>
					{t("¡Preguntale!")}
				</Typography.Link>
			);
		}
	};

	const resetRows = () => {
		if (["xs", "sm", "md"].includes(breakpoint(screens))) {
			const offsetHeader = 120;
			const bodyRect = document.body.getBoundingClientRect().top;
			const tableRect = tableRef.current.getBoundingClientRect().top;
			const tablePosition = tableRect - bodyRect;
			const offsetPosition = tablePosition - offsetHeader;

			window.scrollTo({
				top: offsetPosition,
			});
		}

		setActiveRows(rows);
	};

	if (loading) return <Skeleton active />;

	return (
		<>
			<div ref={tableRef} className="temporary-rental">
				{itemsToRender.map((season, i) => {
					return (
						<Row
							key={"temporary_rental_" + i}
							gutter={theme.spacing.mdSpacing}
							justify="space-between">
							<Col>
								<Typography.Text ellipsis className="title" title={t(season.title)}>
									<Typography.Text type={"secondary"} className="small">
										᛫
									</Typography.Text>
									{t(season.title)}
								</Typography.Text>
							</Col>
							<Col flex={1} className="dots" />
							<Col>
								{season.value !== 0 ? (
									<Typography.Text strong ellipsis>
										{season.value}
									</Typography.Text>
								) : (
									<SpanAskFast field={season.title} />
								)}
							</Col>
						</Row>
					);
				})}
			</div>
			{itemsToRender.length < data.length && (
				<Button onClick={() => setActiveRows(activeRows * 3)} className="secondary">
					{t("Ver más")}
				</Button>
			)}
			{itemsToRender.length === data.length && rows < activeRows && (
				<Button onClick={resetRows} className="secondary">
					{t("Ver menos")}
				</Button>
			)}
			<style jsx global>{`
				.temporary-rental {
					display: grid;
					grid-column-gap: ${theme.spacing.xlSpacing}px;
					grid-template-columns: ${[...Array(columns[breakpoint(screens)])]
						.map(v => "auto")
						.join(" ")};
				}
				.temporary-rental .ant-row {
					margin-bottom: ${theme.spacing.lgSpacing}px;
				}
				.temporary-rental .ant-row .ant-typography.small {
					margin-right: ${theme.spacing.xsSpacing}px;
				}
				.temporary-rental .ant-row .dots {
					border-color: ${theme.colors.borderColor};
				}
			`}</style>
		</>
	);
};

import "./styles.less";

import { Col, Row, Skeleton, Space, Typography } from "antd";
import React, { FC } from "react";

import { useFacilities } from "./Facilities.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { PropComponentMode } from "../PropertyInterfaces";

interface FacilitiesProps {
	id: string;
	mode?: PropComponentMode;
}

export const Facilities: FC<FacilitiesProps> = ({ id, mode = 'auto' }) => {
	const { facilities, loading } = useFacilities({ id, mode });
	const { theme } = useTheme();
	const { t } = useTranslation();

	if (!facilities || facilities.length == 0) {
		return null;
	}

	return (
		<Row gutter={[0, theme.spacing.lgSpacing]} className="property-facilities">
			<Col span={24} key="property_facilities">
				<Typography.Title level={4}>{t(mode === 'project' ? "Amenities" : "Comodidades de la propiedad")}</Typography.Title>
				{loading ? (
					<Skeleton active />
				) : (
					<Row gutter={[theme.spacing.lgSpacing, theme.spacing.xsSpacing]}>
						{facilities.map((o, i) => (
							<Col key={"property_facility_" + i}>
								<Space size={5}>
									<Typography.Text type={"secondary"} className="small">
										᛫
									</Typography.Text>
									<Typography.Text>{o.name}</Typography.Text>
								</Space>
							</Col>
						))}
					</Row>
				)}
			</Col>
		</Row>
	);
};

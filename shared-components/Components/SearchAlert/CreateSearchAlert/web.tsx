import "./styles.less";

import { Button, notification } from "antd";

import { BellOutlined } from "@ant-design/icons";
import React from "react";
import { useCreateSearchAlert } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const CreateSearchAlert = () => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { searchAlert, alertIsSaved, loading } = useCreateSearchAlert();

	const changeAlert = isSaveAlert => {
		searchAlert.send().then(data => {
			if (!data) {
				return;
			}

			let message = "Busqueda eliminada";
			let description =
				"Ya no recibira alertas cuando se actualicen los resultados de la busqueda";
			if (!isSaveAlert) {
				message = "Busqueda guardada";
				description =
					"Su busqueda ha sido guardada exitosamente. Le enviaremos alertas cuando se actualicen en los resultados de su busqueda";
			}
			notification.open({
				message,
				description,
			});
		});
	};

	let text = "Guardar Busqueda";
	if (searchAlert.response.loading) text = "Guardando";
	else if (alertIsSaved) text = "Busqueda Guardada";

	return (
		<>
			<Button
				disabled={loading}
				className="btn-save-search"
				onClick={() => changeAlert(alertIsSaved)}
				loading={searchAlert.response.loading}
				icon={<BellOutlined />}>
				{t(text)}
			</Button>
			<style jsx global>{`
				.btn-save-search {
					border-color: ${theme.colors.linkColor};
					color: ${theme.colors.linkHoverColor};
				}

				.btn-save-search:hover,
				.btn-save-search:active,
				.btn-save-search:focus {
					border-color: ${theme.colors.linkColor};
					color: ${theme.colors.linkHoverColor};
					background: ${theme.colors.secondaryOpacityColor};
				}
			`}</style>
		</>
	);
};

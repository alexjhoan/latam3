import gql from "graphql-tag";
import {useMutation} from "@apollo/client";

const MUTATION_UPDATE_SEARCH_ALERT = gql`
    mutation updateSearchAlert($data: SearchAlertInput!) {
        updateSearchAlert(data: $data) {
            id
            ...on Individual {
                searchAlerts {
                    id
                    search
                    frequency
                    status
                    title
                }
            }
        }
    }
`;

export const useUpdateSearchAlert = () => {
   const [mutation] = useMutation(MUTATION_UPDATE_SEARCH_ALERT, {
       onError: (error) => {
           console.error(error);
       }
   });

   const updateSearchAlert = (data) => mutation({
       variables: {
        data
       }
   });

    return {updateSearchAlert}
}
import gql from "graphql-tag";
import {useQuery} from "@apollo/client";

const QUERY_SAVED_SEARCH_ALERTS_COMPLETE = gql`
    query savedAlerts {
        me {
            id
            ...on Individual {
                searchAlerts {
                    id
                    search
                    frequency
                    status
                    title
                }
            }
        }
    }
`

export const useListSearchAlerts = () => {
    const {data, loading, error} = useQuery(QUERY_SAVED_SEARCH_ALERTS_COMPLETE)
    return {data: data?.me.searchAlerts, loading, error}
}
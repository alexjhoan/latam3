import React, { useEffect, useState } from "react";

export const OnScreen = ({ onEnterView, onLeaveView, children }) => {
	const [ref, setRef] = useState(null);
	const [observer, setObserver] = useState(null);

	useEffect(() => {
		const obs = new IntersectionObserver(
			entries => {
				entries.forEach(entry => {
					if (entry.isIntersecting) onEnterView();
					else onLeaveView();
				});
			},
			{ threshold: 0.1 }
		);
		setObserver(obs);
	}, []);

	useEffect(() => {
		if (ref && observer) observer.observe(ref);
		return () => {
			if (ref) observer.unobserve(ref);
		};
	}, [observer, ref]);

	return <div ref={setRef}>{children}</div>;
};

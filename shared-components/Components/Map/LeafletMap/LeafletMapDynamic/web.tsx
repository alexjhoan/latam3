import dynamic from 'next/dynamic'


export const LeafletMapDynamic = dynamic({
    loader: () => import('../web').then((mod) => mod.LeafletMap),
    loading: () => null,
    ssr: false
});

import "./styles.less";

import { Button, Col, Grid, Row, Skeleton, Space, Typography } from "antd";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

import { useSEO } from "../hook";
import { useState } from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Title } = Typography;
const { useBreakpoint } = Grid;

export const LinkBox = () => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const screen = useBreakpoint();
	const { seoMetaTags, error, loading } = useSEO();

	if (loading) {
		return (
			<Row
				gutter={[
					theme.spacing.lgSpacing,
					{
						lg: theme.spacing.smSpacing,
						xs: theme.spacing.xsSpacing,
						sm: theme.spacing.lgSpacing,
					},
				]}>
				{[...Array(4)].map((o, i) => {
					return (
						<Col span={24} lg={6} key={"link_box_item_" + i}>
							<Skeleton active paragraph={{ rows: screen.xl ? 4 : 2 }} />
						</Col>
					);
				})}
			</Row>
		);
	}
	if (error || !seoMetaTags?.links) return null;

	const cantSeoMetaTags = Object.entries(seoMetaTags.links).length;

	return (
		<Row
			justify={"space-between"}
			gutter={[theme.spacing.lgSpacing, theme.spacing.smSpacing]}
			className="link-boxes container-items">
			{Object.entries(seoMetaTags.links).map(([categoryName, links], i) => {
				return (
					<Col
						span={24}
						lg={cantSeoMetaTags === 4 ? 6 : 8}
						key={"link_box_item_" + i}
						className={"link_box_item"}>
						<Title level={4}>{t(categoryName)}</Title>
						<CollapsableLinkBox links={links} />
					</Col>
				);
			})}
		</Row>
	);
};

const CollapsableLinkBox = ({ links }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const screen = useBreakpoint();
	const [more, setMore] = useState(false);

	return (
		<Row
			gutter={[
				theme.spacing.xsSpacing,
				{ xs: theme.spacing.smSpacing, lg: theme.spacing.xsSpacing },
			]}
			className="link-box">
			{links.slice(0, more ? links.length : 4).map((link, j) => {
				return (
					<Col lg={24} key={"link_box_item_link_" + j} className="link-box-item">
						<Space size={4}>
							{!screen.lg && j > 0 && <Typography.Text strong>᛫</Typography.Text>}
							<Typography.Link href={link["url"]}>{t(link["text"])}</Typography.Link>
						</Space>
					</Col>
				);
			})}
			<Col lg={24}>
				{links.length > 4 && (
					<Button
						type="text"
						style={{ padding: "0px", height: "auto" }}
						onClick={() => setMore(!more)}>
						<Space size={theme.spacing.xsSpacing}>
							<span>{t(more ? "Menos" : "Más")}</span>
							{more ? <UpOutlined /> : <DownOutlined />}
						</Space>
					</Button>
				)}
			</Col>
		</Row>
	);
};

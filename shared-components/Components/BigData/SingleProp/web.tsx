import { ArrowDownOutlined, ArrowUpOutlined } from "@ant-design/icons";
import { ChartsIcon, FlagsIcon, TagsIcon } from "../../CustomIcons/web";
import { Col, Row, Skeleton, Space, Typography } from "antd";

import React from "react";
import { useBigDataSingleProp } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text, Title, Paragraph } = Typography;

export const BigDataSingleProp = ({ id }) => {
	const {
		loading,
		error,
		bigData,
		propData,
		offsetData,
		currencySymbol,
		operation,
		show,
	} = useBigDataSingleProp({ property_id: id });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (error || !show) return null;

	return (
		<Row gutter={[theme.spacing.lgSpacing, theme.spacing.lgSpacing]}>
			<Col xs={24}>
				<Title level={4}>{t("Esta propiedad en números")}</Title>
			</Col>
			{loading ? (
				[1, 2, 3].map((e, i) => (
					<Col xs={24} md={8} key={i}>
						<Skeleton active avatar />
					</Col>
				))
			) : (
				<>
					<Col xs={24} md={8}>
						<Space align={"start"} size="middle">
							<Paragraph>
								<Title level={4}>
									<ChartsIcon />
								</Title>
							</Paragraph>
							<Paragraph>
								<Title level={4}>{propData?.price_m2_text} /m²</Title>
								<Text>
									{t(`Precio promedio por m² de propiedades similares`)}:{" "}
									{bigData?.avg_price_m2_text} /m²
								</Text>
								<Text
									strong
									style={{
										display: "block",
										marginTop: theme.spacing.smSpacing,
									}}>
									{offsetData.price_m2 < -4 && (
										<>
											{offsetData.price_m2}% {t(`por debajo`)}
											<ArrowDownOutlined
												style={{
													color: theme.colors.successColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.price_m2 > 4 && (
										<>
											{offsetData.price_m2}% {t(`por arriba`)}
											<ArrowUpOutlined
												style={{
													color: theme.colors.errorColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.price_m2 >= -4 && offsetData.price_m2 <= 4 && (
										<>{t(`Dentro del promedio`)}.</>
									)}
								</Text>
							</Paragraph>
						</Space>
					</Col>

					<Col xs={24} md={8}>
						<Space align={"start"} size="middle">
							<Paragraph>
								<Title level={4}>
									<TagsIcon />
								</Title>
							</Paragraph>
							<Paragraph>
								<Title level={4}>{propData.price_text}</Title>
								<Text>
									{t(
										`Precio de ${String(
											operation
										).toLowerCase()} para propiedades similares`
									)}
									: {bigData?.avg_price_text}
									<br />
								</Text>
								<Text
									strong
									style={{
										display: "block",
										marginTop: theme.spacing.smSpacing,
									}}>
									{offsetData.price < -4 && (
										<>
											{offsetData.price}% {t(`por debajo`)}
											<ArrowDownOutlined
												style={{
													color: theme.colors.successColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.price > 4 && (
										<>
											{offsetData.price}% {t(`por arriba`)}
											<ArrowUpOutlined
												style={{
													color: theme.colors.errorColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.price >= -4 && offsetData.price <= 4 && (
										<>{t(`Dentro del promedio`)}.</>
									)}
								</Text>
							</Paragraph>
						</Space>
					</Col>

					<Col xs={24} md={8}>
						<Space align={"start"} size="middle">
							<Paragraph>
								<Title level={4}>
									<FlagsIcon />
								</Title>
							</Paragraph>
							<Paragraph>
								<Title level={4}>
									{Math.round(propData.time_in_market / 30)} {t(`meses`)}
								</Title>
								<Text>
									{t(`Tiempo promedio de publicación de propiedades similares`)}:{" "}
									{Math.round(bigData?.avg_time_in_market / 30)} {t(`meses`)}
									<br />
								</Text>
								<Text
									strong
									style={{
										display: "block",
										marginTop: theme.spacing.smSpacing,
									}}>
									{offsetData.time_in_market < -4 && (
										<>
											{offsetData.time_in_market}% {t(`por debajo`)}
											<ArrowDownOutlined
												style={{
													color: theme.colors.successColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.time_in_market > 4 && (
										<>
											{offsetData.time_in_market}% {t(`por arriba`)}
											<ArrowUpOutlined
												style={{
													color: theme.colors.errorColor,
													margin: `0 ${theme.spacing.smSpacing}px`,
												}}
											/>
										</>
									)}
									{offsetData.time_in_market >= -4 &&
										offsetData.time_in_market <= 4 && (
											<>{t(`Dentro del promedio`)}.</>
										)}
								</Text>
							</Paragraph>
						</Space>
					</Col>
				</>
			)}
		</Row>
	);
};

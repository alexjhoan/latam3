import "./styles.less";

import { Button, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../Components/Image/web";
import { PlusOutlined } from "@ant-design/icons";
import { useBannerHome } from "./BannerHome.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Link, Paragraph } = Typography;

export function BannerHome() {
	const { loading, data, error } = useBannerHome({});
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (loading) return null;
	const d =
		error || !data
			? {
					id: "00",
					name: "default",
					image:
						"https://cdn2.infocasas.com.uy/web/5786707324f50_infocdn__home30-opti.jpg",
			  }
			: data;

	return (
		<div className={"destacadoHome"} key={"destacado_home_" + d.id}>
			<div className="overLay" />
			<Img src={d.image} className="background" alt={d.name} />
			{typeof d.url != "undefined" && (
				<div className={"background-container " + (d.html_logo == "" ? "big" : "small")}>
					<Link href={d.url} target="_blank">
						<picture>
							<source
								media={`(max-width: ${theme.breakPoints.lg})`}
								srcSet={d.logo_mobile ? d.logo_mobile : d.logo}
							/>
							<Img src={d.logo} alt={d.name} className="logo" />
						</picture>
						<Paragraph className="text" ellipsis={{ rows: 3 }}>
							<span dangerouslySetInnerHTML={{ __html: d.html_logo }} />
						</Paragraph>
					</Link>
					<Button
						className={"more"}
						href={d.url}
						icon={<PlusOutlined />}
						children={t("Ver Más")}
					/>
				</div>
			)}
			<style jsx>{`
				.destacadoHome :global(.background-container) {
					background-color: ${d.logo_bg_color};
					bottom: ${theme.spacing.mdSpacing}px;
					right: ${theme.spacing.mdSpacing}px;
					padding: ${theme.spacing.lgSpacing}px;
					border-radius: ${theme.spacing.smSpacing}px;
				}
				.destacadoHome :global(picture) {
					width: ${typeof data?.logo_width != "undefined"
						? data?.logo_width
						: "183"}px !important;
				}
				.destacadoHome :global(.logo) {
					width: ${typeof data?.logo_width != "undefined"
						? data?.logo_width
						: "183"}px !important;
				}
				.destacadoHome :global(.text) {
					color: ${d.logo_text_color};
				}
				.destacadoHome :global(.ant-btn) {
					color: ${d.logo_text_color};
					border: ${"solid 1px " + d.logo_text_color};
					border-radius: ${theme.spacing.xsSpacing}px;
				}
				.destacadoHome :global(.ant-btn:hover) {
					color: ${d.logo_bg_color};
					background: ${d.logo_text_color};
				}

				@media screen and (max-width: 992px) {
					.destacadoHome :global(.background-container) {
						width: calc(100% - ${theme.spacing.smSpacing * 2}px);
						background-color: ${d.logo_bg_color};
						bottom: ${theme.spacing.smSpacing}px;
						right: ${theme.spacing.smSpacing}px;
					}
					.destacadoHome :global(picture) {
						min-width: ${d.logo_mobile_width
							? d.logo_mobile_width
							: "112"}px !important;
					}
					.destacadoHome :global(.logo) {
						min-width: ${d.logo_mobile_width
							? d.logo_mobile_width
							: "112"}px !important;
					}
				}
			`}</style>
		</div>
	);
}

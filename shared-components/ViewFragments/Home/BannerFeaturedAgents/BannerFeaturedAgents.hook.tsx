import React, { useState } from "react";

import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";

const INMO_QUERY = gql`
	query PremiumRealAgents {
		realEstateAgents(premium_agents: true) {
			id
			logo
			name
			profile_url
			inmoLink
			inmoPropsLink
		}
	}
`;

const useBannerFeaturedAgents = () => {
	const { data, loading, error } = useQuery(INMO_QUERY);

	return {
		loading: loading,
		data: data?.realEstateAgents,
		error: error,
	};
};

export { useBannerFeaturedAgents };

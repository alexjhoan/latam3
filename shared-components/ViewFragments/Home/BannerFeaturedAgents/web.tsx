import "./style.less";

import { Carousel, Skeleton, Typography } from "antd";
import React, { useEffect, useState } from "react";

import { CarouselArrow } from "../../../Components/Property/LazyImageGallery/CarouselArrow/web";
import { ImagenOptimizada } from "../../../Components/Image/web";
import { removeUrlProtocol } from "../../../Utils/Functions";
import { useBannerFeaturedAgents } from "./BannerFeaturedAgents.hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Link } = Typography;

const BannerFeaturedAgents = () => {
	const { theme } = useTheme();
	const { data, loading, error } = useBannerFeaturedAgents();

	const [settings, setSettings] = useState({
		dots: false,
		arrows: true,
		nextArrow: <CarouselArrow type={"ghost"} position="inside" side={"right"} />,
		prevArrow: <CarouselArrow type={"ghost"} position="inside" side={"left"} />,
		infinite: data?.length > 6,
		speed: 400,
		autoplaySpeed: 5000,
		autoplay: false,
		slidesToScroll: 2,
		slidesToShow: 6,
		render: true,
		responsive: [],
	});

	useEffect(() => {
		setSettings({
			...settings,
			render: false,
			responsive: [
				{
					breakpoint: Number(
						theme.breakPoints.lg.substring(0, theme.breakPoints.lg.length - 2)
					),
					settings: {
						slidesToShow: 5,
						slidesToScroll: 2,
						infinite: data?.length > 5,
						autoplaySpeed: 5000,
						autoplay: true,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.xs.substring(0, theme.breakPoints.xs.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						infinite: data?.length > 2,
						autoplaySpeed: 5000,
						autoplay: true,
					},
				},
			],
		});
	}, []);

	useEffect(() => {
		if (!settings.render) setSettings({ ...settings, render: true });
	}, [settings.render]);

	if (!settings.render) return null;
	if (error) return null;
	if (data && data.length < 4) return null;

	const banners = loading ? [...Array(8)] : data;

	return (
		<>
			<Carousel
				{...settings}
				className={
					loading ? "featured-agents featured-agents-skeleton" : "featured-agents "
				}>
				{banners.map((agent, i) => (
					<div
						key={"featured_agent_" + i}
						className={
							loading ? "featured-agent featured-agent-skeleton" : "featured-agent "
						}>
						{loading ? (
							<Skeleton.Image />
						) : (
							<Link
								target="_blank"
								href={removeUrlProtocol(agent.inmoPropsLink)}
								title={agent.name}>
								<ImagenOptimizada src={agent.logo} alt={agent.name} />
							</Link>
						)}
					</div>
				))}
			</Carousel>
			<style jsx global>{`
				.featured-agents {
					padding: ${theme.spacing.lgSpacing}px ${theme.spacing.xlSpacing * 2}px;
					border: 1px solid ${theme.colors.borderColor};
					border-radius: ${theme.spacing.smSpacing}px;
					box-shadow: ${theme.colors.borderColor} 0px 2px 2px 0px;
					background: ${theme.colors.backgroundColor};
				}

				.featured-agents .featured-agent {
					width: calc(100% - ${theme.spacing.xlSpacing}px) !important;
					padding: ${theme.spacing.smSpacing}px 0px;
				}

				.featured-agents
					.featured-agent.featured-agent-skeleton
					.ant-skeleton-element
					.ant-skeleton-image {
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.featured-agents .featured-agent img {
					border-radius: ${theme.spacing.smSpacing}px;
					background: ${theme.colors.backgroundColor};
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.featured-agents {
						padding: ${theme.spacing.mdSpacing}px ${theme.spacing.xxlSpacing}px;
					}
				}
			`}</style>
		</>
	);
};

export { BannerFeaturedAgents };

import "./styles.less";

import { Button, Col, Form, Row } from "antd";
import {
	KeywordLocation,
	setRecientLocations,
} from "../../../Components/Filters/KeywordLocation/web";
import React, { useEffect, useState } from "react";
import { isTemporal, showPropertyType } from "../../../Utils/Functions";

import { OperationType } from "../../../Components/Filters/OperationType/web";
import { PropertyType } from "../../../Components/Filters/PropertyType/web";
import { SearchOutlined } from "@ant-design/icons";
import { TemporalFilter } from "../../../Components/Filters/TemporalFilter/web";
import { encodeHashUrl } from "../../../Utils/Functions";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useHomeFilters } from "./HomeFilters.hook";
import { useRouter } from "next/router";
import { useTheme } from "../../../Styles/ThemeHook";

export const HomeFilters = () => {
	const { filters, changeFilters, filtersTags, search } = useHomeFilters();

	const doSearch = () => search.send({ variables: { params: filters } });

	const router = useRouter();
	const screens = useBreakpoint();
	const { theme } = useTheme();
	const showTemporalFilter = isTemporal(filters.operation_type_id);
	const showPropertyFilter = showPropertyType(filters.operation_type_id);

	useEffect(() => {
		if (search.response.data && !search.response.loading) {
			setRecientLocations(filtersTags.neighborhood_id, filtersTags.estate_id);
			router.push(
				{
					pathname: "/searchPage",
					query: { hashed: encodeHashUrl({ filters: filtersTags }) },
				},
				search.response.data.searchUrl.url
			);
		}
	}, [search.response]);

	useEffect(() => {
		if (filters.estate_id != null || filters.searchstring != null) {
			search.send({ variables: { params: filters } });
		}
	}, [filters.estate_id, filters.searchstring]);

	return (
		<>
			<Form onFinish={doSearch} className="home-filters">
				<Row
					justify={"center"}
					gutter={[
						{
							xs: theme.spacing.mdSpacing,
							sm: theme.spacing.mdSpacing,
							lg: 0,
						}, // horizontal
						{
							xs: theme.spacing.mdSpacing,
							sm: theme.spacing.mdSpacing,
							lg: theme.spacing.smSpacing,
						}, // vertical
					]}>
					<Col
						xs={showTemporalFilter ? 24 : 12}
						sm={showTemporalFilter ? 24 : 12}
						lg={22}>
						<OperationType
							filterChanged={changeFilters}
							home
							inputType={screens.lg ? "butons" : "select"}
						/>
					</Col>
					{showPropertyFilter && (
						<Col xs={12} sm={12} lg={8} className="big-input">
							<PropertyType filterChanged={changeFilters} />
						</Col>
					)}
					{showTemporalFilter && (
						<Col xs={24} sm={24} lg={8} className="big-input temporal-filter-container">
							<TemporalFilter filterChanged={changeFilters} />
						</Col>
					)}
					<Col flex={"1"} className="big-input">
						<Row gutter={0}>
							<Col flex={"1"}>
								<KeywordLocation filterChanged={changeFilters} />
							</Col>
							<Col className={"search-button-container"}>
								<Button
									className="search-button superPrimary"
									type="primary"
									loading={search.response.loading}
									onClick={() =>
										search.send({
											variables: { params: filters },
										})
									}
									icon={<SearchOutlined />}
								/>
							</Col>
						</Row>
					</Col>
				</Row>
			</Form>
			<style jsx global>{`
				.home-filters .big-input .search-button-container .search-button {
					border-radius: 0 ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0;
				}

				.home-filters .big-input .ant-picker {
					padding: ${theme.spacing.smSpacing}px;
				}

				.home-filters
					.big-input
					.search-button-container
					.search-button-skeleton
					.ant-skeleton-button {
					border-radius: 0 ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0;
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-filters {
						padding: ${theme.spacing.mdSpacing}px ${theme.spacing.mdSpacing}px 1px;
						background-color: ${theme.colors.backgroundModalColor};
						border-radius: ${theme.spacing.smSpacing}px;
					}

					.home-filters .big-input .search-button-container .search-button {
						border-radius: 0 ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0;
					}

					.home-filters .big-input .keyword-location-skeleton {
						border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
					}
				}
			`}</style>
		</>
	);
};

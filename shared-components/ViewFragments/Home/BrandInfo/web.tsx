import "./styles.less";

import { Carousel, Col, Row, Skeleton, Typography } from "antd";
import { useEffect, useState } from "react";

import { ImagenOptimizada as Img } from "../../../Components/Image/web";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Title, Paragraph } = Typography;

export const BrandInfo = () => {
	const { theme } = useTheme();
	const { t } = useTranslation();

	const data = [
		{
			title: "Todas las propiedades",
			paragraph:
				"Con más de 200.000 propiedades publicadas, alcanzamos el 97% de la oferta inmobiliaria del país.",
			imgUrl: "https://cdn1.infocasas.com.uy/web/5f74d7f0f2408_infocdn__propiedades.png",
		},
		{
			title: "Búsqueda exacta",
			paragraph:
				"Nuestros filtros te permiten encontrar la propiedad que estás buscando en el menor tiempo posible.",
			imgUrl: "https://cdn1.infocasas.com.uy/web/5f74d700efa10_infocdn__busqueda.png",
		},
		{
			title: "Big Data",
			paragraph:
				"Diseñamos herramientas para que puedas tomar las mejores decisiones basadas en datos.",
			imgUrl: "https://cdn1.infocasas.com.uy/web/5f74d6c6b18c4_infocdn__big-data.png",
		},
	];

	const [settings, setSettings] = useState({
		render: true,
		arrows: false,
		responsive: [],
		speed: 400,
		autoplaySpeed: 6000,
		autoplay: false,
		slidesToShow: 3,
		slidesToScroll: 1,
	});

	useEffect(() => {
		setSettings({
			...settings,
			render: false,
			responsive: [
				{
					breakpoint: Number(
						theme.breakPoints.md.substring(0, theme.breakPoints.md.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
						autoplay: true,
						autoplaySpeed: 6000,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.sm.substring(0, theme.breakPoints.sm.length - 2)
					),
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: true,
						autoplay: true,
						autoplaySpeed: 6000,
					},
				},
			],
		});
	}, []);

	useEffect(() => {
		if (!settings.render) setSettings({ ...settings, render: true });
	}, [settings.render]);
	if (!settings.render) return null;

	return (
		<>
			<div className="brand-info">
				<Carousel {...settings}>
					{data.map((item, i) => (
						<div key={"brandInfo_item_" + i}>
							<Row justify={"center"}>
								<>
									<Col span={24}>
										<Img
											src={item.imgUrl}
											alt={item.title}
											className={"brand-info-image"}
										/>
									</Col>
									<Col span={24}>
										<Title className="brand-info-title" level={3}>
											{t(item.title)}
										</Title>
									</Col>
									<Col span={24}>
										<Paragraph className="brand-info-text">
											{t(item.paragraph)}
										</Paragraph>
									</Col>
								</>
							</Row>
						</div>
					))}
				</Carousel>
			</div>
			<style jsx global>{`
				.brand-info .slick-slide {
					padding: 0 ${theme.spacing.lgSpacing}px;
				}

				.brand-info .slick-slide .ant-row-center .brand-info-title {
					margin-top: ${theme.spacing.xlSpacing}px;
				}

				.brand-info .ant-row-center .brand-info-image.ant-skeleton-image {
					border-radius: ${theme.spacing.smSpacing}px;
				}
			`}</style>
		</>
	);
};

// const BrandInfoSketelon = () => {
// 	return (
// 		<>
// 			<Col span={24}>
// 				<Skeleton.Image className={"brand-info-image"} />
// 			</Col>
// 			<Col span={24}>
// 				<Skeleton active className="brand-info-title" />
// 			</Col>
// 		</>
// 	);
// };

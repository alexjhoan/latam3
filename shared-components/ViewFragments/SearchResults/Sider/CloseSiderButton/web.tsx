import { Dispatch, SetStateAction } from "react";

import { Button } from "antd";
import { CloseCircleOutlined } from "@ant-design/icons";
import { useCloseSiderButton } from "./hook";
import { useTranslation } from "react-i18next";

interface CloseSiderButtonProps {
	setSider: Dispatch<SetStateAction<boolean>>;
	showSearchResults?: boolean;
}

export const CloseSiderButton = ({
	setSider,
	showSearchResults = false,
}: CloseSiderButtonProps) => {
	const { t } = useTranslation();
	const { data, loading } = useCloseSiderButton();

	return (
		<Button
			size="large"
			disabled={loading}
			style={{ width: showSearchResults ? "100%" : "auto" }}
			loading={loading && showSearchResults}
			type={showSearchResults ? "primary" : "text"}
			onClick={() => setSider(true)}
			icon={!showSearchResults && <CloseCircleOutlined />}
			children={
				!loading &&
				showSearchResults &&
				t("Ver") + " " + (data > 400 ? "+400" : data) + " " + t("propiedades")
			}
		/>
	);
};

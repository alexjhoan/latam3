import { Button, Typography, Tag } from "antd";

import { MapIcon } from "../../../../Components/CustomIcons/web";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useFilters } from "../../../../Components/Filters/Filters.hook";

export const ButtonMapToggle = ({ map, setMap }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();

	return (
		<Button onClick={() => setMap(!map)}>
			<MapIcon
				style={{ color: theme.colors.primaryColor, fontSize: theme.fontSizes.smFontSize }}
			/>
			<Typography.Text style={{ marginLeft: `${theme.spacing.smSpacing}px` }}>
				{map ? t("Ver Listado") : t("Ver Mapa")}
			</Typography.Text>
		</Button>
	);
};

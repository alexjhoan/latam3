import "./styles.less";

import { Button, Space, Typography } from "antd";
import { CompassOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";
import {
	K_INFOWINDOW_MARGIN_SIDES,
	K_INFOWINDOW_WIDTH,
	ListingInfoWindow,
} from "../../../../Components/Map/Listing/InfoWindow/web";
import {
	K_LISTING_MARKER_SIZE,
	ListingMarker,
} from "../../../../Components/Map/Listing/Marker/web";
import React, { useEffect, useRef, useState } from "react";

import GoogleMapReact from "google-map-react";
import { SearchResultDisplay } from "../SearchResultsCards/SearchResultsPagination/ResultDisplay/web";
import { useSearchResultsMapWithBackend } from "./SearchResultsMap.hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { RTBhouse } from "../../../../Components/RTBhouse/web";
import { useGoogleTagManager } from "../../../../GlobalHooks/web/GoogleTagManager.hook";

const { Text } = Typography;

/**
 * @todo    multiple markers, same latlng
 * @todo    click on marker q es proyecto, rompe todo
 */

const DEFAULT_ZOOM_LEVEL = 14;
const MAX_POINTS_TO_SHOW = 100;
const MIN_ZOOM_TO_SHOW_PRICE_MARKER = 17;
const PIXELS_MARGIN = 25;

export function SearchResultsMap() {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const GTM = useGoogleTagManager();

	const {
		polygon,
		loading,
		error,
		setPolygon,
		selectMarker,
		selectedMarker,
		mapRegionChanged,
		markers: { activeMarkers, totalMarkers },
		mapCenter
	} = useSearchResultsMapWithBackend(MAX_POINTS_TO_SHOW);

	const MAP = useRef(null);
	const MAPS = useRef(null);
	const [editing, setEditing] = useState(false);
	const [zoomPrecio, setZoomPrecio] = useState(false);
	const [coords, setCoords] = useState(null);

	const handleApiLoaded = (__map, __maps) => {
		MAP.current = __map;
		MAPS.current = __maps;
		MAPS.current.editing = editing;
		MAPS.current.drawing = false;
		MAPS.current.drawingPolygon = null;
		MAPS.current.savedPolygon = null;
		MAPS.current.event.addDomListener(MAP.current.getDiv(), "mousedown", function(e) {
			if (MAPS.current.editing) {
				if (MAPS.current.drawingPolygon) {
					MAPS.current.drawingPolygon.setMap(null);
				}
				MAPS.current.drawingPolygon = new MAPS.current.Polyline({
					map: MAP.current,
					clickable: false,
				});
				MAPS.current.drawing = true;
			}
			MAPS.current.event.addListener(MAP.current, "mousemove", function(e) {
				if (MAPS.current.drawing) {
					MAPS.current.drawingPolygon.getPath().push(e.latLng);
				}
			});
			MAPS.current.event.addListenerOnce(MAP.current, "mouseup", function(e) {
				if (MAPS.current.drawing) {
					MAPS.current.drawing = false;
					setEditing(false);
					setPolygon(
						MAPS.current.drawingPolygon
							.getPath()
							.getArray()
							.map(o => {
								return {
									latitude: o.lat(),
									longitude: o.lng(),
								};
							})
					);
					MAPS.current.drawingPolygon.setMap(null);
				}
			});
		});

		updatePolygon();
		updateMapOptions();
	};
	const updateMapOptions = () => {
		if (!MAP.current && !MAPS.current) return;
		MAP.current.setOptions({
			clickableIcons: false,
			draggable: !editing,
			draggableCursor: editing ? "crosshair" : "grab",
			zoomControl: !editing,
			scrollwheel: !editing,
			disableDoubleClickZoom: editing,
		});
	};
	const updatePolygon = () => {
		if (!MAP.current && !MAPS.current) return;
		if (MAPS.current.savedPolygon !== null) {
			MAPS.current.savedPolygon.setMap(null);
		}
		if (polygon.length === 0) return;
		MAPS.current.savedPolygon = new MAPS.current.Polygon({
			paths: polygon.map(o => {
				return { lat: o.latitude, lng: o.longitude };
			}),
			strokeColor: theme.colors.primaryColor,
			strokeOpacity: 1,
			strokeWeight: 2,
			fillColor: theme.colors.primaryColor,
			fillOpacity: 0.4,
			clickable: false,
			map: MAP.current,
		});

		let bounds = new MAPS.current.LatLngBounds();
		MAPS.current.savedPolygon.getPath().forEach(function(latLng) {
			bounds.extend(latLng);
		});

		MAP.current.fitBounds(bounds);
	};
	const enableGPS = () => {
		navigator.geolocation.getCurrentPosition(
			x => {
				setEditing(false);
				MAP.current.setZoom(MIN_ZOOM_TO_SHOW_PRICE_MARKER);
				MAP.current.setCenter({
					lat: x.coords.latitude,
					lng: x.coords.longitude,
				});
			},
			x => {
				//@todo
				console.error(x);
			}
		);
	};

	const updateInfoWindowPosition = markerBoundaries => {
		const CARD_HEIGHT = 350;
		const mapBoundaries = MAP.current.getDiv().getBoundingClientRect();
		const mapWidth = mapBoundaries.right - mapBoundaries.left;
		const mapHeight = mapBoundaries.bottom - mapBoundaries.top;
		const safe_height_top = mapBoundaries.top + mapHeight/2 - 100;
		const safe_height_bottom = mapBoundaries.top + mapHeight/2 + 100;
		let newCoords = { left: null, right: null, top: null, bottom: null};


		if(markerBoundaries.left < mapBoundaries.left + mapWidth/2){
			newCoords.left = markerBoundaries.left - mapBoundaries.left + PIXELS_MARGIN;
			if (zoomPrecio) newCoords.left += 30;
		} else {
			newCoords.right = mapBoundaries.right - markerBoundaries.right + PIXELS_MARGIN;
			if (zoomPrecio) newCoords.right += 30;
		}

		if(markerBoundaries.top < safe_height_top){
			newCoords.top = PIXELS_MARGIN;
		} else if (markerBoundaries.top >= safe_height_top && markerBoundaries.top <= safe_height_bottom){
			newCoords.top = markerBoundaries.top - mapBoundaries.top - CARD_HEIGHT/2;
		} else {
			newCoords.bottom = PIXELS_MARGIN;
		}

		setCoords(newCoords);
	};
	const startDrawing = () => {
		setEditing(true);
		setPolygon(null);
	};
	const deleteDrawing = () => {
		setEditing(false);
		setPolygon(null);
	};
	const handleClick = x => selectMarker(0);
	const handleChildClick = (x, y) => selectMarker(x);

	useEffect(() => updatePolygon(), [polygon]);
	useEffect(() => {
		if (MAPS.current) MAPS.current.editing = editing;
		updateMapOptions();
	}, [editing]);
	useEffect(() => {
		if (activeMarkers.length > 0)
			GTM.Event({
				name: "searchresults",
				data: {
					listing_id: activeMarkers.map(p => p.id),
					listing_totalvalue: activeMarkers.map(p => p.price.amount),
					listing_pagetype: "searchresults-map",
				},
			});
	}, [activeMarkers]);

	/*
	useEffect(() => {
		if (selectedMarker > 0 && !zoomPrecio) {
			const y = activeMarkers.find(o => o.id == selectedMarker);
			MAP.current.setZoom(MIN_ZOOM_TO_SHOW_PRICE_MARKER);
			MAP.current.setCenter({ lat: y.latitude, lng: y.longitude });
		}
	}, [selectedMarker]);
	*/

	if(!mapCenter){ return <></>; }

	return  (
		<>
			<div className="map-container">
				{/* Card Default */}
				<ListingInfoWindow
					onClose={handleClick}
					show={selectedMarker > 0}
					listingId={selectedMarker}
					coords={coords}
				/>

				{/* SearchResultDisplay */}
				<div className="map-search-result">
					<Space direction="vertical" size={0}>
						<Text strong>
							<SearchResultDisplay
								activeMarkers={activeMarkers.length}
								totalMarkers={totalMarkers}
								loading={loading || error}
							/>
						</Text>
						<Text style={{ fontSize: 12 }}>
							{t("Mueve el mapa o haz zoom para buscar más")}
						</Text>
					</Space>
				</div>

				{/* ButtonsActionsMap */}
				<div className={"map-search-buttons"}>
					<Space direction="vertical" size={theme.spacing.smSpacing}>
						<Button
							ghost={editing}
							onClick={() => startDrawing()}
							className={"secondary"}>
							<Space direction="vertical" size={-2}>
								<EditOutlined />
								{t("Dibujar")}
							</Space>
						</Button>
						<Button
							disabled={polygon.length == 0}
							onClick={() => deleteDrawing()}
							className={"secondary"}>
							<Space direction="vertical" size={-2}>
								<DeleteOutlined />
								{t("Borrar")}
							</Space>
						</Button>
						<Button onClick={() => enableGPS()} className={"secondary"}>
							<Space direction="vertical" size={-2}>
								<CompassOutlined />
								{t("GPS")}
							</Space>
						</Button>
					</Space>
				</div>

				{/* Map */}
				<GoogleMapReact
					bootstrapURLKeys={{ key: "AIzaSyDBA9FLlSyCGsbvPX-IGZm5BdAFnM04zUw" }}
					defaultCenter={mapCenter}
					options={{ fullscreenControl: false }}
					defaultZoom={DEFAULT_ZOOM_LEVEL}
					onDrag={x => {
						selectMarker(0);
					}}
					onChange={x => {
						selectMarker(0)
						const bounds = [
							{ latitude: x.bounds.ne.lat, longitude: x.bounds.ne.lng },
							{ latitude: x.bounds.nw.lat, longitude: x.bounds.nw.lng },
							{ latitude: x.bounds.sw.lat, longitude: x.bounds.sw.lng },
							{ latitude: x.bounds.se.lat, longitude: x.bounds.se.lng },
							{ latitude: x.bounds.ne.lat, longitude: x.bounds.ne.lng },
						];
						const zoomLevel = x.zoom;

						if (!zoomPrecio && zoomLevel >= MIN_ZOOM_TO_SHOW_PRICE_MARKER) {
							setZoomPrecio(true);
						} else if (zoomPrecio && zoomLevel < MIN_ZOOM_TO_SHOW_PRICE_MARKER) {
							setZoomPrecio(false);
						}

						if (!(polygon && polygon.length > 0 && totalMarkers < 100)) {
							mapRegionChanged(bounds, zoomLevel);
						}
					}}
					onClick={handleClick}
					distanceToMouse={(markerPos, mousePos, markerProps) => {
						const x = markerPos.x + K_LISTING_MARKER_SIZE / 2;
						const y = markerPos.y + K_LISTING_MARKER_SIZE / 2;

						// and i want that hover probability on markers with text === 'A' be greater than others
						// so i tweak distance function (for example it's more likely to me that user click on 'A' marker)
						// another way is to decrease distance for 'A' marker
						// this is really visible on small zoom values or if there are a lot of markers on the map
						const distanceKoef = 1; //markerProps.text !== 'A' ? 1.5 : 1;

						// it's just a simple example, you can tweak distance function as you wish
						return (
							distanceKoef *
							Math.sqrt(
								(x - mousePos.x) * (x - mousePos.x) +
									(y - mousePos.y) * (y - mousePos.y)
							)
						);
					}}
					hoverDistance={K_LISTING_MARKER_SIZE + 0}
					yesIWantToUseGoogleMapApiInternals
					onGoogleApiLoaded={x => handleApiLoaded(x.map, x.maps)}>
					{activeMarkers.map(m => {
						return (
							// Banderin Precio
							<ListingMarker
								key={m.id}
								id={m.id}
								lat={m.latitude}
								lng={m.longitude}
								text={m.id}
								active={selectedMarker === m.id}
								zoomPrecio={zoomPrecio}
								updateInfoWindowPosition={updateInfoWindowPosition}
								onClick={handleChildClick}
							/>
						);
					})}
				</GoogleMapReact>
			</div>
			<RTBhouse page={"listing"} data={activeMarkers.map(p => p.id)} />
			<style jsx global>{`
				.map-container .map-search-result {
					background-color: ${theme.colors.backgroundColor};
					border-radius: ${theme.spacing.smSpacing}px;
					padding: ${theme.spacing.smSpacing}px;
					top: ${theme.spacing.xlSpacing}px;
				}
				.map-container .map-search-buttons {
					padding: ${theme.spacing.smSpacing}px;
				}
				.map-container .map-search-buttons .ant-btn {
					padding: ${theme.spacing.xsSpacing}px;
					border-radius: ${theme.spacing.xsSpacing}px;
					font-size: ${theme.fontSizes.xsFontSize};
					line-height: ${theme.fontSizes.xsLineHeight};
				}
				.map-container .map-search-buttons .ant-btn .anticon {
					font-size: ${theme.fontSizes.baseFontSize};
				}
			`}</style>
		</>
	);
}

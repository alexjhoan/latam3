import "./styles.less";

import { Pagination, Skeleton, Space } from "antd";

import { formatMoney } from "../../../../../Utils/Functions";
import { useSearchResultsPagination } from "./SearchResultsPagination.hook";

export const SearchResultsPagination = () => {
	const {
		changePage,
		error,
		loading,
		data: { currentPage, perPage, total },
	} = useSearchResultsPagination();

	if (loading) {
		return (
			<Space>
				<Skeleton.Button style={{ width: "45px" }} active />
				<Skeleton.Button style={{ width: "45px" }} active />
				<Skeleton.Button style={{ width: "45px" }} active />
				<Skeleton.Button style={{ width: "45px" }} active />
			</Space>
		);
	}
	if (error) return null;

	const itemRender = (page, type: "page" | "prev" | "next", originalElement) => {
		if (page >= 1000 && type === "page") {
			return <a>{formatMoney(page)}</a>;
		}

		return originalElement;
	};

	return (
		<Pagination
			responsive
			className="search-results-pagination"
			total={total}
			showSizeChanger={false}
			current={currentPage}
			pageSize={perPage}
			hideOnSinglePage={true}
			itemRender={itemRender}
			size="default"
			onChange={page => {
				window.scrollTo(0, 0);
				changePage(page);
			}}
			showLessItems={true}
		/>
	);
};

import "./styles.less";

import { ImagenOptimizada as Img } from "../../../Components/Image/web";
import { useTheme } from "../../../Styles/ThemeHook";

export const PicHomeFooter = () => {
	const { theme } = useTheme();
	return (
		<>
			<div className="pic-home-footer-container">
				<div className="pic-home-footer">
					<Img
						src={
							"https://cdn1.infocasas.com.uy/web/5f771b79c47c0_infocdn__montevideo.png"
						}
						alt={"Montevideo"}
						className={"pic-home-footer-image"}
					/>
				</div>
			</div>
			<style jsx>{`
				.pic-home-footer-container {
					box-shadow: inset 0px -1px 0px ${theme.colors.borderColor};
					border-bottom: ${theme.spacing.xsSpacing}px solid ${theme.colors.primaryColor};
				}
			`}</style>
		</>
	);
};

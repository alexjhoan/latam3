import "./styles.less";

import { Col, Row, Tooltip, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../../Components/Image/web";
import getConfig from "next/config";
import { removeUrlProtocol } from "../../../../Utils/Functions";
import { useAppLinks } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";

const { NODE_ENV, APP_NAME, APP_VERSION } = getConfig().publicRuntimeConfig;

const { Link } = Typography;

const appLinkIcons = {
	appstore: "https://cdn2.infocasas.com.uy/web/5e8b905da033d_infocdn__appstore.png",
	googleplay: "https://cdn2.infocasas.com.uy/web/5e8b905da2050_infocdn__googleplay.png",
};

export const AppLinks = () => {
	const { appLinks } = useAppLinks();
	const { theme } = useTheme();

	return (
		<>
			<div className="app-links">
				<Row>
					<Col span={5}>
						<Tooltip title={APP_NAME + "-" + APP_VERSION + "-" + NODE_ENV}>
							<div className="phone">
								<Img
									src={
										"https://cdn1.infocasas.com.uy/web/5f592e3a61ef9_infocdn__iphone-svgrepo-com-1.png"
									}
									alt={"Phone"}
								/>
							</div>
						</Tooltip>
					</Col>
					<Col span={19}>{appLinks.map(AppLink)}</Col>
				</Row>
			</div>
			<style jsx global>{`
				.app-link {
					margin: ${theme.spacing.smSpacing}px 0 0 5px;
				}
			`}</style>
		</>
	);
};

const AppLink = ({ id, name, url, display_name }) => {
	const { theme } = useTheme();
	return (
		<Link
			key={id}
			className="app-link"
			title={display_name}
			href={removeUrlProtocol(url)}
			target="_blank">
			<div className={name}>
				<Img src={appLinkIcons[name]} alt={display_name} />
			</div>
		</Link>
	);
};

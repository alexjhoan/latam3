export const useAppLinks = () => {
	return {
		appLinks: [
			{
				id: 1,
				name: "appstore",
				display_name: "App Store",
				url: "https://itunes.apple.com/uy/app/infocasas/id1126880888?mt=8",
			},
			{
				id: 2,
				name: "googleplay",
				display_name: "Google Play Store",
				url:
					"https://play.google.com/store/apps/details?id=uy.com.infocasas.infoapp",
			},
		],
	};
};

import "./styles.less";

import { Col, Row, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../../Components/Image/web";
import { removeUrlProtocol } from "../../../../Utils/Functions";
import { useCountries } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";

const { Link } = Typography;

export const Flags = () => {
	const { theme } = useTheme();
	const { countries } = useCountries();

	return (
		<>
			<div className="flags">
				<Row gutter={[theme.spacing.smSpacing, theme.spacing.smSpacing]}>
					{countries.map(country => (
						<Col key={"flags" + country.id}>
							<Link href={removeUrlProtocol(country.url)} title={country.name}>
								<Img src={country.country_flag} alt={country.name} />
							</Link>
						</Col>
					))}
				</Row>
			</div>
			<style jsx global>{`
				.flags img {
					box-shadow: 0px 1px 2px ${theme.colors.textInverseColor};
					border-radius: ${theme.spacing.mdSpacing}px;
				}
			`}</style>
		</>
	);
};

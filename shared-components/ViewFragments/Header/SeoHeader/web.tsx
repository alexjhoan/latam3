import "./styles.less";

import { Menu, Typography } from "antd";
import React, { useState } from "react";

import { SeoHeaderBanner } from "./SeoHeaderBanner/web";
import { SeoHeaderDropdown } from "./SeoHeaderDropdown/web";
import { useSeoHeader } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Link } = Typography;

export const SeoHeaderDesktop = () => {
	const { theme } = useTheme();
	const { headerLinks } = useSeoHeader();

	return (
		<>
			<Menu className="header_desktop_menu" mode="horizontal">
				{headerLinks.map(({ link, title, banner, links }, i) => {
					if (links.length == 0) {
						return (
							<Menu.Item key={"seoHeader_" + i} className={"header_desktop_menu_li"}>
								<Link className="header_desktop_menu_a" href={link}>
									{title}
								</Link>
							</Menu.Item>
						);
					}
					return (
						<Menu.SubMenu
							popupOffset={[0, 0]}
							popupClassName="menu-dropdown-popup"
							className="header_desktop_menu_li"
							key={"seoHeader_submenu_" + i}
							title={
								<Link className="header_desktop_menu_a" href={link}>
									{title}
								</Link>
							}>
							<SeoHeaderDropdown links={links} banner={banner} />
						</Menu.SubMenu>
					);
				})}
			</Menu>
			<style jsx global>{`
				.header_desktop_menu .header_desktop_menu_li .header_desktop_menu_a {
					border-radius: ${theme.spacing.smSpacing}px;
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px;
					color: ${theme.colors.textColor};
					background-color: ${theme.colors.backgroundColor};
				}

				.header_desktop_menu .header_desktop_menu_li:hover .header_desktop_menu_a {
					color: ${theme.colors.backgroundColor};
					background-color: ${theme.colors.secondaryHoverColor};
				}
			`}</style>
		</>
	);
};

export const SeoHeaderMobile = () => {
	const { theme } = useTheme();
	const { headerLinks } = useSeoHeader();
	const [bannerKey, setBannerKey] = useState(null);

	const changeBannerKey = (e: string[]) => {
		if (e.length > 0) {
			const res = e.filter(s => s.split("_").length == 2);
			if (res.length > 0) setBannerKey(Number(res[0].split("_")[1]));
		} else setBannerKey(null);
	};

	return (
		<>
			<Menu className="header-mobile-menu" mode="inline" onOpenChange={changeBannerKey}>
				{headerLinks.map(({ link, title, links }, i) => {
					if (links.length == 0) {
						return (
							<Menu.Item key={"seoHeader_" + i}>
								<Link href={link}>{title}</Link>
							</Menu.Item>
						);
					}

					return (
						<Menu.SubMenu key={"seoHeader_" + i} title={title}>
							{links.map(({ link, title, categories }, j) => {
								if (categories.length == 0) {
									return (
										<Menu.Item key={"seoHeader_" + i + "_" + j}>
											<Link href={link}>{title}</Link>
										</Menu.Item>
									);
								}

								return (
									<Menu.SubMenu key={"seoHeader_" + i + "_" + j} title={title}>
										{categories.map(({ links, title }, k) => {
											if (links.length == 0) {
												return (
													<Menu.Item
														key={"seoHeader_" + i + "_" + j + "_" + k}>
														<Link href={link}>{title}</Link>
													</Menu.Item>
												);
											}

											return (
												<Menu.ItemGroup
													key={"seoHeader_" + i + "_" + j + "_" + k}
													title={title}>
													{links.map(({ link, title }, l) => {
														return (
															<Menu.Item
																key={
																	"seoHeader_" +
																	i +
																	"_" +
																	j +
																	"_" +
																	k +
																	"_" +
																	l
																}>
																<Link href={link}>{title}</Link>
															</Menu.Item>
														);
													})}
												</Menu.ItemGroup>
											);
										})}
									</Menu.SubMenu>
								);
							})}
						</Menu.SubMenu>
					);
				})}
				{bannerKey != null && headerLinks[bannerKey].banner.img && (
					<SeoHeaderBanner banner={headerLinks[bannerKey].banner} />
				)}
			</Menu>
			<style jsx global>{`
				.header-mobile-menu {
					color: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu-submenu-title,
				.header-mobile-menu.ant-menu .ant-menu-item {
					padding: 0 ${theme.spacing.lgSpacing}px !important;
					border-radius: ${theme.spacing.smSpacing}px;
					color: ${theme.colors.backgroundColor};
					margin: ${theme.spacing.xsSpacing}px 0 !important;
				}
				.header-mobile-menu .ant-menu-item a.ant-typography,
				.header-mobile-menu .ant-menu-item a.ant-typography {
					color: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu-submenu-title:hover,
				.header-mobile-menu .ant-menu-submenu .ant-menu-submenu-title:active,
				.header-mobile-menu .ant-menu-item:hover,
				.header-mobile-menu .ant-menu-item :focus {
					color: ${theme.colors.textColor};
					background-color: ${theme.colors.backgroundColor};
				}
				.header-mobile-menu .ant-menu-item:hover a.ant-typography,
				.header-mobile-menu .ant-menu-item a.ant-typography:hover {
					color: ${theme.colors.textColor};
				}

				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::before,
				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title
					.ant-menu-submenu-arrow::after {
					background: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title:hover
					.ant-menu-submenu-arrow::before,
				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title:hover
					.ant-menu-submenu-arrow::after,
				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title:active
					.ant-menu-submenu-arrow::before,
				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu-submenu-title:active
					.ant-menu-submenu-arrow::after {
					background: ${theme.colors.textColor};
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu {
					padding: 0 0 0 ${theme.spacing.mdSpacing}px;
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu .ant-menu-item {
					padding: 0 ${theme.spacing.lgSpacing}px !important;
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu
					.ant-menu-item
					a.ant-typography:active,
				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu
					.ant-menu-item
					a.ant-typography:hover {
					color: ${theme.colors.textColor} !important;
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu .ant-menu-item:active,
				.header-mobile-menu .ant-menu-submenu .ant-menu .ant-menu-item:hover {
					background: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu .ant-menu-submenu .ant-menu .ant-menu-item a.ant-typography,
				.header-mobile-menu .ant-menu-submenu .ant-menu .ant-menu-item .ant-typography a {
					color: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu
					.ant-menu-submenu
					.ant-menu
					.ant-menu-item-group
					.ant-menu-item-group-title {
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.lgSpacing}px;
				}

				.header-mobile-menu .banner_seoheader {
					background: ${theme.colors.backgroundColor};
				}

				.header-mobile-menu .banner_seoheader .row-container .ant-space {
					margin-left: ${theme.spacing.mdSpacing}px;
				}

				.header-mobile-menu .banner_seoheader .row-container .ant-space .ant-space-item {
					margin-bottom: ${theme.spacing.xlSpacing}px !important;
				}
			`}</style>
		</>
	);
};

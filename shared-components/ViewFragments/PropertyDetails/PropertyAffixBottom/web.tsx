import "./styles.less";
import { Affix } from "antd";
import { LeadButtons } from "../../../Components/Property/InformationRequest/LeadButtons/web";
import React from "react";
import { useTheme } from "../../../Styles/ThemeHook";

export const PropertyAffixBottom = ({ id, mode }) => {
	const { theme } = useTheme();

	return (
		<React.Fragment>
			<Affix className={"pd_affix_bottom"} offsetBottom={0}>
				<div className="pd_affix_bottom_content">
					<LeadButtons id={id} mode={mode} />
				</div>
			</Affix>
			<style jsx global>
				{`
					.pd_affix_bottom .pd_affix_bottom_content {
						padding: ${theme.spacing.mdSpacing}px;
						background: ${theme.colors.backgroundColor};
					}
				`}
			</style>
		</React.Fragment>
	);
};

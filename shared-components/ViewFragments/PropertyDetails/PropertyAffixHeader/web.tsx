import "./styles.less";

import { Affix, Col, Row, Space } from "antd";

import { Favorite } from "../../../Components/Property/Favorite/web";
import { FormButton } from "../../../Components/Property/InformationRequest/LeadButtons/FormButton/web";
import { LocationTag } from "../../../Components/Property/LocationTag/web";
import { PriceTag } from "../../../Components/Property/PriceTag/web";
import React from "react";
import { SocialShare } from "../../../Components/Property/SocialShare/web";
import { Title } from "../../../Components/Property/Title/web";
import { isTemporal } from "../../../Utils/Functions";
import { useTheme } from "../../../Styles/ThemeHook";

export const PropertyAffixHeader = ({ id, operationType }) => {
	const { theme } = useTheme();
	return (
		<>
			<Affix className={"pd_affix_top"} offsetTop={theme.headerHeight}>
				<div className="container pd-container">
					<Row className={"pd-affix-row"} gutter={theme.spacing.xlSpacing}>
						<Col span={8}>
							<Title id={id} level={4} hideInformation={true} limitLength={1} />
							<LocationTag id={id} />
						</Col>
						{!isTemporal(operationType) ? (
							<Col flex={1}>
								<PriceTag id={id} showExpenses={true} skeleton="fixed" />
							</Col>
						) : null}
						<Col>
							<Space size={theme.spacing.lgSpacing}>
								<Favorite id={id} />
								<SocialShare id={id} />
								<FormButton id={id} />
							</Space>
						</Col>
					</Row>
				</div>
			</Affix>
			<style jsx global>{`
				.pd_affix_top .ant-affix {
					background: ${theme.colors.backgroundColor};
				}
				.pd-affix-row {
					padding: ${theme.spacing.mdSpacing}px 0;
				}
			`}</style>
		</>
	);
};

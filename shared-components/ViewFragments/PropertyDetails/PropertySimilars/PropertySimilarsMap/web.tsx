import React from "react";
import {LeafletMapDynamic} from "../../../../Components/Map/LeafletMap/LeafletMapDynamic/web";
import {usePropertySimilarsMap} from "./hook";

export function PropertySimilarsMap({id}) {

    const {data, loading, error} = usePropertySimilarsMap({property_id: id});

    if(!data || error) {
        return <span>Loading PropertySimilarsMap...</span>
    }


    const markers = data.map(o => {
        return {...o, icon:o.id==id?"primary":"secondary"}
    });

    return (<>

        <h1>MAPA SIMILARES</h1>
        <div style={{width:"100%",height:"500px",display:"flex",backgroundColor:"red"}}>
            <LeafletMapDynamic defaultZoom={15} center={data[0]} markers={markers} options={{scrollWheelZoom: false}}  />
        </div>
    </>)
}

import { PropertySimilarsCards } from "./PropertySimilarsCards/web";
import { PropertySimilarsCharts } from "./PropertySimilarsCharts/web";
import { BigDataProperties } from "../../../Components/Property/BigDataProperties/web";
import { PropComponentMode } from "../../../Components/Property/PropertyInterfaces";

type PropertySimilarsProps = {
	id: string;
	mode: PropComponentMode;
	isTemporal?: boolean;
};

export function PropertySimilars({
	id,
	mode = "property",
	isTemporal = false,
}: PropertySimilarsProps) {
	return (
		<>
			{mode === "property" && !isTemporal && <PropertySimilarsCharts id={id} /> }
			{mode === "property" && !isTemporal && (
				<BigDataProperties id={id} componentTitle="Tabla Comparativa de Precios" />
			)}
			{/* {mode === 'property') && <PropertySimilarsMap id={id}/>} */}
			<PropertySimilarsCards id={id} mode={mode} />
		</>
	);
}

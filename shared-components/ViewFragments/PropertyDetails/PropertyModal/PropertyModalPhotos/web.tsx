import React, { useState } from "react";
import { Carousel, Space, Typography } from "antd";
import { CarouselArrow } from "../../../../Components/Property/LazyImageGallery/CarouselArrow/web";
import { useTheme } from "../../../../Styles/ThemeHook";
import { usePropertyCover } from "../../PropertyCover/hook";
import { PropComponentMode } from "../../../../Components/Property/PropertyInterfaces";
import { ImagenOptimizada } from "../../../../Components/Image/web";
import "./styles.less";

const { Text } = Typography;
export const PropertyModalPhotos = ({
	id,
	mode = "auto",
}: {
	id: string;
	mode: PropComponentMode;
}) => {
	const { data, error, loading } = usePropertyCover(id, mode);
	const { theme } = useTheme();
	const [current, setCurrent] = useState(0);
	if (error) return null;
	if (loading) return <>...</>;

	const settings = {
		dots: false,
		arrows: true,
		accessibility: true,
		swipeToSlide: true,
		nextArrow: <CarouselArrow side={"right"} />,
		prevArrow: <CarouselArrow side={"left"} />,
		infinite: true,
		autoplay: false,
		slidesToScroll: 1,
		slidesToShow: 1,
		speed: 300,
		afterChange: index => setCurrent(index),
		style: {
			height: "100%",
		},
	};

	return (
		<>
			<div className={"property-modal-photos"}>
				<Carousel {...settings}>
					{data?.images.map((g, i) => {
						return (
							<div className={"gallery-image"} key={"Gallery_" + i}>
								<ImagenOptimizada src={g.image} />
							</div>
						);
					})}
				</Carousel>
				<div className="photos-footer">
					<Space size={theme.spacing.xsSpacing}>
						<Text>{current + 1}</Text>
						<Text>/</Text>
						<Text>{data?.images.length}</Text>
					</Space>
				</div>
			</div>
			<style jsx>{`
				.property-modal-photos .photos-footer {
					border-color: ${theme.colors.textColor};
					padding: 0 ${theme.spacing.lgSpacing}px;
				}
				.property-modal-photos .photos-footer :global(.ant-typography) {
					color: ${theme.colors.textInverseColor};
					font-size: ${theme.fontSizes.smFontSize};
				}
			`}</style>
		</>
	);
};

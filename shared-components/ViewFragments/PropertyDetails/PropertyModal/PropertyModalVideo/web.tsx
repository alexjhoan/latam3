import React from "react";
import { usePropertyModalVideo } from "./PropertyModalVideo.hook";
import { Button, Empty, message } from "antd";
import { useTranslation } from "react-i18next";
import Text from "antd/lib/typography/Text";
import { useTheme } from "../../../../Styles/ThemeHook";

export function PropertyModalVideo({ id, mode }) {
	const { loading, data, loadingSendLead, error, sendLead } = usePropertyModalVideo({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (loading) return <span>...</span>;

	if (!data.youtube) {
		return (
			<Empty
				imageStyle={{
					height: 50,
					opacity: 0.9,
				}}
				description={
					<Text style={{ color: theme.colors.textInverseColor }}>
						{t("La propiedad no tiene video")}
					</Text>
				}>
				<Button
					loading={loadingSendLead}
					onClick={() =>
						sendLead().then(response => {
							response.success
								? message.success("Consulta enviada")
								: message.error("Error al enviar la consulta");
						})
					}
					ghost>
					{t("Pedir un video")}
				</Button>
			</Empty>
		);
	}

	return (
		<iframe
			frameBorder="0"
			scrolling="no"
			marginHeight={0}
			marginWidth={0}
			width="100%"
			height="100%"
			src={
				data.youtube +
				"?autoplay=0&fs=1&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0"
			}
		/>
	);
}

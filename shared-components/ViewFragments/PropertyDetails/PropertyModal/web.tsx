import React, { useEffect, useState } from "react";
import { usePropertyModal } from "./PropertyModal.hook";
import { PropertyModalVideo } from "./PropertyModalVideo/web";
import { PropertyModalFloorPlans } from "./PropertyModalFloorPlans/web";
import { PropertyModalBigData } from "./PropertyModalBigData/web";
import { PropertyModalWorkProgress } from "./PropertyModalWorkProgress/web";
import { PropertyMap } from "../../../Components/Property/PropertyMap/web";
import { PropertyStreetView } from "../../../Components/Property/PropertyStreetView/web";
import { Tabs, Col, Row, Space, Grid } from "antd";
import { Favorite } from "../../../Components/Property/Favorite/web";
import { SocialShare } from "../../../Components/Property/SocialShare/web";
import { useTranslation } from "react-i18next";
import { InformationRequest } from "../../../Components/Property/InformationRequest/web";
import { PropComponentMode } from "../../../Components/Property/PropertyInterfaces";
import { useTheme } from "../../../Styles/ThemeHook";
import { PropertyModalPhotos } from "./PropertyModalPhotos/web";
import "./styles.less";
import { FormButton } from "../../../Components/Property/InformationRequest/LeadButtons/FormButton/web";
import { FullScreen } from "../../../Components/FullScreen/web";

const { TabPane } = Tabs;
const { useBreakpoint } = Grid;

export function PropertyModal({
	id,
	activeTab,
	mode = "auto",
	onChangeTab,
}: {
	id: any;
	activeTab: string;
	mode: PropComponentMode;
	onChangeTab: (tab: any) => void;
}) {
	const { data, loading, error, isProject } = usePropertyModal({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	const screen = useBreakpoint();

	const callback = tab => onChangeTab(tab);
	const extrasButtons = (
		<Space size={theme.spacing.mdSpacing}>
			<Favorite id={id} />
			<SocialShare id={id} mode={mode} />
		</Space>
	);

	if (loading) return <div>{t("Loading PropertyModal...")}</div>;

	return (
		<FullScreen
			className="property-modal-container"
			offset={screen.lg ? theme.spacing.xxlSpacing * 2 : 0}>
			<Row className={"tabrow"} style={{ position: "relative" }}>
				<Col span={24} className={"content-column"}>
					<Tabs
						className="tabs-property-modal"
						activeKey={activeTab}
						onChange={callback}
						tabBarExtraContent={extrasButtons}
						tabBarGutter={theme.spacing.mdSpacing}>
						<TabPane tab={t("Fotos")} key="photos" className="pm-tab-pane black-pane">
							<Row>
								<Col span={24} lg={16}>
									<PropertyModalPhotos id={id} mode={mode} />
								</Col>
							</Row>
						</TabPane>

						<TabPane
							tab={t("Video")}
							key="video"
							disabled={data.youtube != null}
							className="pm-tab-pane black-pane pm-tab-pane-video">
							<Row>
								<Col span={24} lg={16}>
									<PropertyModalVideo id={id} mode={mode} />
								</Col>
							</Row>
						</TabPane>

						<TabPane
							tab={t("Mapa")}
							key="map"
							className="pm-tab-pane pm-tab-pane-map"
							disabled={!data.latitude || !data.longitude}>
							<Row>
								<Col span={24}>
									<PropertyMap
										id={id}
										width={"100%"}
										height={"100%"}
										mode={mode}
									/>
								</Col>
							</Row>
						</TabPane>

						<TabPane
							disabled={!data.latitude || !data.longitude}
							tab={t("Street View")}
							key="streetView"
							className="pm-tab-pane pm-tab-pane-map">
							<Row>
								<Col span={24}>
									<PropertyStreetView
										id={id}
										width={"100%"}
										height={"100%"}
										mode={mode}
									/>
								</Col>
							</Row>
						</TabPane>

						{/* {!isProject && (
							<TabPane
								tab={t("Big Data")}
								key="bigData"
								className="pm-tab-pane pm-tab-bigdata">
								<Row>
									<Col span={24} lg={16}>
										<PropertyModalBigData id={id} />
									</Col>
								</Row>
							</TabPane>
						)} */}

						{isProject && (
							<TabPane
								tab={t("Planos")}
								key="floorPlans"
								className="pm-tab-pane pm-tab-pane-plans">
								<Row>
									<Col span={24} lg={16}>
										<PropertyModalFloorPlans id={id} mode={mode} />
									</Col>
								</Row>
							</TabPane>
						)}
						{isProject && (
							<TabPane
								tab={t("Avances de obra")}
								key="workProgress"
								className="pm-tab-pane black-pane pm-tab-pane-workprogress">
								<Row>
									<Col span={24} lg={16}>
										<PropertyModalWorkProgress id={id} mode={mode} />
									</Col>
								</Row>
							</TabPane>
						)}
					</Tabs>
				</Col>
				<Col className={"information-request-container"} span={0} lg={8}>
					<div className={"information-request-content"}>
						<InformationRequest id={id} mode={mode} showDisclaimer={false} />
					</div>
				</Col>

				<Col className={"information-request-container-mobile"} span={24} lg={0}>
					<FormButton id={id} size={"large"} />
				</Col>
				<style jsx global>
					{`
						.pd-property-modal
							.ant-modal-content
							.ant-modal-body
							.tabs-property-modal
							.ant-tabs-nav {
							padding: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing * 7}px
								${theme.spacing.smSpacing}px ${theme.spacing.lgSpacing}px;
						}
						.pd-property-modal
							.ant-modal-content
							.ant-modal-body
							.tabs-property-modal
							.ant-tabs-nav
							.ant-tabs-tab {
							padding: ${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px;
						}

						.pd-property-modal
							.ant-modal-content
							.ant-modal-body
							.tabs-property-modal
							.ant-tabs-nav
							.ant-tabs-tab.ant-tabs-tab-active {
							border-color: ${theme.colors.borderColor};
							border-radius: ${theme.spacing.smSpacing}px;
						}
						.information-request-container,
						.information-request-container-mobile {
							padding: ${theme.spacing.smSpacing}px;
						}
						.information-request-container .information-request-content {
							padding: ${theme.spacing.smSpacing}px;
							background-color: ${theme.colors.backgroundColor};
							border-radius: ${theme.spacing.smSpacing}px;
						}
						.tabs-property-modal .ant-tabs-extra-content {
							padding-left: ${theme.spacing.mdSpacing}px;
						}
					`}
				</style>
			</Row>
		</FullScreen>
	);
}

import {useQuery} from "@apollo/client";
import {useAuthCheck} from "../Components/User/useAuthCheck";

export const useQuerySkipAuth = (query, options?) => {
    const {isLoggedIn} = useAuthCheck()
    options = options ?? {skip: false}
    options.skip = (options.skip ?? false) || !isLoggedIn;
    return useQuery(query, options);
}
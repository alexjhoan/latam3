import React from 'react'
import Fade from 'react-reveal/Fade';
import { useTheme } from "../shared-components/Styles/ThemeHook";

export default function Sponsor() {
	const { theme } = useTheme();

	return (
		<React.Fragment>
			<div id="Sponsor">
				<div className='containerLanding'>
          <p className="title">Participan:</p>
          <img src="/images/banner.jpg" alt="icon" />
        </div>
      </div>
      <style jsx>{`
      #Sponsor{
        background: #fff;
        padding: 60px 0;
      }
      #Sponsor p.title{
        font-size: 24px;
        text-align: center;
        font-weight: 700;
      }
      #Sponsor img {
        width: 100%;
      }
      
      `}</style>
    </React.Fragment>
  )
}
        		
import { Button } from 'antd';
import React from 'react'
import Fade from 'react-reveal/Fade';
import { useTheme } from "../shared-components/Styles/ThemeHook";

export default function OpenDay() {
	const { theme } = useTheme();

	return (
		<React.Fragment>
			<div id="openDay">
				<div className='containerLanding'>
					<Fade up>
						<div className='dRowCenter imgBanner'>
							<img src="/images/openDay-mobile.png" alt="" className='dMdNone'/>
							<img src="/images/openDay.png" alt="" className='dNone dMdBlock'/>
						</div>
					</Fade>
					{/* <Fade up>
						<div className='dRowCenter textBanner'>
							<p>Mirá la apertura con el lanzamiento de 8 proyectos</p>
						</div>
					</Fade> */}
					<Fade up>
						<div className='dRowCenter textBanner'>
							<div className="item">
								<img src="/images/icons/ocho.png" alt="" />
								<p className="titleItem">LANZAMIENTOS<br />EXCLUSIVOS</p>
								<p>Con promos especiales</p>
							</div>
							<div className="item">
							<img src="/images/icons/present.png" alt="" />
								<p className="titleItem">BONOS DE<br />USD 3.000</p>
								<p>Por compra, señando<br />durante el evento</p>
							</div>
							<div className="item">
							<img src="/images/icons/light.png" alt="" />
								<p className="titleItem">SEÑA FLASH<br />USD 500</p>
								<p>100% reembolsable</p>
							</div>
						</div>
					</Fade>
					<div className="containerVideo">
						<div className="theVideo">
							<iframe width="1090" height="613" src="https://www.youtube.com/embed/siY1_g03dWk"></iframe>
						</div>
					</div>
					{/* <div className="footerOpenDay">
						<div className="tagNumber">
							<p>MARTES</p>
							<p className="numberDay">23</p>
							<p>NOVIEMBRE</p>
						</div>
						<div className="legend">
							<p>¡Registrate para participar de la apertura!</p>
							<p className='info'>Revelaremos 8 proyectos que salen al mercado + Ofertas<br/>flash a las que solo podrás acceder durante la transmisión.</p>
							<div className="hours">
								<p><img src="/images/icons/clock.png" alt="" />11hs</p>
								<p><img src="/images/icons/zoom.png" alt="" />Vía Zoom</p>
							</div>
							<Button className='btn btnPrimary' href='https://infocasas.com.uy/latam-invierte/amp2' target='_blank'>
								Registrate para recibir el link
							</Button>
						</div>
					</div> */}
				</div>
			</div>
			<style jsx>{`
				#openDay {
					background-color: #DDD;
					position: relative;
				}
				.imgBanner img{
					width: 100%;
  				max-width: 700px;
				}
				.textBanner {
					justify-content: space-between;
					margin: 40px 0;
					align-items: flex-start;
				}
				.textBanner img{
					height: 74px;
					width: auto!important;
					margin-bottom: 30px;
				}
				.textBanner p{
					font-size: 24px;
					font-weight: 500;
					margin: 0;
					line-height: 1.2;
					max-width: 330px;
				}
				.textBanner p.titleItem{
					font-size: 42px;
					font-weight: 900;
				}
				.textBanner .item:hover img {
					transform: scale(1);
				}
				.containerVideo {
					max-width: 900px;
					margin: 60px auto;
				}
				.theVideo{
					padding-top: 56.26%;
					position: relative;
					width: 100%;
				}
				.footerOpenDay{
					display: flex;
					flex-direction: row;
					justify-content: center;
					gap: 25px;
				}
				.footerOpenDay .tagNumber {
					display: flex;
					flex-direction: column;
					justify-content: center;
					align-items: center;
					background: #fff;
					padding: 30px;
					border-radius: 20px;
					box-shadow: 0 3px 6px 0 #00000030;
					width: 170px;
				}
				.footerOpenDay .tagNumber p{
					font-size: 24px;
					margin-bottom: 0;
					font-weight: 900;
				}
				.footerOpenDay .tagNumber p.numberDay{
					font-size: 90px;
					line-height: 1;
				}
				.footerOpenDay .legend p {
					font-size: 24px;
					margin-bottom: 0;
					font-weight: 900;
				}
				.footerOpenDay .legend p.info {
					font-size: 16px;
					margin: 10px 0 20px;
					font-weight: 500;
				}
				.footerOpenDay .legend .hours{
					display: flex;
					flex-direction: row;
					column-gap: 30px;
					margin-bottom: 15px;
				}
				.footerOpenDay .legend .hours p img{
					width: 25px;
					height: auto;
					margin-right: 10px;
					transform: translateY(-3px);
				}

				@media (max-width: ${theme.breakPoints.xl}){
					.textBanner {
						justify-content: space-between;
						flex-direction: column;
						row-gap: 30px;
						align-items: flex-start;
					}
				}
				@media (max-width: ${theme.breakPoints.lg}){
					.containerDateP {
						width: 80%;
					}
				}
				@media (max-width: ${theme.breakPoints.md}){
					.imgBanner img{
						max-width: 350px;
					}
					.containerVideo {
						margin: 20px -15px 0;
					}
					.textBanner img{
						height: 60px;
						widht: auto;
						margin-bottom: 20px;
					}
					.textBanner p{
						font-size: 20px;
					}
					.textBanner p.titleItem{
						font-size: 36px;
					}
					.footerOpenDay{
						display: flex;
						flex-direction: column;
						align-items: center;
						justify-content: center;
						gap: 25px;
						margin-top: 50px;
					}
				}
				@media (max-width: ${theme.breakPoints.sm}){
					.containerDateP {
						width: 200px;
						padding: 20px 0;
					}
				}
				
			`}</style>
		</React.Fragment>
	)
}
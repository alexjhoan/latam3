import React from 'react'
import {Button} from "antd";
import Fade from 'react-reveal/Fade';
import { useTheme } from "../shared-components/Styles/ThemeHook";

export default function TheDay() {
	const { theme } = useTheme();

	return (
		<React.Fragment>
			<div id="theDay">
				<div className='containerLanding'>
					<Fade up>
						<div className='dColumnCenter textBanner'>
							<p>Día de asesoría presencial</p>
							<p>¡Te esperamos el sábado en tu ciudad!</p>
						</div>
					</Fade>					
					<div className="containerGrid">
						<Fade up>
							<div className='item'>
								<div className="itemText">
									<p className='title-item'><img src="/images/flags/uy.png" alt="" className='flag'/>Montevideo</p>
									<p>Hotel NH Columbia</p>
									<p>27 de noviembre</p>
									<p className='containerClock'><img src="/images/icons/clock.png" alt="clock" className='clock'/>9 a 17hs</p>
									<Button className='btn btnPrimary' href='https://www.infocasas.com.uy/latam-invierte-uy/amp' target='_blank'>
										Quiero Asistir
									</Button>
								</div>
							</div>
						</Fade>
						<Fade up>
							<div className='item'>
								<div className="itemText">
									<p className='title-item'><img src="/images/flags/uy.png" alt="" className='flag'/>Punta del Este</p>
									<p>Hotel Enjoy, salón Río de Janeiro</p>
									<p>04 de diciembre</p>
									<p className='containerClock'><img src="/images/icons/clock.png" alt="clock" className='clock'/>10 a 20hs</p>
									<Button className='btn btnPrimary' href='https://www.infocasas.com.uy/latam-invierte-uy/amp2' target='_blank'>
										Quiero Asistir
									</Button>
								</div>
							</div>
						</Fade>
						<Fade up>
							<div className='item'>
								<div className="itemText">
									<p className='title-item'><img src="/images/flags/py.png" alt="" className='flag'/>Asunción</p>
									<p>Hotel Sheraton</p>
									<p>4 de diciembre</p>
									<p className='containerClock'><img src="/images/icons/clock.png" alt="clock" className='clock'/>10 a 20hs</p>
									<Button className='btn btnPrimary' href='https://www.infocasas.com.py/latam-invierte-py/amp3' target='_blank'>
										Quiero Asistir
									</Button>
								</div>
							</div>
						</Fade>
						<Fade up>
							<div className='item'>
								<div className="itemText">
									<p className='title-item'><img src="/images/flags/pe.png" alt="" className='flag'/>Lima</p>
									<p>Hotel Swissôtel Lima</p>
									<p>4 de diciembre</p>
									<p className='containerClock'><img src="/images/icons/clock.png" alt="clock" className='clock'/>10 a 20hs</p>
									<Button className='btn btnPrimary' href='https://www.infocasas.com.pe/latam-invierte-pe/amp' target='_blank'>
										Quiero Asistir
									</Button>
								</div>
							</div>
						</Fade>
						
					</div>
					<Fade up>
						<div className='dColumnCenter textBanner'>
							<p>Una muestra de lo que vas a encontrar:</p>
						</div>
					</Fade>	
					<div className="containerVideo">
						<div className="theVideo">
							<iframe width="1090" height="613" src="https://www.youtube.com/embed/pkTWC7AabHM"></iframe>
						</div>
					</div>
				</div>
			</div>
			<style jsx>{`
				#theDay {
					background-color: #3a4145;
				}
				.textBanner p {
					font-size: 48px;
					color: #ddd;
					line-height: 1;
					margin-bottom: 15px;
					font-weight: 900;
				}
				.textBanner p:last-child {
					font-size: 30px;
					font-weight: 500;
					color: #fff;
				}
				.containerVideo {
					max-width: 900px;
					margin: 15px auto 60px;
				}
				.theVideo{
					padding-top: 56.26%;
					position: relative;
					width: 100%;
				}
				.containerGrid {
					display: grid;
					grid-template-columns: 50% 50%;
					max-width: 1150px;
  				margin: 50px auto;
				}
				.containerDate {
					background-color: #fff;
					border-radius: 20px;
					box-shadow: 0 3px 6px #00000030;
					margin: 0 auto;
					height: 100%;
					row-gap: 2px;
					min-height: 180px;
					width: 100%;
				}
				.containerDate p {
					font-size: 24px;
					font-weight: 900;
					text-transform: uppercase;
					margin: 0;
					line-height: 1;
				}
				.containerDate p:nth-child(2){
					font-size: 91px;
				}
				.item{
					position: relative;
					display: flex;
					flex-direction: column;
					justify-content: center;
					align-items: center;
					width: 100%;
					padding: 20px 20px 50px;
				}
				.item:nth-child(1),.item:nth-child(2){
					border-bottom: solid 1px #fff;
				}
				.item:nth-child(2),.item:nth-child(4){
					border-left: solid 1px #fff;
				}
				.item img.flag{
					height: auto;
					width: 45px;
					position: absolute;
					left: -60px;
					top: -10px;
				}
				.itemText{
					margin: 15px 0;
					width: 280px;
				}
				.itemText p{
					color: #fff;
					font-size: 24px;
					font-weight: 500;
					margin-bottom: 0;
				}
				.itemText p.title-item{
					color: #fff;
					font-size: 38px;
					font-weight: 900;
					margin-bottom: 0;
					position: relative;
					line-height: 1;
  				margin-bottom: 15px;
				}
				.itemText p img.clock{
					height: auto;
					width: 20px;
					transform: translateY(-2px);
					margin-right: 10px;
					font-size: 16px;
				}
				.itemText .containerClock{
					margin-bottom: 15px;
				}
				@media (max-width: ${theme.breakPoints.lg}){
					.containerGrid {
						grid-template-columns: 100%;
					}
					.item {
						border-left: none!important;
						border-bottom: none!important;
						margin: 0 auto;
						padding: 20px 0px 35px 50px;
					}
					.item + * {
						border-top: solid 1px #fff;
					}
					.containerDate {
						width: 200px;
					}
				}
				@media (max-width: ${theme.breakPoints.md}){
					.textBanner p {
						font-size: 35px;
					}
					.textBanner p:last-child {
						font-size: 24px;
					}
					.containerVideo {
						margin: 15px -15px 60px;
					}
					.itemText p{
						font-size: 18px;
					}
					.itemText p.title-item{
						font-size: 24px;
					}
				}
				@media (max-width: ${theme.breakPoints.sm}){
					.itemText {
						width: 200px;
					}
				}
			`}</style>
		</React.Fragment>
	)
}
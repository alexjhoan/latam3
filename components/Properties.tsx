import React, { useEffect, useState } from 'react'
import {Button} from "antd";
import { useTheme } from "../shared-components/Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import Fade from 'react-reveal/Fade';
import CounterDown from './CounterDown';

function useWindowSize() {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  useEffect(() => {
    if (typeof window !== 'undefined') {
      function handleResize() {
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight,
        });
      }
      window.addEventListener("resize", handleResize);
      handleResize();
      return () => window.removeEventListener("resize", handleResize);
    }
  }, []);
  return windowSize;
}

const listUy = [
	"Hasta 10% OFF",
	"Lanzamientos exclusivos",
	"Precios de socio inversor",
	"Opciones en los barrios más rentables de Montevideo",
]
const listPy = [
	"Hasta 15% OFF",
	"Proyectos en el centro financiero de Asunción.",
	"Lanzamiento exclusivos",
	"Propuestas de inversión con rentabilidad de más de 10% anual."
]

const clicfalse = (e:any) => {
	e.preventDefault()
}

export default function Properties() {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const size = useWindowSize();
	let screenWidth: number
	if (size.width > 1200) {
		screenWidth = 1.8
	} else if ((size.width <= 1200) && (size.width > 767)) {
		screenWidth = 1.5
	} else if ((size.width <= 767) && (size.width > 576)){
		screenWidth = 1.2
	} else if ((size.width <= 576) && (size.width > 0)){
		screenWidth = 0.9
	}
  
	return (
		<React.Fragment>
			<div id="Properties">
				<Fade down>
					<div className="containerLanding">
						<div className="title">
							<p>Estas ofertas terminan en</p>
							<CounterDown dateEnd={'12/05/2021'} title={' '} colorText={'dark'} sizeMultiplicator={screenWidth}/>
						</div>
					</div>
				</Fade>
			</div>
			<div className="propertiesLanding">
				<div className="containerLanding">
					<div className="containerProps">
						<Fade down>
							<div className="itemProps dColumnCenter">
								<div className="couterPropeties">
									<a href="https://www.infocasas.com.uy/evento-latam" target="_blank">
										<img src="/images/uy.png" alt="."/>
									</a>
								</div>
								<div className="detailsItem dColumnCenter">
									<Button href="https://www.infocasas.com.uy/evento-latam" target="_blank" className='btn btnPrimary '>{t("Ver Proyectos")}</Button>
									{/* <div className="listDetails">
										{
											listUy.map((item, i) => {
												return (
													<div className="itemDetail dRowCenter" key={i}>
														<img src="/images/icons/check.png" alt="."/>
														<p>{t(item)}</p>
													</div>
												)
											})
										}
									</div> */}
								</div>
							</div>
						</Fade>
						<Fade down>
							<div className="itemProps dColumnCenter">
								<div className="couterPropeties">
									<a href="https://www.infocasas.com.py/evento-latam" target="_blank">
										<img src="/images/py.png" alt="."/>
									</a>
								</div>
								<div className="detailsItem dColumnCenter">
									<Button href="https://www.infocasas.com.py/evento-latam" target="_blank" className='btn btnPrimary'>{t("Ver Proyectos")}</Button>
									{/* <div className="listDetails">
										{
											listPy.map((item, i) => {
												return (
													<div className="itemDetail dRowCenter" key={i}>
														<img src="/images/icons/check.png" alt="."/>
														<p>{t(item)}</p>
													</div>
												)
											})
										}
									</div> */}
								</div>
							</div>
						</Fade>
						<Fade down>
							<div className="itemProps dColumnCenter">
								<div className="couterPropeties">
									<a href="https://www.infocasas.com.pe/evento-latam" target="_blank">
										<img src="/images/pe.png" alt="."/>
									</a>
								</div>
								<div className="detailsItem dColumnCenter">
									<Button href="https://www.infocasas.com.pe/evento-latam" target="_blank" className='btn btnPrimary'>{t("Ver Proyectos")}</Button>
									{/* <div className="listDetails">
										{
											listPy.map((item, i) => {
												return (
													<div className="itemDetail dRowCenter" key={i}>
														<img src="/images/icons/check.png" alt="."/>
														<p>{t(item)}</p>
													</div>
												)
											})
										}
									</div> */}
								</div>
							</div>
						</Fade>
					</div>
				</div>
			</div>
			<style jsx>{`
				.propertiesLanding {
					margin-bottom: 80px;
				}
				.title{
					display: flex;
					flex-direction: column;
					align-items: flex-start;
				}
				.title p{
					font-size: 23px;
					font-weight: 900;
					margin-bottom: 0;
				}
				.propertiesLanding .containerProps {
				  display: grid;
				  grid-template-columns: 1fr;
				  grid-gap: 45px 20px;
				  margin: 15px 0 20px;
					width: 100%;
				}
				.propertiesLanding .couterPropeties {
				  position: relative;
				  width: 100%;
				}
				.propertiesLanding .couterPropeties img{
				  width: 100%;
					box-shadow: 0 5px 16px #00000030;
				}
				.propertiesLanding .textCounter {
				  position: absolute;
				  top: 43%;
				  left: 3%;
				}
				.propertiesLanding .couterPropeties span{
					color:#fff;
					font-weight: 900;
					font-size: 7vw;
    			line-height: 6vw;
				}
				.propertiesLanding .couterPropeties p{
					color: #fff;
					font-weight: 900;
					font-size: 5vw;
					line-height: 3vw;
					margin-bottom: 0;
				}
				.propertiesLanding .itemProps {
					justify-content: flex-start;
				}
				.propertiesLanding .detailsItem {
					margin-top: -10px;
				}
				.propertiesLanding .listDetails {
				  margin-top: 20px;
				}
				.propertiesLanding .itemDetail {
				  justify-content: right;
				  position: relative;
				}
				.propertiesLanding .itemDetail img {
				  height: 20px;
				  width: auto;
				  position: absolute;
  				top: 0;
				}
				.propertiesLanding .itemDetail p {
				  margin-bottom: 8px;
				  padding-left: 25px;
				  line-height: 18px;
				}
				.aDisabled{
					pointer-events: none;
					user-select: none;
				}
				@media (min-width: ${theme.breakPoints.sm}){
					.propertiesLanding .detailsItem {
						width: 75%;
						margin: -22px auto 0;
					}
					#properties .title p{
						font-size: 32px;
					}
					.propertiesLanding .toProperties {
						font-size: 24px;
						height: auto;
						padding: 5px 30px;
					}
					.title p{
						font-size: 32px;
					}
				}
				@media (min-width: ${theme.breakPoints.md}){
					#properties .blueBall {
						position: absolute;
						width: 120px;
						height: 120px;
						background: #8bb3d5;
						border-radius: 100%;
						top: 20%;
						left: -60px;
					}
					.propertiesLanding .detailsItem {
						width: 80%;
					}
					.propertiesLanding .containerProps {
					  grid-template-columns: 1fr 1fr 1fr;
					}
					.propertiesLanding .couterPropeties span{
						font-size: 3.3vw;
	    			line-height: 3vw;
					}
					.propertiesLanding .couterPropeties p{
						font-size: 2.2vw;
						line-height: 1vw;
					}
					.propertiesLanding .detailsItem {
						margin: -18px auto 0;
					}
					.propertiesLanding .toProperties {
						font-size: 18px;
						height: auto;
						padding: 0px 24px;
					}
					.title p{
						font-size: 40px;
					}
				}
				@media (min-width: ${theme.breakPoints.lg}){
					#properties .title p{
						font-size: 35px;
					}
					#properties .blueBall{
						position: absolute;
						width: 150px;
						height: 150px;
						background: #8bb3d5;
						border-radius: 100%;
						top: 19.5%;
						left: -75px;
					}
					.propertiesLanding .detailsItem button {
					  font-size: 20px;
					  height: auto;
					}
					.propertiesLanding .itemDetail p {
				    margin-bottom: 30px;
				    line-height: 22px;
				    font-size: 20px;
					}
				}
				@media (min-width: ${theme.breakPoints.xl}){
					#properties .title p{
						font-size: 38px;
					 }
					.propertiesLanding .couterPropeties span{
						font-size: 3.1vw;
	    			line-height: 3vw;
					}
					.propertiesLanding .couterPropeties p{
						font-size: 2vw;
						line-height: 1vw;
					}
					.title p{
						font-size: 48px;
					}
				}
				@media (min-width: ${theme.breakPoints.xxl}){
					#properties .title p{
						font-size: 44px;
					 }
					.propertiesLanding .detailsItem {
						width: 60%;
					}
					.propertiesLanding .couterPropeties span{
						font-size: 45px;
	    			line-height: 38px;
					}
					.propertiesLanding .couterPropeties p{
						font-size: 33px;
						line-height: 25px;
					}
					.propertiesLanding .detailsItem {
						margin: -22px auto 0;
					}
					.propertiesLanding .toProperties {
						font-size: 24px;
						height: auto;
						padding: 5px 30px;
					}
				}
			`}</style>
		</React.Fragment>
	)
}
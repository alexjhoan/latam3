import React from 'react'
import { useTheme } from "../shared-components/Styles/ThemeHook";
import Banner from "./Banner"
import OpenDay from "./OpenDay"
import TheDay from "./TheDay"
import Properties from "./Properties"
import Sponsor from "./Sponsor"
import Footer from "./Footer"
import Addons from './Addons';

export default function LandingComponent() {
	const { theme } = useTheme();
	return (
		<React.Fragment>
			<Banner />
			<OpenDay />
			<TheDay />
			<Properties />
			<Addons />
			{/* <Sponsor /> */}
			<Footer />
			<style jsx global>{`
				html{
					scroll-behavior: smooth;
				}
				#__next .theVideo iframe {
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
					display: block!important;
				}
				#openDay, #theDay, #Properties {
					padding-top: 60px;
					padding-bottom: 60px;
				}
				.containerLanding{
					width: 100%;
					flex: 0 0 100%;
					max-width: 1400px;
			    margin: 0 auto;
			    padding: 0 15px 15px;
				}
				.dRowCenter{
					display: flex;
			    flex-direction: row;
			    justify-content: center;
			    align-items: center;
			    position: relative;
				}
				.dColumnCenter{
					display: flex;
			    flex-direction: column;
			    justify-content: center;
			    align-items: center;
			    position: relative;
				}
				.textCenter{
					text-align: center;
				}
				.dNone{
					display: none!important;
				}
				.dBlock{
					display: block!important;
				}
				.dFlex{
					display: flex!important;
				}
				.btn.btnPrimary{
					color: #fff;
					background-color: #EA7329;
					border-radius: 25px;
					font-size: 16px;
					padding: 10px 30px;
					height: auto;
					font-weight: 500;
					border: none;
					line-height: 1.3;
				}
				.btn.btnDisabled{
					color: #fff;
					background-color: #D5D5D5;
					border-radius: 25px;
					font-size: 16px;
					padding: 10px 30px;
					height: auto;
					font-weight: 500;
					border: none;
					line-height: 1.5;
					pointer-events: none;
				}
				@media (min-width: ${theme.breakPoints.sm}){
					.dSmNone{
						display: none!important;
					}
					.dSmBlock{
						display: block!important;
					}
					.dSmFlex{
						display: flex!important;
					}
				}
				@media (min-width: ${theme.breakPoints.md}){
					.containerLanding{
						width: 90%;
						flex: 0 0 90%;
					}
					.dMdNone{
						display: none!important;
					}
					.dMdBlock{
						display: block!important;
					}
					.dMdInlineBlock{
						display: inline-block!important;
					}
					.dMdFlex{
						display: flex!important;
					}
				}
				@media (min-width: ${theme.breakPoints.lg}){
					.dLgNone{
						display: none!important;
					}
					.dLgBlock{
						display: block!important;
					}
					.dLgFlex{
						display: flex!important;
					}
				}
				@media (min-width: ${theme.breakPoints.xl}){
					.containerLanding{
						width: 80%;
						flex: 0 0 80%;
					}
				}
			`}</style>
		</React.Fragment>
	)
}
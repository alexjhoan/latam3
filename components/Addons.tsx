import React from 'react'
import Fade from 'react-reveal/Fade';
import { useTheme } from "../shared-components/Styles/ThemeHook";

export default function Addons() {
	const { theme } = useTheme();

	return (
		<React.Fragment>
			<div id="Addons">
				<div className='containerLanding'>
					<Fade up>
						<div className='addonsGrid'>
							<div className="itemGrid">
                <img src="/images/icons/dcto.png" alt="icon" />
                <p>Hasta<br />20% OFF</p>
              </div>
              <div className="itemGrid">
                <img src="/images/icons/socio.png" alt="icon" />
                <p>Precios de socio<br />inversor</p>
              </div>
              <div className="itemGrid">
                <img src="/images/icons/premios.png" alt="icon" />
                <p>Lanzamientos<br />Exclusivos</p>
              </div>
              <div className="itemGrid">
                <img src="/images/icons/bono.png" alt="icon" />
                <p>Bono de USD 3.000 en<br />proyectos seleccionados</p>
              </div>
              <div className="itemGrid">
                <img src="/images/icons/proyectos.png" alt="icon" />
                <p>Proyectos en los<br />barrios más rentables</p>
              </div>
              <div className="itemGrid">
                <img src="/images/icons/inversion.png" alt="icon" />
                <p>Propuestas de inversión<br />con alta rentabilidad</p>
              </div>
						</div>
					</Fade>
        </div>
      </div>
      <style jsx>{`
      #Addons{
        background: #DDDDDD;
        padding: 80px 0;
      }
      .addonsGrid{
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        justify-items: center;
        gap: 30px;
      }
      .addonsGrid .itemGrid {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      .addonsGrid .itemGrid p{
        text-align: center;
        margin-bottom: 0;
        font-size: 24px;
        line-height: 1.3;
        font-weight: 500;
      }
      .addonsGrid .itemGrid img {
        width: auto;
        height: 80px;
        margin-bottom: 20px;
      }
      `}</style>
    </React.Fragment>
  )
}
        		
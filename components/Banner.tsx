import React, { useEffect, useState } from "react";
import CounterDown from "./CounterDown";
import {useTheme} from "../shared-components/Styles/ThemeHook";
import Fade from 'react-reveal/Fade';

function useWindowSize() {
  const [windowSize, setWindowSize] = useState({
    width: undefined,
    height: undefined,
  });

  useEffect(() => {
    if (typeof window !== 'undefined') {
      function handleResize() {
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight,
        });
      }
      window.addEventListener("resize", handleResize);
      handleResize();
      return () => window.removeEventListener("resize", handleResize);
    }
  }, []);
  return windowSize;
}

export default function Banner() {
	const size = useWindowSize();
	const {theme} = useTheme();
	let screenWidth: number
	if (size.width < 1200) {
		screenWidth = 1
	} else {
		screenWidth = 1.5
	}
	return (
		<React.Fragment>
			<div className='bannerMain'>
				<img src="/images/banner-mobile.jpg" alt="" className='bgBanner dMdNone'/>
				<img src="/images/banner.png" alt="" className='bgBanner dNone dMdBlock'/>
				<div className='bannerContainer'>
					<Fade left>
						<div className="textLeft">
							<img src="/images/latam-banner-movil.png" alt="" className='imgBannerLeft dMdNone'/>
							<img src="/images/latam-banner.png" alt="" className='imgBannerLeft dNone dMdBlock'/>
							{/* <div className="innerText">
								<p>Del 23 de noviembre<br />al 04 de Diciembre</p>
							</div> */}
							<img src="/images/oportunidades-movil.png" alt="" className='imgBannerRight dMdNone'/>
						</div>
					</Fade>
					<Fade up>
					<div className="centerBanner">
						<CounterDown 
						dateEnd={"12/05/2021"}
						title={"Termina en"}
						colorText={"light"}
						sizeMultiplicator={screenWidth}/>
					</div>
					</Fade>
					<Fade right>
					<div className="textRight">
						<img src="/images/oportunidades.png" alt="" className='imgBannerRight dNone dMdBlock'/>
					</div>
					</Fade>
				</div>
			</div>
			<style jsx>{`
				.bannerMain{
					display: flex;
					flex-direction: row;
					margin-top: 64px;
					padding-bottom: 33.85%;
					width: 100%;
					position: relative;
				}
				.bgBanner {
					width: 100%;
					position: absolute;
					z-index: 0;
					left: 0;
				}
				.bannerContainer{
					display: flex;
					flex-direction: row;
					position: absolute;
					z-index: 1;
					width: 100%;
					justify-content: space-between;
					align-items: center;
					top: 50%;
					transform: translateY(-50%);
				}
				.imgBannerRight{
					max-width: 300px;
					width: 18vw;
					margin-right: -4px;
				}				
				.imgBannerLeft{
					max-width: 300px;
					width: 18vw;
				}
				.innerText p {
					font-size: 24px;
					font-weight: 900;
					color: #102A3A;
					line-height: 1.8;
					padding: 17px 30px 10px;
					background: #ECB840;
					border-radius: 0 10px 10px 0;
  				margin: 5px 0 15px;
				}
				@media (max-width: ${theme.breakPoints.lg}) {
					.innerText p {
						font-size: 18px;
						line-height: 1.5;
						padding: 10px 30px;
					}
				}
				@media (max-width: ${theme.breakPoints.md}) {
					.bannerMain{
						padding-bottom: 56.23%;
						min-height: 60vh;
					}
					.bannerContainer{
						display: flex;
						flex-direction: column;
						justify-content: center;
						row-gap: 30px;
					}
					.textLeft{
						row-gap: 30px;
						display: flex;
						flex-direction: column;
						justify-content: center;
						align-items: center;
					}
					.imgBannerRight{
						width: 65%;
						margin-right: 0;
					}
					.innerText p {
						border-radius: 20px;
						text-align: center;
						margin: 0;
					}
					.imgBannerLeft{
						width: 65%;
						padding-left: 0px;
					}
					.innerText p {
						font-size: 20px;
					}
				}
			`}</style>
		</React.Fragment>
	);
}

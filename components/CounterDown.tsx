import React from "react";
import {Statistic} from "antd";
import Lottie from 'react-lottie';
import * as animationReloj from './animations/reloj/data.json'
import {useTheme} from "../shared-components/Styles/ThemeHook";

interface count {
	dateInit?: string,
	dateEnd: string,
  title: string,
	sizeMultiplicator?: number,
	colorText: "light" | "dark",
	className?: any
}

export default function CounterDown(props: count) {
	const {theme} = useTheme();
	const {Countdown} = Statistic;
	const deadline = Date.parse(props.dateEnd);
	const today = Date.now();
	const init = Date.parse(props.dateInit);

	const multiplicator = props.sizeMultiplicator ? props.sizeMultiplicator : 1

	let color: string

	if (props.colorText === "light") {
		color = '#fff'
	} else if (props.colorText === "dark") {
		color = '#3A4145'
	}

	const styleCounter = {
		fontSize: `calc(36px * ${multiplicator})`,
		lineHeight: 1,
		fontWeight: 900,
		color: color
	}

	let counter: any;
	if (props.dateInit) {
		if ((today > init) && (today < deadline)) {
			counter = (
				<Countdown
					value={deadline}
					format="D : HH : mm : ss"
					valueStyle={styleCounter}
				/>
			);
		} else {
			counter = (<Statistic value={"0 : 00 : 00 : 00"} valueStyle={styleCounter}/>);
		}
	} else {
		if (today < deadline) {
			counter = (
				<Countdown
					value={deadline}
					format="D : HH : mm : ss"
					valueStyle={styleCounter}
				/>
			);
		} else {
			counter = (<Statistic value={"0 : 00 : 00 : 00"} valueStyle={styleCounter}/>);
		}
	}
	

  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationReloj.default,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  };

	return (
		<React.Fragment>
			<div className={`dColumnCenter counterContainer ${props.className}`}>
				<div className="dRowCenter">
					<div className="img-HourGlass">
						<Lottie options={defaultOptions}/>
					</div>
					<div className="timeCounter">
						<p className="titleCounter">
							{props.title}
						</p>
						{counter}
						<div className="textCounter">
							<span>días</span>
							<span>horas</span>
							<span>minutos</span>
							<span>segundos</span>
						</div>
					</div>
				</div>
			</div>	
			<style jsx>{`
				.counterContainer{
					justify-content: flex-start;
				}
				.counterContainer .img-HourGlass img {
					max-width: 100%;
				}
				.counterContainer .img-HourGlass {
					width: calc(80px * ${multiplicator});
				}
				.counterContainer p.titleCounter {
					margin: 0;
					font-weight: 900;
					font-size: calc(13px * ${multiplicator});
					color: ${color};
				}
				.counterContainer .textCounter {
					display: flex;
					flex-direction: row;
					justify-content: space-between;
					font-size: calc(12px * ${multiplicator});
					color: ${color};
				}
				.counterContainer .textCounter span:last-child{
					margin-left: calc(-10px - 1.1vw);
				}
				@media (max-width: ${theme.breakPoints.sm}) {
					.counterContainer .img-HourGlass {
						width: calc(68px * ${multiplicator});
					}
					.counterContainer p.titleCounter {
						font-size: calc(12px * ${multiplicator});
					}
					.counterContainer span.ant-statistic-content-value {
						font-size: calc(33px * ${multiplicator});
					}
					.counterContainer .textCounter {
						font-size: calc(11px * ${multiplicator});
					}
				}
			`}</style>
		</React.Fragment>
	);
}
